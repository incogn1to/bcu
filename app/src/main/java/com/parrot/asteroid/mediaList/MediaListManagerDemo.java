package com.parrot.asteroid.mediaList;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.util.Log;
import com.parrot.asteroid.ManagerObserverInterface;
import com.parrot.asteroid.audio.service.Source;
import com.parrot.asteroid.tools.AsteroidObservable;

class MediaListManagerDemo extends AsteroidObservable<MediaListObserverInterface> implements MediaListManagerInterface, MediaListQueryListener {
    private static final String TAG = MediaListManagerDemo.class.getSimpleName();
    private ArrayInfo mArrayInfo;
    private MediaListCursorInfo mCursorInfo;
    private MediaListQuery mQuery;

    MediaListManagerDemo() {
    }

    public void launchQuery(MediaListQuery query) {
        this.mQuery = query;
        query.setMediaListQueryListener(this);
        query.launchRequest();
    }

    public boolean deleteObserver(MediaListObserverInterface mediaListObserver) {
        return super.deleteObserver(mediaListObserver);
    }

    public boolean addObserver(MediaListObserverInterface mediaListObserver) {
        return super.addObserver(mediaListObserver);
    }

    /* Access modifiers changed, original: protected */
    public void notifyOnUpdateAdapter() {
        for (int i = 0; i < super.countObservers(); i++) {
            ((MediaListObserverInterface) super.getObserver(i)).onUpdateAdapter();
        }
    }

    /* Access modifiers changed, original: protected */
    public void notifyOnUpdatePosition(int position) {
        for (int i = 0; i < super.countObservers(); i++) {
            ((MediaListObserverInterface) super.getObserver(i)).onUpdatePosition(position);
        }
    }

    public void onQueryCompleted(Cursor cursor) {
        this.mCursorInfo = new MediaListCursorInfo(cursor, this.mQuery.getProjection()[1]);
        notifyOnUpdateAdapter();
    }

    public ArrayInfo getDataArray() {
        return this.mArrayInfo;
    }

    public MediaListCursorInfo getDataCursor() {
        return this.mCursorInfo;
    }

    public boolean requestPosition(String beginText) {
        int position = 0;
        if (beginText == null) {
            return false;
        }
        if (this.mCursorInfo == null) {
            return false;
        }
        if (this.mCursorInfo.getCursor() == null) {
            Log.w(TAG, "the cursor is null");
            return false;
        }
        Cursor cursor = this.mCursorInfo.getCursor();
        int index = cursor.getColumnIndex(this.mCursorInfo.getMainColumn()[0]);
        int savedPosition = cursor.getPosition();
        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            position = i;
            if (cursor.getString(index).compareToIgnoreCase(beginText) > 0) {
                break;
            }
        }
        cursor.moveToPosition(savedPosition);
        notifyOnUpdatePosition(position);
        return true;
    }

    public void setCursor(MatrixCursor cursor) {
    }

    public void requestDestroy(boolean force) {
    }

    public void launchCurentListQuery(String browsingPath) {
    }

    public void launchTrack(long id) {
    }

    public void setTranslation(Translation translation) {
    }

    public boolean isTrack(long id) {
        return false;
    }

    public void onError(int errorCode) {
    }

    public void setSource(Source source) {
    }

    public String getColumn(String columnName, int position, long id) {
        return null;
    }

    public String getValue(int position, long id) {
        return null;
    }

    public boolean isTrack(int position, long id) {
        return false;
    }

    public void launchTrack(int position, long id, boolean isCurrentList) {
    }

    public void addManagerObserver(ManagerObserverInterface managerObserver) {
    }

    public void deleteManagerObserver(ManagerObserverInterface managerObserver) {
    }

    public boolean isManagerReady() {
        return true;
    }

    public boolean isFilterable(String category, Source source) {
        return false;
    }

    public void onLongRequest() {
    }

    public boolean requestIsPending() {
        return false;
    }

    public boolean isCurrentListReco() {
        return false;
    }

    public void getCurrentBrowsing() {
    }

    public int getPlaylistPosition() {
        return 0;
    }

    public void setPositionInCurrentList(int position) {
    }

    public int getPositionInCurrentList() {
        return 0;
    }
}
