package com.parrot.asteroid.mediaList;

import com.parrot.asteroid.audio.service.Source;
import org.xmlpull.v1.XmlPullParserException;

public interface MediaListControllerInterface {
    boolean addObserver(MediaListObserverInterface mediaListObserverInterface);

    boolean back();

    boolean deleteObserver(MediaListObserverInterface mediaListObserverInterface);

    int getBrowsingMode();

    String getDynamicHeader(int i, long j, int i2);

    int getIconId();

    int getMediaListStackDepth();

    String getParentTxt();

    int getPosition();

    int getPositionInCurrentList();

    boolean hasAllEnabled();

    boolean hasDynamicHeader();

    boolean hasSubList(int i, int i2);

    boolean isEmpty();

    boolean isFilterable();

    boolean isRecusive();

    boolean isRootPage();

    void launchMediaPlayer();

    void requestDestroy(boolean z);

    boolean requestPosition(String str);

    void select(int i, long j, int i2);

    void setLanguage(Translation translation);

    void showLastStack();

    void start(Source source, int i) throws XmlPullParserException;

    void startCurrentList(Source source, int i) throws XmlPullParserException;
}
