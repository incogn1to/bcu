package com.parrot.asteroid.mediaList;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.util.Log;
import com.parrot.asteroid.ParrotIntent;
import com.parrot.asteroid.audio.service.AudioEffect;
import com.parrot.asteroid.audio.service.Source;
//import com.parrot.asteroid.tools.HSTIHelpers;
import com.parrot.hsti.HSTIInterface;
import com.parrot.hsti.SysListener;
//import com.parrot.hsti.data.HSTIArgId;
//import com.parrot.hsti.generated.ATParams.DISCO_PLAYLIST_ACTION;
//import com.parrot.hsti.generated.ATParams.MULTIMEDIA_PLAYER_CONTEXT_TYPE;
//import com.parrot.hsti.generated.Command.CPCC;
//import com.parrot.hsti.generated.Command.DLPA;
//import com.parrot.hsti.generated.Command.DLPE_Q;
import com.parrot.hsti.generated.Event;
import com.parrot.hsti.router.GenericCommand;
import com.parrot.hsti.router.GenericSystemEvt;
import com.parrot.platform.provider.MediaStore.Audio.DirectoryColumns;
import com.parrot.platform.provider.MediaStore.Audio.SettingsColumns;
import com.parrot.platform.provider.MediaStore.Audio.TrackColumns;
import com.parrot.platform.provider.MediaStoreHelper;

public class MediaListManagerHSTI extends MediaListManager implements SysListener {
    private static final String CATEGORY_DIRECTORY = "directory";
    private static final boolean DEBUG = true;
    private static final int DISCO_TYPE_TRACK = 1;
    private static final int DISCO_TYPE_UPNP_TRACK = 145;
    private static final String TAG = MediaListManager.class.getSimpleName();
    private MediaListQueryListener filterListener = new MediaListQueryListener() {
        public void onQueryCompleted(Cursor cursor) {
            if (cursor != null) {
                Bundle bundle = cursor.getExtras();
                if (bundle != null) {
                    int position = bundle.getInt("Browsing position", -1);
                    if (position != -1) {
                        MediaListManagerHSTI.this.notifyOnUpdatePosition(position);
                    } else {
                        Log.e(MediaListManagerHSTI.TAG, "the position isn't available in the cursor extras");
                    }
                } else {
                    Log.e(MediaListManagerHSTI.TAG, "no extra available in the cursor");
                }
                cursor.close();
                return;
            }
            Log.e(MediaListManagerHSTI.TAG, "Cursor invalid (null)");
        }

        public void onError(int errorCode) {
            Log.e(MediaListManagerHSTI.TAG, "query error : " + errorCode);
            MediaListManagerHSTI.this.notifyOnUpdatePosition(-1);
        }

        public void onLongRequest() {
            MediaListManagerHSTI.this.notifyOnTooLongAdapter();
        }
    };
    private BroadcastReceiver mBroadCastHomeIntentReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Source src = (Source) intent.getParcelableExtra("source");
            if (src == null) {
                Log.w(MediaListManagerHSTI.TAG, "receive sourceRemoved without source extra");
            } else if (MediaListManagerHSTI.this.mSource == null) {
                Log.w(MediaListManagerHSTI.TAG, "Current source was never set");
            } else if (src.equals(MediaListManagerHSTI.this.mSource)) {
                Log.d(MediaListManagerHSTI.TAG, "source removed :" + MediaListManagerHSTI.this.mSource.getSourceName());
                MediaListManagerHSTI.this.notifyOnSourceRemoved();
            }
        }
    };
    private Context mContext;
    private MediaListCursorInfo mCursorInfo;
    private boolean mHSTIConnected = false;
    private HSTIInterface mHsti;
    private MediaListQuery mLastQueryForFilter;
    private int mPositionInCurrentList;
    private Source mSource = null;
    private boolean m_requestIsPending = false;
    private MediaListQueryListener queryListener = new MediaListQueryListener() {
        public void onQueryCompleted(Cursor cursor) {
            MediaListManagerHSTI.this.queryCompleted(cursor);
        }

        public void onError(int errorCode) {
            MediaListManagerHSTI.this.setDataCursor(new MediaListCursorInfo(null, null));
            MediaListManagerHSTI.this.mCursorInfo.setErrCode(errorCode);
            MediaListManagerHSTI.this.notifyOnUpdateAdapter();
            MediaListManagerHSTI.this.setRequestPending(false);
        }

        public void onLongRequest() {
            MediaListManagerHSTI.this.notifyOnTooLongAdapter();
        }
    };

    public MediaListManagerHSTI(Context context) {
        this.mContext = context;
        this.mHsti = new HSTIInterface(TAG, context);
        registerHSTIEvent();
        this.mHsti.startInternalInterface();
        sourceRemovingRegister();
    }

    /* Access modifiers changed, original: protected */
    public void finalize() {
        Log.e(TAG, "finalize !!");
        unregisterHSTIEvent();
        sourceRemovingUnRegister();
    }

    private void registerHSTIEvent() {
        this.mHsti.registerSystemEvt(2, this);
    }

    private void unregisterHSTIEvent() {
        this.mHsti.unregisterSystemEvt(2, this);
    }

    public void launchQuery(MediaListQuery query) {
        String str = "/";
        this.mLastQueryForFilter = (MediaListQuery) query.clone();
        query.setMediaListQueryListener(this.queryListener);
        setRequestPending(true);
        if (query.getCategory().contains(CATEGORY_DIRECTORY)) {
            if (query.getSelections().size() > 0) {
                String str2;
                StringBuilder str3 = new StringBuilder();
                for (int i = 0; i < query.getSelections().size(); i++) {
                    String value = ((QuerySelection) query.getSelections().get(i)).getValue();
                    if (str3.length() == 0 || str3.charAt(str3.length() - 1) != '/') {
                        str2 = "/";
                        if (!value.startsWith(str)) {
                            str2 = "/";
                            str3.append(str);
                        }
                    }
                    str3.append(value);
                }
                if (!(str3.length() == 0 || str3.charAt(str3.length() - 1) == '/')) {
                    str2 = "/";
                    str3.append(str);
                }
                String name = ((QuerySelection) query.getSelections().get(0)).getName();
                query.clearSelection();
                query.addSelection(new QuerySelection(name, str3.toString()));
            }
            if (query.isRecursive()) {
                query.setSpecificProjectionfield(new String[]{"type", DirectoryColumns.UPNP_READABLE, DirectoryColumns.UPNP_RTP_SUPPORTED, DirectoryColumns.UPNP_TBSEEK_SUPPORTED, DirectoryColumns.UPNP_BBSEEK_SUPPORTED});
            }
        }
        query.launchRequest();
    }

    public void queryCompleted(Cursor cursor) {
        String columnName;
        String path = null;
        int browsingMode = 0;
        if (cursor != null) {
            try {
                columnName = cursor.getColumnName(1);
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.w(TAG, "no column name");
                columnName = null;
                cursor.close();
                cursor = null;
            }
            Bundle extras = cursor.getExtras();
            if (extras != null) {
                browsingMode = extras.getInt("Browsing mode");
                path = extras.getString("Browsing path");
            }
        } else {
            columnName = null;
        }
        setDataCursor(new MediaListCursorInfo(cursor, columnName));
        this.mCursorInfo.setInfo("browsingMode", browsingMode);
        this.mCursorInfo.setInfo(TrackColumns.PATH, path);
        notifyOnUpdateAdapter();
        setRequestPending(false);
    }

    public MediaListCursorInfo getDataCursor() {
        return this.mCursorInfo;
    }

    private void setDataCursor(MediaListCursorInfo m) {
        if (this.mCursorInfo != null) {
            this.mCursorInfo.closeCusor();
        }
        this.mCursorInfo = m;
    }

    public boolean requestPosition(String beginText) {
        this.mLastQueryForFilter.setMediaListQueryListener(this.filterListener);
        this.mLastQueryForFilter.setFilter(beginText);
        this.mLastQueryForFilter.launchRequest();
        return true;
    }

    public void setCursor(MatrixCursor cursor) {
        queryCompleted(cursor);
    }

    public void requestDestroy(boolean force) {
        if ((force || this.mMediaListObserverList.countObservers() == 0) && this.mCursorInfo != null) {
            this.mCursorInfo.closeCusor();
        }
    }

    public void launchCurentListQuery(String browsingPath) {
    }

    public void launchTrack(int position, long id, boolean iscurrentList) {
        if (this.mCursorInfo == null || this.mCursorInfo.getCursor() == null) {
            Log.e(TAG, "can't launch track id, no request is done");
            return;
        }
        Bundle extras = this.mCursorInfo.getCursor().getExtras();
        if (extras == null) {
            Log.e(TAG, "no extras informations in the cursor");
            return;
        }
        int browsingMode = extras.getInt("Browsing mode", -1);
        String browsingPath = extras.getString("Browsing path");
        if (iscurrentList) {
            launchTrackFromCurrentList(id);
        } else {
            createContext((int) id, browsingMode, browsingPath);
        }
    }

    private void createContext(int trackId, int browsingMode, String browsingPath) {
/*        CPCC createContextCommand;
        if (this.mSource.getType() == 11) {
            createContextCommand = new CPCC(new HSTIArgId(this.mSource.getId()), MULTIMEDIA_PLAYER_CONTEXT_TYPE.BROWSING, trackId);
        } else {
            createContextCommand = new CPCC(new HSTIArgId(this.mSource.getId()), MULTIMEDIA_PLAYER_CONTEXT_TYPE.BROWSING, trackId, browsingMode, browsingPath);
        }
        Log.d(TAG, "create context :source:" + this.mSource.getId() + ", track:" + trackId + " ,browsingMode:" + browsingMode + ", browsingPath:" + browsingPath);
        createContextCommand.setAsynchronous();
        this.mHsti.send(createContextCommand);
        HSTIHelpers.hasError(TAG, createContextCommand);*/
    }

    public void onSysEvt(GenericSystemEvt evt) {
/*        switch (evt.getId()) {
            case 2:
                this.mHSTIConnected = true;
                Log.d(TAG, "hsti connected");
                return;
            case 3:
                this.mHSTIConnected = false;
                Log.d(TAG, "hsti disconnected");
                return;
            default:
                Log.w(TAG, "unknown event : " + evt.getName());
                return;
        }*/
    }

    public void setTranslation(Translation translation) {
        sendTranslationCommand(translation);
    }

    private void sendTranslationCommand(Translation translation) {
        String str = SettingsColumns.ROOT;
        Log.d(TAG, "send disco language info");
        ContentValues values = new ContentValues();
        values.put("all", translation.getTextAll("All"));
        values.put(SettingsColumns.EMPTY, translation.getTextEmpty("Empty"));
        String str2 = SettingsColumns.ROOT;
        str2 = SettingsColumns.ROOT;
        values.put(str, translation.getTextRoot(str));
        this.mContext.getContentResolver().update(MediaStoreHelper.getSettings().getUri(), values, null, null);
    }

    public boolean isTrack(int position, long id) {
        if (this.mCursorInfo == null || this.mCursorInfo.getCursor() == null) {
            Log.e(TAG, "no cursor info available");
            return false;
        }
        Cursor cursor = this.mCursorInfo.getCursor();
        cursor.moveToPosition(position);
        int columnIndex = cursor.getColumnIndex("type");
        if (columnIndex == -1) {
            Log.e(TAG, "invalid colum index for cursor");
            return false;
        }
        int type = cursor.getInt(columnIndex);
        return type == 1 || type == DISCO_TYPE_UPNP_TRACK;
    }

    public String getValue(int position, long id) {
        if (this.mCursorInfo == null || this.mCursorInfo.getCursor() == null) {
            Log.e(TAG, "no cursor info available");
            return null;
        }
        Cursor cursor = this.mCursorInfo.getCursor();
        cursor.moveToPosition(position);
        return cursor.getString(1);
    }

    public String getColumn(String columnName, int position, long id) {
        if (this.mCursorInfo == null || this.mCursorInfo.getCursor() == null) {
            Log.e(TAG, "no cursor info available");
            return null;
        }
        Cursor cursor = this.mCursorInfo.getCursor();
        cursor.moveToPosition(position);
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    /* Access modifiers changed, original: 0000 */
    public void sourceRemovingRegister() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ParrotIntent.ACTION_SOURCE_REMOVE);
        filter.addCategory("android.intent.category.DEFAULT");
        filter.setPriority(AudioEffect.SUBWOOFER_FREQUENCY_MAX);
        this.mContext.registerReceiver(this.mBroadCastHomeIntentReceiver, filter);
    }

    /* Access modifiers changed, original: 0000 */
    public void sourceRemovingUnRegister() {
        this.mContext.unregisterReceiver(this.mBroadCastHomeIntentReceiver);
    }

    public void setSource(Source source) {
        Log.d(TAG, "setSource : " + source.getSourceName());
        this.mSource = source;
    }

    public boolean isManagerReady() {
        return this.mHSTIConnected;
    }

    public boolean isFilterable(String category, Source source) {
        if (this.mSource.getType() == 8 && category.equals("media")) {
            return false;
        }
        return true;
    }

    public boolean requestIsPending() {
        return this.m_requestIsPending;
    }

    private void setRequestPending(boolean value) {
        this.m_requestIsPending = value;
    }

    private void launchTrackFromCurrentList(long id) {
      /*  DLPA cmdDLPA = new DLPA(DISCO_PLAYLIST_ACTION.SET_CURRENT_SONG, (int) id);
        this.mHsti.send(cmdDLPA);
        if (cmdDLPA.isOK()) {
            Log.d(TAG, "DLPA successfull");
        } else {
            Log.e(TAG, "DLPA error :" + cmdDLPA.getId());
        }*/
    }

    public boolean isCurrentListReco() {
        /*GenericCommand cmdDLPE = new DLPE_Q();
        this.mHsti.send(cmdDLPE);
        if (cmdDLPE.isOK()) {
            Event.DLPE_Q evtDLPE = (Event.DLPE_Q) HSTIHelpers.getEventType(605764833, cmdDLPE);
            if (evtDLPE != null) {
                return evtDLPE.Count != 0;
            } else {
                Log.e(TAG, " no DLPE_Q response :");
                return false;
            }
        }
        Log.e(TAG, "DLPE_Q error :" + cmdDLPE.getId());*/
        return false;
    }

    public int getPlaylistPosition() {
        /*GenericCommand cmdDLPA = new DLPA(DISCO_PLAYLIST_ACTION.GET_CURRENT_SONG);
        this.mHsti.send(cmdDLPA);
        if (cmdDLPA.isOK()) {
            Event.DLPA evtDLPA = (Event.DLPA) HSTIHelpers.getEventType(2010269824, cmdDLPA);
            if (evtDLPA != null) {
                return evtDLPA.Index - 1;
            }
            Log.e(TAG, "no DLPA response");
            return 0;
        }
        Log.e(TAG, "DLPA error :" + cmdDLPA.getId());*/
        return 0;
    }

    public void getCurrentBrowsing() {
    }

    public void setPositionInCurrentList(int position) {
        this.mPositionInCurrentList = position;
    }

    public int getPositionInCurrentList() {
        return this.mPositionInCurrentList;
    }
}
