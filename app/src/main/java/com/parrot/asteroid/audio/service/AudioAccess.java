package com.parrot.asteroid.audio.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.parrot.asteroid.ParrotIntent;
import com.parrot.asteroid.ServiceStatusInterface;
import com.parrot.asteroid.ServiceStatusInterface.Status;
import com.parrot.asteroid.audio.service.IAudioServiceCallBacks.Stub;
import java.util.ArrayList;
import java.util.List;

public class AudioAccess {
    private static final boolean DEBUG = true;
    private static final boolean DEBUG_ALL = false;
    protected static final String TAG = "AudioAccess";
    private ServiceStatusInterface mAudioAccessListener;
    private AudioCallbackInterface mAudioListener;
    private IAudioService mAudioService;

    private final Stub mAudioServiceCallBacks = new Stub() {
        public void onServiceReady(boolean ready) {
            String str = AudioAccess.TAG;
            Log.i(str, "onServiceReady:" + ready);
            if (ready) {
                AudioAccess.this.mServiceStatus = Status.SERVICE_READY;
            } else {
                AudioAccess.this.mServiceStatus = Status.SERVICE_CONNECTED;
            }
            try {
                AudioAccess.this.mAudioAccessListener.onServiceReady(AudioAccess.this.mServiceStatus);
            } catch (NullPointerException e) {
                NullPointerException e2 = e;
                Log.e(str, "mAudioAccessListener is null ?", e2);
            }
        }

        public void volumeChanged(int source, int currentVolume, int maxVolume) {
            String str = AudioAccess.TAG;
            Log.i(str, "Volume Changed");
            try {
                AudioAccess.this.mAudioListener.onVolumeChanged(source, currentVolume, maxVolume);
            } catch (NullPointerException e) {
                NullPointerException e2 = e;
                Log.w(str, "mMediaListener is null ?", e2);
            }
        }

        public void onMetadataChanged(Metadata metadata, int changeFlag) {
            String str = AudioAccess.TAG;
            Log.i(str, "onMetadataChanged");
            try {
                AudioAccess.this.mMediaListener.onMetadataChanged(metadata, changeFlag);
            } catch (NullPointerException e) {
                NullPointerException e2 = e;
                Log.w(str, "mMediaListener is null ?", e2);
            }
        }

        public void onSourceChanged(Source currentSource) {
            String str = AudioAccess.TAG;
            Log.i(str, "onSourceChanged");
            try {
                AudioAccess.this.mMediaListener.onSourceChanged(currentSource);
            } catch (NullPointerException e) {
                NullPointerException e2 = e;
                Log.w(str, "mMediaListener is null ?", e2);
            }
        }

        public void onPlayerStateChanged(PlayerInfo playerInfo) {
            try {
                AudioAccess.this.mMediaListener.onPlayerStateChanged(playerInfo);
            } catch (NullPointerException e) {
                Log.w(AudioAccess.TAG, "mMediaListener is null ?", e);
            }
        }

        public void onAllMediaInfos(Source source, PlayerInfo playerInfo, Metadata metadata) {
            String str = AudioAccess.TAG;
            Log.i(str, "onAllMediaInfos");
            try {
                AudioAccess.this.mMediaListener.onPlayerStatusAvailable(source, playerInfo, metadata);
            } catch (NullPointerException e) {
                NullPointerException e2 = e;
                Log.w(str, "mMediaListener is null ?", e2);
            }
        }

        public void onSourceCreation(Source creatingSource) {
            String str = AudioAccess.TAG;
            Log.i(str, "onSourceCreation");
            try {
                AudioAccess.this.mMediaListener.onSourceCreation(creatingSource);
            } catch (NullPointerException e) {
                NullPointerException e2 = e;
                Log.w(str, "mMediaListener is null ?", e2);
            }
        }

        public void onSourceError(Source source) {
            String str = AudioAccess.TAG;
            Log.i(str, "onSourceError");
            try {
                AudioAccess.this.mMediaListener.onSourceError(source);
            } catch (NullPointerException e) {
                NullPointerException e2 = e;
                Log.w(str, "mMediaListener is null ?", e2);
            }
        }

        public void onSourceReady(Source readySource) {
            String str = AudioAccess.TAG;;
            Log.i(str, "onSourceReady");
            try {
                AudioAccess.this.mMediaListener.onSourceReady(readySource);
            } catch (NullPointerException e) {
                NullPointerException e2 = e;
                Log.w(str, "mMediaListener is null ?", e2);
            }
        }

        public void onSourceRemoved(Source source) {
            String str = AudioAccess.TAG;
            Log.i(str, "onSourceRemoved");
            try {
                AudioAccess.this.mMediaListener.onSourceRemoved(source);
            } catch (NullPointerException e) {
                NullPointerException e2 = e;
                Log.w(str, "mMediaListener is null ?", e2);
            }
        }

        public void onSourceUpdate(Source source) {
            String str = AudioAccess.TAG;
            Log.i(str, "onSourceUpdate");
            try {
                AudioAccess.this.mMediaListener.onSourceUpdate(source);
            } catch (NullPointerException e) {
                NullPointerException e2 = e;
                Log.w(str, "mMediaListener is null ?", e2);
            }
        }

        public void onServiceDestroyed() {
            String str = AudioAccess.TAG;
            Log.i(str, "onServiceDestroyed -> try unbind");
            try {
                AudioAccess.this.mContext.unbindService(AudioAccess.this.mConnection);
            } catch (IllegalArgumentException e) {
                Log.w(str, "unbind failed  status: " + AudioAccess.this.mServiceStatus);
            }
            if (AudioAccess.this.mServiceStatus != Status.SERVICE_DISCONNECTED) {
                AudioAccess.this.mServiceStatus = Status.SERVICE_DISCONNECTED;
                if (AudioAccess.this.mAudioAccessListener != null) {
                    AudioAccess.this.mAudioAccessListener.onServiceReady(AudioAccess.this.mServiceStatus);
                } else {
                    Log.w(str, "mAudioAccessListener is NULL");
                }
            }
            AudioAccess.this.mAudioService = null;
            AudioAccess.this.mAudioListener = null;
            AudioAccess.this.mMediaListener = null;
        }
    };

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            String str = AudioAccess.TAG;
            AudioAccess.this.mAudioService = IAudioService.Stub.asInterface(service);
            Log.i(str, "Service " + name.flattenToShortString() + " Connected " + AudioAccess.this.mAudioService);
            AudioAccess.this.mServiceStatus = Status.SERVICE_CONNECTED;
            if (AudioAccess.this.mAudioAccessListener != null) {
                AudioAccess.this.mAudioAccessListener.onServiceReady(AudioAccess.this.mServiceStatus);
                if (!AudioAccess.this.registerService(AudioAccess.this.mStartFilterMask | 1)) {
                    Log.e(str, "Error when registering to service!");
                    return;
                }
                return;
            }
            Log.w(str, "Already destroy");
            AudioAccess.this.mServiceStatus = Status.SERVICE_DISCONNECTED;
        }

        public void onServiceDisconnected(ComponentName name) {
            Log.i(AudioAccess.TAG, "Service " + name.flattenToShortString() + " DISConnected");
            if (AudioAccess.this.mServiceStatus != Status.SERVICE_DISCONNECTED) {
                AudioAccess.this.mServiceStatus = Status.SERVICE_DISCONNECTED;
                if (AudioAccess.this.mAudioAccessListener != null) {
                    AudioAccess.this.mAudioAccessListener.onServiceReady(AudioAccess.this.mServiceStatus);
                }
            }
            AudioAccess.this.mAudioService = null;
        }
    };

    private Context mContext;
    private int mFilterMask = 0;
    private MediaCallbackInterface mMediaListener;
    private Status mServiceStatus = Status.SERVICE_DISCONNECTED;
    private int mStartFilterMask = 1;

    public AudioAccess(Context ctx, ServiceStatusInterface audioAccessListener) {
        String str = TAG;
        Log.i(str, "Startup");
        this.mContext = ctx;
        if (audioAccessListener == null) {
            Log.e(str, "Setting a listener is Mandatory");
        }
        this.mAudioAccessListener = audioAccessListener;
        this.mAudioListener = null;
        this.mMediaListener = null;
        Intent audioServiceIntent = new Intent(ParrotIntent.ACTION_PARROT_AUDIO_SERVICE_START);
        Log.i(str, "binding");
        if (!ctx.bindService(audioServiceIntent, this.mConnection, 1)) {
            Log.e(str, "not binded !");
        }
    }

    public boolean setListener(AudioCallbackInterface listener) {
        boolean succeeded;
        String str = TAG;
        Log.i(str, "setListener");
        if (this.mServiceStatus != Status.SERVICE_CONNECTED && this.mServiceStatus != Status.SERVICE_READY) {
            Log.w(str, "Service not yet connected -> record the listener to register it later");
            this.mStartFilterMask |= 2;
            succeeded = true;
        } else if ((this.mFilterMask & 2) == 0) {
            succeeded = changeFilterMask(this.mFilterMask | 2);
        } else {
            succeeded = true;
        }
        if (succeeded) {
            this.mAudioListener = listener;
        } else {
            Log.e(str, "registerService failed");
        }
        return succeeded;
    }

    public boolean removeListener(AudioCallbackInterface listener) {
        Log.i(TAG, "removeListener");
        this.mAudioListener = null;
        if (this.mAudioListener != null || this.mMediaListener != null) {
            return changeFilterMask(this.mFilterMask & -3) && null != null;
        } else {
            if (!unregisterService() || null == null) {
                return false;
            }
            return true;
        }
    }

    public boolean setListener(MediaCallbackInterface listener) {
        boolean succeeded;
        String str = TAG;
        Log.i(str, "addListener");
        if (this.mServiceStatus == Status.SERVICE_CONNECTED || this.mServiceStatus == Status.SERVICE_READY) {
            Log.i(str, "service connected");
            if ((this.mFilterMask & 4) == 0) {
                succeeded = changeFilterMask(this.mFilterMask | 4);
            } else {
                succeeded = true;
            }
        } else {
            Log.w(str, "Service not yet connected -> record the listener to register it later");
            this.mStartFilterMask |= 4;
            succeeded = true;
        }
        if (succeeded) {
            this.mMediaListener = listener;
        } else {
            Log.e(str, "registerService failed");
        }
        return succeeded;
    }

    public boolean removeListener(MediaCallbackInterface listener) {
        Log.i(TAG, "removeListener");
        this.mMediaListener = null;
        if (this.mAudioListener != null || this.mMediaListener != null) {
            return changeFilterMask(this.mFilterMask & -5) && null != null;
        } else {
            if (!unregisterService() || null == null) {
                return false;
            }
            return true;
        }
    }

    public boolean destroy() {
        String str = TAG;
        boolean succeeded = false;
        Log.i(str, "destroy");
        if (!(this.mAudioListener == null && this.mMediaListener == null)) {
            Log.w(str, "listeners are not unregistered");
        }
        try {
            if (!(this.mAudioService == null || this.mAudioServiceCallBacks == null || this.mServiceStatus != Status.SERVICE_READY)) {
                succeeded = this.mAudioService.unregister(this.mAudioServiceCallBacks);
            }
            this.mServiceStatus = Status.SERVICE_DISCONNECTED;
            this.mFilterMask = 0;
            this.mContext.unbindService(this.mConnection);
        } catch (RemoteException e) {
            catchDOEx(e);
        } catch (Exception e2) {
            Exception e3 = e2;
            Log.e(str, "Error during AudioAccess Destroy");
            e3.printStackTrace();
        }
        this.mAudioAccessListener = null;
        this.mAudioListener = null;
        this.mMediaListener = null;
        return succeeded;
    }

    /* Access modifiers changed, original: 0000 */
    public boolean registerService(int filterMask) {
        String str = TAG;
        boolean succeded = false;
        Log.i(str, "registerService");
        if (this.mServiceStatus != Status.SERVICE_CONNECTED && this.mServiceStatus != Status.SERVICE_READY) {
            Log.e(str, "service not yet connected");
        } else if (this.mAudioService != null) {
            try {
                succeded = this.mAudioService.register(filterMask, this.mAudioServiceCallBacks);
                this.mFilterMask = filterMask;
            } catch (RemoteException e) {
                catchDOEx(e);
                return false;
            }
        } else {
            Log.e(str, "mAudioService is null");
            succeded = false;
        }
        return succeded;
    }

    /* Access modifiers changed, original: 0000 */
    public boolean changeFilterMask(int filterMask) {
        String str = TAG;
        boolean succeded = false;
        Log.i(str, "changeFilterMask old:" + this.mFilterMask + "new :" + filterMask);
        if (this.mServiceStatus == Status.SERVICE_DISCONNECTED || this.mAudioService == null) {
            boolean z;
            StringBuilder append = new StringBuilder().append("mServiceStatus: ").append(this.mServiceStatus).append(" mAudioService null:");
            if (this.mAudioService == null) {
                z = true;
            } else {
                z = false;
            }
            Log.e(str, append.append(z).toString());
            return false;
        }
        if (this.mFilterMask != filterMask) {
            try {
                succeded = this.mAudioService.changeListenMask(filterMask, this.mAudioServiceCallBacks);
            } catch (RemoteException e) {
                catchDOEx(e);
                succeded = false;
            }
            if (succeded) {
                Log.i(str, "change successfull old:" + this.mFilterMask + " to new :" + filterMask);
                this.mFilterMask = filterMask;
            }
        }
        return succeded;
    }

    private boolean unregisterService() {
        String str = TAG;
        Log.i(str, "unregisterService");
        if (!(this.mAudioListener == null && this.mMediaListener == null)) {
            Log.w(str, "Listener aren't null! ");
        }
        if ((this.mServiceStatus == Status.SERVICE_CONNECTED || this.mServiceStatus == Status.SERVICE_READY) && this.mAudioService != null) {
            return changeFilterMask(1);
        }
        Log.w(str, "Error during Unregistering! status: " + this.mServiceStatus + " audioService Null ? " + (this.mAudioService == null));
        return false;
    }

    /* Access modifiers changed, original: protected */
    public void finalize() throws Throwable {
        if (destroy()) {
            Log.e(TAG, "destroyed called in finalize!");
        }
        super.finalize();
    }

    private void catchDOEx(RemoteException e) {
        String str = TAG;
        if (e instanceof DeadObjectException) {
            Log.w(str, "client Died -> mAudioService = null");
            this.mAudioService = null;
            if (this.mServiceStatus != Status.SERVICE_DISCONNECTED) {
                this.mServiceStatus = Status.SERVICE_DISCONNECTED;
                this.mAudioAccessListener.onServiceReady(this.mServiceStatus);
            }
        } else if (e.toString().contains("android.os.DeadObjectException")) {
            Log.w(str, "client Died -> mAudioService = null: contains DeadObjectException instance of " + e.toString());
            this.mAudioService = null;
            if (this.mServiceStatus != Status.SERVICE_DISCONNECTED) {
                this.mServiceStatus = Status.SERVICE_DISCONNECTED;
                this.mAudioAccessListener.onServiceReady(this.mServiceStatus);
            }
        } else {
            e.printStackTrace();
        }
    }

    public boolean setVolume(int src, int volume) {
        String str = TAG;
        boolean succeeded = false;
        Log.i(str, "set Volume");
        if (this.mAudioService != null) {
            try {
                return this.mAudioService.setAudioVolume(src, volume);
            } catch (RemoteException e) {
                catchDOEx(e);
                return succeeded;
            }
        }
        Log.e(str, "mAudioService is null");
        return succeeded;
    }

    public boolean blockVolumeUI(boolean block) {
        String str = TAG;
        Log.i(str, "blockVolumeUI:" + block);
        boolean ret = false;
        if (this.mAudioService != null) {
            try {
                return this.mAudioService.blockUiMediaNotification(block, 1);
            } catch (RemoteException e) {
                catchDOEx(e);
                return ret;
            }
        }
        Log.e(str, "mAudioService is null");
        return ret;
    }

    public boolean nextMedia() {
        String str = TAG;
        Log.i(str, "nextMedia");
        if (this.mAudioService != null) {
            try {
                this.mAudioService.nextMedia();
                return true;
            } catch (RemoteException e) {
                catchDOEx(e);
                return false;
            }
        }
        Log.e(str, "mAudioService is null");
        return false;
    }

    public boolean previousMedia() {
        String str = TAG;
        Log.i(str, "previousMedia");
        if (this.mAudioService != null) {
            try {
                this.mAudioService.previousMedia();
                return true;
            } catch (RemoteException e) {
                catchDOEx(e);
                return false;
            }
        }
        Log.e(str, "mAudioService is null");
        return false;
    }

    public boolean playPause() {
        String str = TAG;
        Log.i(str, "playPause");
        if (this.mAudioService != null) {
            try {
                this.mAudioService.playPause();
                return true;
            } catch (RemoteException e) {
                catchDOEx(e);
                return false;
            }
        }
        Log.e(str, "mAudioService is null");
        return false;
    }

    public boolean play() {
        String str = TAG;
        Log.i(str, "play");
        if (this.mAudioService != null) {
            try {
                this.mAudioService.play();
                return true;
            } catch (RemoteException e) {
                catchDOEx(e);
                return false;
            }
        }
        Log.e(str, "mAudioService is null");
        return false;
    }

    public boolean playSpeedNormal() {
        String str = TAG;
        Log.i(str, "playSpeedNormal");
        if (this.mAudioService != null) {
            try {
                this.mAudioService.playSpeedNormal();
                return true;
            } catch (RemoteException e) {
                catchDOEx(e);
                return false;
            }
        }
        Log.e(str, "mAudioService is null");
        return false;
    }

    public boolean pause() {
        String str = TAG;
        Log.i(str, "pause");
        if (this.mAudioService != null) {
            try {
                this.mAudioService.pause();
                return true;
            } catch (RemoteException e) {
                catchDOEx(e);
                return false;
            }
        }
        Log.e(str, "mAudioService is null");
        return false;
    }

    public boolean pauseSync() {
        String str = TAG;
        boolean succeeded = false;
        Log.i(str, "pauseSync");
        if (this.mAudioService != null) {
            try {
                return this.mAudioService.pauseSync();
            } catch (RemoteException e) {
                catchDOEx(e);
                return succeeded;
            }
        }
        Log.e(str, "mAudioService is null");
        return succeeded;
    }

    public boolean askMetadata() {
        String str = TAG;
        Log.i(str, "askMetadata");
        if (this.mAudioService != null) {
            try {
                this.mAudioService.askMetadata();
                return true;
            } catch (RemoteException e) {
                catchDOEx(e);
                return false;
            }
        }
        Log.e(str, "mAudioService is null");
        return false;
    }

    @Deprecated
    public Source getSource() {
        return null;
    }

    public Source getCurrentSource() {
        String str = TAG;
        Source source = null;
        Log.i(str, "getCurrentSource");
        if (this.mAudioService != null) {
            try {
                return this.mAudioService.getCurrentSource();
            } catch (RemoteException e) {
                catchDOEx(e);
                return source;
            }
        }
        Log.e(str, "mAudioService is null");
        return source;
    }

    public boolean setRandomRepeat(int random, int repeat) {
        String str = TAG;
        Log.i(str, "setRandomRepeat");
        if (this.mAudioService != null) {
            try {
                this.mAudioService.setRandomRepeat(random, repeat);
                return true;
            } catch (RemoteException e) {
                catchDOEx(e);
                return false;
            }
        }
        Log.e(str, "mAudioService is null");
        return false;
    }

    public boolean askAllInfos() {
        String str = TAG;
        Log.i(str, "askAllInfos");
        if (this.mAudioService != null) {
            try {
                this.mAudioService.askAllMediaInfos();
                return true;
            } catch (RemoteException e) {
                catchDOEx(e);
                return false;
            }
        }
        Log.e(str, "mAudioService is null");
        return false;
    }

    public ArrayList<Source> getSources(boolean onlyStacked) {
        String str = TAG;
        Log.i(str, "getSources");
        List<Source> sources = new ArrayList();
        if (this.mAudioService != null) {
            try {
                sources = this.mAudioService.getSourceStack(onlyStacked);
            } catch (RemoteException e) {
                catchDOEx(e);
            }
        } else {
            Log.e(str, "mAudioService is null");
        }
        return (ArrayList) sources;
    }

    public void playSource(Source src, boolean newContext) {
        String str = TAG;
        Log.i(str, "playSource : " + src.getId());
        if (this.mAudioService != null) {
            try {
                this.mAudioService.playSource(src, newContext);
                return;
            } catch (RemoteException e) {
                catchDOEx(e);
                return;
            }
        }
        Log.e(str, "mAudioService is null");
    }

    @Deprecated
    public void playSource(Source src) {
        playSource(src, false);
    }

    public boolean start() {
        String str = TAG;
        boolean succeeded = false;
        Log.i(str, "Start ParrotService");
        if (this.mAudioService != null) {
            try {
                return this.mAudioService.startParrotService();
            } catch (RemoteException e) {
                catchDOEx(e);
                return succeeded;
            }
        }
        Log.e(str, "mAudioService is null");
        return succeeded;
    }

    public int getVolume(int audioSrcGet) {
        String str = TAG;
        int volume = -2;
        Log.i(str, "getVolume:" + audioSrcGet);
        if (this.mAudioService != null) {
            try {
                return this.mAudioService.getVolume(audioSrcGet);
            } catch (RemoteException e) {
                catchDOEx(e);
                return volume;
            }
        }
        Log.e(str, "mAudioService is null");
        return volume;
    }

    public int getMaxVolume(int audioSrcGet) {
        String str = TAG;
        int volume = -2;
        Log.i(str, "getMaxVolume:" + audioSrcGet);
        if (this.mAudioService != null) {
            try {
                return this.mAudioService.getMaxVolume(audioSrcGet);
            } catch (RemoteException e) {
                catchDOEx(e);
                return volume;
            }
        }
        Log.e(str, "mAudioService is null");
        return volume;
    }

    public boolean isTypeSourceActivated(int type, int id) {
        String str = TAG;
        boolean ret = false;
        Log.i(str, "isTypeSourceActivated");
        if (this.mAudioService != null) {
            try {
                return this.mAudioService.isTypeActivated(type, id);
            } catch (RemoteException e) {
                catchDOEx(e);
                return ret;
            }
        }
        Log.e(str, "mAudioService is null");
        return ret;
    }

    public boolean setTypeSourceActivated(int type, int id, boolean activated) {
        String str = TAG;
        boolean succeeded = false;
        Log.i(str, "setTypeSourceActivated");
        if (this.mAudioService != null) {
            try {
                return this.mAudioService.setTypeActivated(type, id, activated);
            } catch (RemoteException e) {
                catchDOEx(e);
                return succeeded;
            }
        }
        Log.e(str, "mAudioService is null");
        return succeeded;
    }

    public boolean seekToPosition(int position) {
        String str = TAG;
        Log.i(str, "seekToPosition:" + position);
        if (this.mAudioService != null) {
            try {
                this.mAudioService.seekToPosition(position);
                return true;
            } catch (RemoteException e) {
                catchDOEx(e);
                return false;
            }
        }
        Log.e(str, "mAudioService is null");
        return false;
    }

    public boolean getCoverActivated() {
        String str = TAG;
        boolean activated = false;
        Log.i(str, "getCoverActivated");
        if (this.mAudioService != null) {
            try {
                return this.mAudioService.getCoverActivated();
            } catch (RemoteException e) {
                catchDOEx(e);
                return activated;
            }
        }
        Log.e(str, "mAudioService is null");
        return activated;
    }

    public boolean setCoverActivated(boolean activated) {
        String str = TAG;
        Log.i(str, "setCoverACtivated:" + activated);
        if (this.mAudioService != null) {
            try {
                this.mAudioService.setCoverActivated(activated);
                return true;
            } catch (RemoteException e) {
                catchDOEx(e);
                return false;
            }
        }
        Log.e(str, "mAudioService is null");
        return false;
    }

    public boolean suspendEngine() {
        String str = TAG;
        Log.i(str, "suspendEngine");
        if (this.mAudioService != null) {
            try {
                this.mAudioService.suspendEngine(this.mAudioServiceCallBacks);
                return true;
            } catch (RemoteException e) {
                catchDOEx(e);
                return false;
            }
        }
        Log.e(str, "mAudioService is null");
        return false;
    }

    public boolean resumeEngine() {
        String str = TAG;
        Log.i(str, "resumeEngine");
        if (this.mAudioService != null) {
            try {
                this.mAudioService.resumeEngine(this.mAudioServiceCallBacks);
                return true;
            } catch (RemoteException e) {
                catchDOEx(e);
                return false;
            }
        }
        Log.e(str, "mAudioService is null");
        return false;
    }

    public void fastBackward() {
        String str = TAG;
        Log.i(str, "fastBackward");
        if (this.mAudioService != null) {
            try {
                this.mAudioService.fastBackward();
                return;
            } catch (RemoteException e) {
                catchDOEx(e);
                return;
            }
        }
        Log.e(str, "mAudioService is null");
    }

    public void fastForward() {
        String str = TAG;
        Log.i(str, "fastForward");
        if (this.mAudioService != null) {
            try {
                this.mAudioService.fastForward();
                return;
            } catch (RemoteException e) {
                catchDOEx(e);
                return;
            }
        }
        Log.e(str, "mAudioService is null");
    }

    public boolean isListPlayedFromMVR() {
        boolean ret = false;
        if (this.mAudioService == null) {
            return ret;
        }
        try {
            return this.mAudioService.isListPlayedFromMVR();
        } catch (RemoteException e) {
            catchDOEx(e);
            return ret;
        }
    }

    public boolean blockUiMediaNotification(boolean block, int flags) {
        String str = TAG;
        boolean succeeded = false;
        Log.i(str, "blockUiMediaNotification");
        if (this.mAudioService != null) {
            try {
                return this.mAudioService.blockUiMediaNotification(block, flags);
            } catch (RemoteException e) {
                catchDOEx(e);
                return succeeded;
            }
        }
        Log.e(str, "mAudioService is null");
        return succeeded;
    }
}
