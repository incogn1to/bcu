package com.parrot.platform.provider;

import android.net.Uri;
import com.parrot.asteroid.audio.service.Metadata;
import com.parrot.platform.provider.MediaStore.Audio.AlbumColumns;
import com.parrot.platform.provider.MediaStore.Audio.PlayerQueueColumns;
import com.parrot.platform.provider.MediaStore.Audio.SettingsColumns;
import com.parrot.platform.provider.MediaStore.Audio.VolumeColumns;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MediaStoreHelper {

    public static abstract class UriHelper {
        private final String baseUri;
        private Map<String, String> conditions = new HashMap();
        private boolean filterExactmatch = false;
        private String filterKey = null;
        private String filterValue = null;

        public abstract String[] getExaustiveProjection();

        public UriHelper(String baseUri) {
            this.baseUri = baseUri;
        }

        public String getBaseUri() {
            return this.baseUri;
        }

        /* Access modifiers changed, original: protected */
        public void setFilter(String filterKey, String filterValue, boolean filterExactmatch) {
            this.filterKey = filterKey;
            this.filterValue = filterValue;
            this.filterExactmatch = filterExactmatch;
        }

        public void setCondition(String conditionKey, int conditionValue) {
            this.conditions.put(conditionKey, Integer.toString(conditionValue));
        }

        public void setCondition(String conditionKey, String conditionValue) {
            this.conditions.put(conditionKey, conditionValue);
        }

        public Uri getUri() {
            StringBuilder sb = new StringBuilder();
            sb.append("content://");
            sb.append(MediaStore.AUTHORITY);
            sb.append("/");
            sb.append(this.baseUri);
            return Uri.parse(sb.toString());
        }

        public String getSelection() {
            StringBuilder sb = new StringBuilder();
            boolean first = true;
            for (String key : this.conditions.keySet()) {
                if (!first) {
                    sb.append(" AND ");
                }
                sb.append(key);
                sb.append("=?");
                first = false;
            }
            if (this.filterKey != null) {
                sb.append(" WHERE ");
                sb.append(this.filterKey);
                sb.append(" LIKE ?");
                if (this.filterExactmatch) {
                    sb.append("%");
                }
            }
            return sb.toString();
        }

        public String[] getSelectionArgs() {
            Collection<String> strings = this.conditions.values();
            int argsCount = strings.size();
            if (this.filterKey != null) {
                argsCount++;
            }
            String[] args = new String[argsCount];
            int offset = 0;
            for (String value : strings) {
                int offset2 = offset + 1;
                args[offset] = value;
                offset = offset2;
            }
            if (offset < argsCount) {
                args[offset] = this.filterValue;
            }
            return args;
        }
    }

    public static class AlbumsUri extends UriHelper {
        public AlbumsUri(String baseUri) {
            super(baseUri);
        }

        /* Access modifiers changed, original: 0000 */
        public void filterAlbumName(String name, boolean exactmatch) {
            setFilter("album_name", name, exactmatch);
        }

        public void setAlbumId(int albumId) {
            setCondition("album_id", albumId);
        }

        public void setArtistId(int artistId) {
            setCondition("artist_id", artistId);
        }

        public void setGenreId(int genreId) {
            setCondition("genre_id", genreId);
        }

        public void setComposerId(int composerId) {
            setCondition("composer_id", composerId);
        }

        public String[] getExaustiveProjection() {
            return new String[]{"_id", AlbumColumns.NAME, "artist", AlbumColumns.GENRE};
        }
    }

    public static class ArtistsUri extends UriHelper {
        public ArtistsUri(String baseUri) {
            super(baseUri);
        }

        /* Access modifiers changed, original: 0000 */
        public void filterArtistName(String name, boolean exactmatch) {
            setFilter("artist_name", name, exactmatch);
        }

        public void setArtistId(int albumId) {
            setCondition("artist_id", albumId);
        }

        public void setGenreId(int genreId) {
            setCondition("genre_id", genreId);
        }

        public String[] getExaustiveProjection() {
            return new String[]{"_id", "artist"};
        }
    }

    public static class ComposersUri extends UriHelper {
        public ComposersUri(String baseUri) {
            super(baseUri);
        }

        /* Access modifiers changed, original: 0000 */
        public void filterComposerName(String name, boolean exactmatch) {
            setFilter("composer_name", name, exactmatch);
        }

        public String[] getExaustiveProjection() {
            return new String[]{"_id", "name"};
        }
    }

    public static class DirectoriesUri extends UriHelper {
        public DirectoriesUri(String baseUri) {
            super(baseUri);
        }

        /* Access modifiers changed, original: 0000 */
        public void filterDirectoryName(String name, boolean exactmatch) {
            setFilter("directory_name", name, exactmatch);
        }

        public String[] getExaustiveProjection() {
            return new String[]{"_id", "name", "type"};
        }

        public void setPath(String path) {
            setCondition("dir_path", path);
        }
    }

    public static class FlatDirectoriesUri extends UriHelper {
        public FlatDirectoriesUri(String baseUri) {
            super(baseUri);
        }

        /* Access modifiers changed, original: 0000 */
        public void filterFlatDirectoryName(String name, boolean exactmatch) {
            setFilter("flatdirectory_name", name, exactmatch);
        }

        public String[] getExaustiveProjection() {
            return new String[]{"_id", "name", "type"};
        }
    }

    public static class GenresUri extends UriHelper {
        public GenresUri(String baseUri) {
            super(baseUri);
        }

        /* Access modifiers changed, original: 0000 */
        public void filterGenreName(String name, boolean exactmatch) {
            setFilter("genre_name", name, exactmatch);
        }

        public void setGenreId(int genreId) {
            setCondition("genre_id", genreId);
        }

        public String[] getExaustiveProjection() {
            return new String[]{"_id", "name"};
        }
    }

    public static class PlayerQueueUri extends UriHelper {
        public PlayerQueueUri(String baseUri) {
            super(baseUri);
        }

        public String[] getExaustiveProjection() {
            return new String[]{"_id", "title", PlayerQueueColumns.SOURCE};
        }
    }

    public static class PlaylistsUri extends UriHelper {
        public PlaylistsUri(String baseUri) {
            super(baseUri);
        }

        /* Access modifiers changed, original: 0000 */
        public void filterPlaylistName(String name, boolean exactmatch) {
            setFilter("playlist_name", name, exactmatch);
        }

        public String[] getExaustiveProjection() {
            return new String[]{"_id", "name"};
        }
    }

    public static class SettingsUri extends UriHelper {
        public SettingsUri(String baseUri) {
            super(baseUri);
        }

        public String[] getExaustiveProjection() {
            return new String[]{"_id", "all", SettingsColumns.EMPTY, SettingsColumns.ROOT};
        }
    }

    public static class SourcesUri extends UriHelper {
        public SourcesUri() {
            super(Metadata.NO_COVER);
        }

        public String[] getExaustiveProjection() {
            return new String[]{"_id", "id", "name", "type", VolumeColumns.TYPE_STR, VolumeColumns.SUB_TYPE, VolumeColumns.DB_STATE, VolumeColumns.DB_STATE_STR, VolumeColumns.STATE, VolumeColumns.STATE_STR, VolumeColumns.CONNEXION_TYPE, VolumeColumns.CONNEXION_TYPE_STR, VolumeColumns.CONNEXION_STATE, VolumeColumns.RESTORE_STATE, VolumeColumns.RESTORE_STATE_STR, VolumeColumns.SUPPORT_PLAY_CONTROL, VolumeColumns.SUPPORT_METADATA, VolumeColumns.SUPPORT_TRACK_POSITION, VolumeColumns.SUPPORT_BROWSING, VolumeColumns.SUPPORT_RANDOM_SONG, VolumeColumns.SUPPORT_RANDOM_ALBUM, VolumeColumns.SUPPORT_REPEAT, VolumeColumns.SUPPORT_REPEAT_EXTENSIONS};
        }

        public void setFilter(String sourceFilter) {
            setFilter("source_id", sourceFilter, false);
        }
    }

    public static class TracksUri extends UriHelper {
        public TracksUri(String baseUri) {
            super(baseUri);
        }

        /* Access modifiers changed, original: 0000 */
        public void filterTitle(String title, boolean exactmatch) {
            setFilter("title", title, exactmatch);
        }

        public void setGenreId(int genreId) {
            setCondition("genre_id", genreId);
        }

        public void setAlbumId(int albumId) {
            setCondition("album_id", albumId);
        }

        public void setArtistId(int artistId) {
            setCondition("artist_id", artistId);
        }

        public void setPlaylistId(int playlistId) {
            setCondition("playlist_id", playlistId);
        }

        public void setComposerId(int composerId) {
            setCondition("composer_id", composerId);
        }

        public void setFlatDirectoryId(int flatDirectoryId) {
            setCondition("flatdirectory_id", flatDirectoryId);
        }

        public void setPath(String path) {
            setCondition("dir_path", path);
        }

        public String[] getExaustiveProjection() {
            return new String[]{"_id", "title"};
        }
    }

    public static TracksUri getTracks(String source) {
        return new TracksUri(source + "/audio/media");
    }

    public static TracksUri getTrack(String source, int trackId) {
        return new TracksUri(source + "/audio/media/" + Integer.toString(trackId));
    }

    public static TracksUri getTracksByPlaylist(String source, int playlistId) {
        return new TracksUri(source + "/audio/playlists/" + Integer.toString(playlistId) + "/members");
    }

    public static TracksUri getTrackByPlaylist(String source, int playlistId, int trackId) {
        return new TracksUri(source + "/audio/playlists/" + Integer.toString(playlistId) + "/members/" + Integer.toString(trackId));
    }

    public static GenresUri getGenres(String source) {
        return new GenresUri(source + "/audio/genres");
    }

    public static GenresUri getGenre(String source, int genreId) {
        return new GenresUri(source + "/audio/genres/" + Integer.toString(genreId));
    }

    public static AlbumsUri getAlbums(String source) {
        return new AlbumsUri(source + "/audio/albums");
    }

    public static AlbumsUri getAlbum(String source, int albumId) {
        return new AlbumsUri(source + "/audio/albums/" + Integer.toString(albumId));
    }

    public static AlbumsUri getAlbumsByArtist(String source, int artistId) {
        return new AlbumsUri(source + "/audio/artists/" + Integer.toString(artistId) + "/albums");
    }

    public static ArtistsUri getArtists(String source) {
        return new ArtistsUri(source + "/audio/artists");
    }

    public static ArtistsUri getArtist(String source, int artistId) {
        return new ArtistsUri(source + "/audio/artists/" + Integer.toString(artistId));
    }

    public static ArtistsUri getArtistsByGenre(String source, int genreId) {
        return new ArtistsUri(source + "/audio/genres/" + Integer.toString(genreId) + "/members");
    }

    public static ArtistsUri getArtistByGenre(String source, int genreId, int artistId) {
        return new ArtistsUri(source + "/audio/genres/" + Integer.toString(genreId) + "/members/" + Integer.toString(artistId));
    }

    public static PlaylistsUri getPlaylists(String source) {
        return new PlaylistsUri(source + "/audio/playlists");
    }

    public static PlaylistsUri getPlaylist(String source, int playlistId) {
        return new PlaylistsUri(source + "/audio/playlists/" + Integer.toString(playlistId));
    }

    public static ComposersUri getComposers(String source) {
        return new ComposersUri(source + "/audio/composers");
    }

    public static FlatDirectoriesUri getFlatDirectories(String source) {
        return new FlatDirectoriesUri(source + "/audio/directory/flat");
    }

    public static DirectoriesUri getDirectories(String source) {
        return new DirectoriesUri(source + "/audio/directory");
    }

    public static PlayerQueueUri getPlayerQueue() {
        return new PlayerQueueUri("S0/audio/playqueue");
    }

    public static SettingsUri getSettings() {
        return new SettingsUri("S0/settings");
    }

    public static SourcesUri getSources() {
        return new SourcesUri();
    }
}
