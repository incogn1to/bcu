package com.parrot.asteroid.audio.service;

public interface  MediaCallbackInterface {
    void onMetadataChanged(Metadata metadata, int i);

    void onPlayerStateChanged(PlayerInfo playerInfo);

    void onPlayerStatusAvailable(Source source, PlayerInfo playerInfo, Metadata metadata);

    void onSourceChanged(Source source);

    void onSourceCreation(Source source);

    void onSourceError(Source source);

    void onSourceReady(Source source);

    void onSourceRemoved(Source source);

    void onSourceUpdate(Source source);
}
