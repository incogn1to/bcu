package com.parrot.platform.provider;

public class MediaStore {
    public static final String AUTHORITY = "parrotmedia";

    public static final class Audio {

        public static final class AlbumColumns {
            public static final String ARTIST = "artist";
            public static final String GENRE = "genre";
            public static final String NAME = "album";
            public static final String NUMSONGS = "numsongs";
            public static final String _ID = "_id";
        }

        public static final class ArtistColumns {
            public static final String NAME = "artist";
            public static final String _ID = "_id";
        }

        public static final class ComposerColumns {
            public static final String NAME = "name";
            public static final String _ID = "_id";
        }

        public static final class DirectoryColumns {
            public static final String NAME = "name";
            public static final String TYPE = "type";
            public static final String UPNP_BBSEEK_SUPPORTED = "upnp_bbseek_supported";
            public static final String UPNP_READABLE = "upnp_readable";
            public static final String UPNP_RTP_SUPPORTED = "upnp_rtp_supported";
            public static final String UPNP_TBSEEK_SUPPORTED = "upnp_tbseek_supported";
            public static final String _ID = "_id";
        }

        public static final class FlatDirectoryColumns {
            public static final String NAME = "name";
            public static final String TYPE = "type";
            public static final String _ID = "_id";
        }

        public static final class GenreColumns {
            public static final String NAME = "name";
            public static final String _ID = "_id";
        }

        public static final class PlayerQueueColumns {
            public static final String SOURCE = "sourceId";
            public static final String TITLE = "title";
            public static final String _ID = "_id";
        }

        public static final class PlaylistColumns {
            public static final String NAME = "name";
            public static final String _ID = "_id";
        }

        public static final class SettingsColumns {
            public static final String ALL = "all";
            public static final String EMPTY = "empty";
            public static final String ROOT = "root";
            public static final String _ID = "_id";
        }

        public static final class TrackColumns {
            public static final String ALBUM = "track_album";
            public static final String ARTIST = "track_artist";
            public static final String ARTWORK_PATH = "artwork_path";
            public static final String GENRE = "track_genre";
            public static final String PATH = "path";
            public static final String TITLE = "title";
            public static final String TRACK_NUMBER = "track_number";
            public static final String _ID = "_id";
        }

        public static final class VolumeColumns {
            public static final String CONNEXION_STATE = "connexion_state";
            public static final String CONNEXION_TYPE = "connexion_type";
            public static final String CONNEXION_TYPE_STR = "connexion_type_str";
            public static final String DB_STATE = "db_state";
            public static final String DB_STATE_STR = "db_state_str";
            public static final String ID = "id";
            public static final int MULTIMEDIA_SOURCE_CNX_TYPE_INVALID = 3;
            public static final int MULTIMEDIA_SOURCE_CNX_TYPE_PERMANENT = 0;
            public static final int MULTIMEDIA_SOURCE_CNX_TYPE_REMOVABLE = 1;
            public static final int MULTIMEDIA_SOURCE_CNX_TYPE_STREAMING = 2;
            public static final int MULTIMEDIA_SOURCE_DB_STATE_BUILDING = 2;
            public static final int MULTIMEDIA_SOURCE_DB_STATE_CLOSED = 1;
            public static final int MULTIMEDIA_SOURCE_DB_STATE_NONE = 0;
            public static final int MULTIMEDIA_SOURCE_DB_STATE_UPDATED = 5;
            public static final int MULTIMEDIA_SOURCE_DB_STATE_UPDATED_NOCHG = 4;
            public static final int MULTIMEDIA_SOURCE_DB_STATE_UPDATING = 3;
            public static final int MULTIMEDIA_SOURCE_RESTORE_STATE_ACTIVE = 4;
            public static final int MULTIMEDIA_SOURCE_RESTORE_STATE_BOOTCTX = 1;
            public static final int MULTIMEDIA_SOURCE_RESTORE_STATE_IMPLICIT = 3;
            public static final int MULTIMEDIA_SOURCE_RESTORE_STATE_LASTCTX = 2;
            public static final int MULTIMEDIA_SOURCE_RESTORE_STATE_NONE = 0;
            public static final int MULTIMEDIA_SOURCE_STATE_ACTIVE = 4;
            public static final int MULTIMEDIA_SOURCE_STATE_ERROR = 5;
            public static final int MULTIMEDIA_SOURCE_STATE_INCOMING = 3;
            public static final int MULTIMEDIA_SOURCE_STATE_MISSING = 0;
            public static final int MULTIMEDIA_SOURCE_STATE_MOUNTING = 1;
            public static final int MULTIMEDIA_SOURCE_STATE_READY = 2;
            public static final int MULTIMEDIA_SOURCE_STATE_REMOVED = 6;
            public static final int MULTIMEDIA_SOURCE_TYPE_A2DP = 2;
            public static final int MULTIMEDIA_SOURCE_TYPE_INV = -1;
            public static final int MULTIMEDIA_SOURCE_TYPE_INVALID = 0;
            public static final int MULTIMEDIA_SOURCE_TYPE_IPOD = 8;
            public static final int MULTIMEDIA_SOURCE_TYPE_LINEIN = 9;
            public static final int MULTIMEDIA_SOURCE_TYPE_LOCAL = 3;
            public static final int MULTIMEDIA_SOURCE_TYPE_MTP = 6;
            public static final int MULTIMEDIA_SOURCE_TYPE_SD = 5;
            public static final int MULTIMEDIA_SOURCE_TYPE_SMARTRADIO = 13;
            public static final int MULTIMEDIA_SOURCE_TYPE_TUNER = 7;
            public static final int MULTIMEDIA_SOURCE_TYPE_UNKNOWN = 1;
            public static final int MULTIMEDIA_SOURCE_TYPE_UPNP = 11;
            public static final int MULTIMEDIA_SOURCE_TYPE_USB = 4;
            public static final int MULTIMEDIA_SOURCE_TYPE_VSC = 10;
            public static final int MULTIMEDIA_SOURCE_TYPE_WEBRADIO = 12;
            public static final String NAME = "name";
            public static final String RESTORE_STATE = "restore_state";
            public static final String RESTORE_STATE_STR = "restore_state_str";
            public static final String STATE = "state";
            public static final String STATE_STR = "state_str";
            public static final String SUB_TYPE = "sub_type";
            public static final String SUPPORT_BROWSING = "support_browsing";
            public static final String SUPPORT_METADATA = "support_metadata";
            public static final String SUPPORT_PLAY_CONTROL = "support_play_control";
            public static final String SUPPORT_RANDOM_ALBUM = "support_random_album";
            public static final String SUPPORT_RANDOM_SONG = "support_random_song";
            public static final String SUPPORT_REPEAT = "support_repeat";
            public static final String SUPPORT_REPEAT_EXTENSIONS = "support_repeat_extensions";
            public static final String SUPPORT_TRACK_POSITION = "support_track_position";
            public static final String TYPE = "type";
            public static final String TYPE_STR = "type_str";
            public static final String _ID = "_id";
        }
    }
}
