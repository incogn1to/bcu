#!/system/bin/sh

key1code="0001 0002 00000000"
key2code="0001 0003 00000000"
key3code="0001 0004 00000000"
key4code="0001 0005 00000000"
key5code="0001 0006 00000000"
key6code="0001 0007 00000000"
key7code="0001 0008 00000000"
key8code="0001 0009 00000000"
key9code="0001 000a 00000000"
key0code="0001 000b 00000000"

launch()
{
    monkey -p $1 -c android.intent.category.LAUNCHER 1
}

adb_on()
{
    setprop service.adb.tcp.port 7777   
    setprop persist.service.adb.enable 1
}

sd_music()
{
    am start -W -c android.intent.category.DEFAULT -a com.parrot.source_button
    sleep 0.3
    input keyevent 66
}

player()
{
    am start -c android.intent.category.DEFAULT -a com.parrot.mediaplayer
}

while :
do
    input_event=/dev/input/event3
    if grep Keyboard /sys/class/input/event2/device/name; then
        input_event=/dev/input/event2
    fi

    code=$(getevent -q -c1 $input_event)
    case "$code" in
        "$key2code" )  sd_music ;;
        "$key3code" )  launch com.parrot.activesources ;;
        "$key6code" )  player ;;
        "$key8code" )  adb_on ;;
    esac
done

exit 0

