package lv.car.bcu;

import android.app.ActivityManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.pm.ResolveInfo;
import android.provider.Settings;

import com.parrot.asteroid.audio.service.Metadata;
import com.parrot.asteroid.audio.service.PlayerInfo;
import com.parrot.asteroid.audio.service.Source;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    static final public int PLAYER_META = 0;
    static final public int PLAYER_STATE = 1;
    static final public int WEATHER = 2;
    static final public int LINE_IN = 3;

    private AudioMgr mAudioMgr;
    private AppMgr mAppMgr;
    private WeatherMgr mWeatherMgr;
    private ToastMgr mToastMgr;
    private JSONObject mConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String font = getApplicationContext().getResources().getString(R.string.FONT);
        final Typeface mTypeface = Typeface.createFromAsset(getApplicationContext().getAssets(), font);

        TextView tv = (TextView) findViewById(R.id.textTime);
        tv.setTypeface(mTypeface);
        tv = (TextView) findViewById(R.id.textCity);
        tv.setTypeface(mTypeface);
        tv = (TextView) findViewById(R.id.textTrackTime);
        tv.setTypeface(mTypeface);
        tv = (TextView) findViewById(R.id.textTrack);
        tv.setTypeface(mTypeface);
        tv = (TextView) findViewById(R.id.textTemp);
        tv.setTypeface(mTypeface);

        updateTime();
        mWeatherMgr = new WeatherMgr(getApplicationContext(),myHandler);
        mWeatherMgr.setStoredCurrentWeather();
        mWeatherMgr.startGpsUpdates();
        mAudioMgr = new AudioMgr(getApplicationContext(),myHandler);
        mAppMgr = new AppMgr(getApplicationContext());
        mToastMgr = new ToastMgr(getApplicationContext());

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateTime();
                    }
                });
            }
        }, 10000, 10000);

        try {
            FileMgr cfg = new FileMgr(getApplicationContext());
            final String file = getApplicationContext().getResources().getString(R.string.CONFIG);
            final String config = cfg.readFromExtFile(file);
            mConfig = new JSONObject(config);
        } catch (Throwable t) {
            t.printStackTrace();
        }

        Settings.System.putInt(
                getContentResolver(),
                Settings.System.ACCELEROMETER_ROTATION,
                0
        );

        Settings.System.putInt(
                getContentResolver(),
                Settings.System.USER_ROTATION,
                Surface.ROTATION_90
        );

        mWeatherMgr.requestCurrentWeatherUpdate();
    }

    @Override
    public void onResume() {
        mToastMgr.cancel();
        mWeatherMgr.setStoredCurrentWeather();
        mWeatherMgr.requestCurrentWeatherUpdate();
        super.onResume();
    }

    @Override
    public void onPause() {
        mToastMgr.cancel();
        super.onPause();
    }

    private void showPlayer() {
        try {
            Source src = mAudioMgr.getCurrentSource();
            if (src == null) {
                Log.d(TAG, "no active source");
                showSources();
            } else {
                Log.d(TAG, "will show playlist");
                mAppMgr.showParrotPlayList(src);
                //mAppMgr.showPlayList(src);
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void showSources() {
        final ArrayList<Source> audioSources = mAudioMgr.getSources();
        List<String> labelList = new ArrayList<String>();
        List<Drawable> iconList = new ArrayList<Drawable>();

        for (Source src : audioSources) {
            final String label = src.getSourceName();
            final Drawable icon = mAudioMgr.getIcon(src.getType());
            labelList.add(label);
            iconList.add(icon);
        }

        final SpinnerAdapter adapter = new SpinnerAdapter(this, labelList, iconList);
        final CustomSpinner spinner = (CustomSpinner) findViewById(R.id.applicationsSpinner);
        spinner.setAdapter(adapter);
        spinner.performClick(); // to activate
        spinner.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Source src = audioSources.get(position);
                Log.d(TAG, "position " + position + "source: " + src.getSourceName() + " id: " + src.getId());
                Source current = mAudioMgr.getCurrentSource();
                Log.d(TAG, "current source: " + current.getSourceName() + " id: " + current.getId());
                if (current.getIntId() != src.getIntId()) {
                    Log.d(TAG, "will change source to " + src.getSourceName());
                    mAudioMgr.playSource(src);
                } else {
                    Log.d(TAG, "will show play list");
                    //mAppMgr.showPlayList(src);
                    mAppMgr.showParrotPlayList(src);
                }
            }
        });
    }

    private void showAllApps() {
        final List<ResolveInfo> appList = mAppMgr.getAppsForLaunch();
        List<String> labelList = new ArrayList<String>();
        List<Drawable> iconList = new ArrayList<Drawable>();

        for (ResolveInfo app : appList) {
            final String label = mAppMgr.getLabel(app.activityInfo.packageName);
            final Drawable icon = mAppMgr.getIcon(app.activityInfo.packageName);
            labelList.add(label);
            iconList.add(icon);
        }

        final SpinnerAdapter adapter = new SpinnerAdapter(this, labelList, iconList);
        final CustomSpinner spinner = (CustomSpinner) findViewById(R.id.applicationsSpinner);
        spinner.setAdapter(adapter);
        spinner.performClick(); // to activate spinner
        spinner.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                mAppMgr.launchApk(appList.get(position));
            }
        });
    }

    private void showRecentApps() {
        final List<ActivityManager.RecentTaskInfo> recentList = mAppMgr.getRecentTasks();
        List<String> labelList = new ArrayList<String>();
        List<Drawable> iconList = new ArrayList<Drawable>();

        for (ActivityManager.RecentTaskInfo app : recentList) {
            final String label = mAppMgr.getLabel(app.baseIntent.getComponent().getPackageName());
            final Drawable icon = mAppMgr.getIntentIcon(app.baseIntent);
            labelList.add(label);
            iconList.add(icon);
        }

        SpinnerAdapter adapter = new SpinnerAdapter(this, labelList, iconList);
        final CustomSpinner spinner = (CustomSpinner) findViewById(R.id.applicationsSpinner);
        spinner.setAdapter(adapter);
        spinner.performClick();
        spinner.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                mAppMgr.activateTask(recentList.get(position));
            }
        });
    }

    void executeShell(String command) {
        try {
            Process process = Runtime.getRuntime().exec(command);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    boolean processKey(int code) {
        try {
            if (mConfig == null) return false;
            final String keyName = code2name(code);
            if (keyName.isEmpty()) return false;
            final JSONObject key = mConfig.getJSONObject("keys").getJSONObject(keyName);
            if (key == null) return false;
            final String action = key.getString("action");
            if (action.equals("shell")) {
                final String cmd = key.getString("cmd");
                executeShell(cmd);
                return true;
            } else if (action.equals("app")) {
                final String pak = key.getString("package");
                final String category = key.getString("category");
                if (category.isEmpty()) {
                    mAppMgr.launchApp(pak);
                } else {
                    mAppMgr.launchApp(pak,category);
                }
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.d(TAG, "keyUp[" + keyCode + "]");
        switch (keyCode) {
            case KeyEvent.KEYCODE_SEARCH: {
                showPlayer();
                return true;
            }
            case KeyEvent.KEYCODE_SETTINGS:
            case KeyEvent.KEYCODE_APP_SWITCH:
            case KeyEvent.KEYCODE_MENU: {
                mAppMgr.startSettingsApp();
                return true;
            }
            case KeyEvent.KEYCODE_DPAD_DOWN: {
                showAllApps();
                return true;
            }
            case KeyEvent.KEYCODE_DPAD_UP: {
                showRecentApps();
                return true;
            }
            case KeyEvent.KEYCODE_BACK: {
                moveTaskToBack(true);
                return true;
            }
            case KeyEvent.KEYCODE_DPAD_CENTER: {
                showSources();
                return true;
            }
            // in my case
            // 7 was bound to search key
            // 8 was bound to camera key
            // 9 was bound to settings key
            // 0 was bound to home key
            // 7,9,0 are handled on OS level
            case KeyEvent.KEYCODE_CAMERA: {
                showWeather();
                return true;
            }
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_3:
            case KeyEvent.KEYCODE_4:
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_6:
            case KeyEvent.KEYCODE_7:
            case KeyEvent.KEYCODE_8:
            case KeyEvent.KEYCODE_9:
            case KeyEvent.KEYCODE_0: {
                processKey(keyCode);
                return super.onKeyUp(keyCode, event);
            }
            default: {
                return super.onKeyUp(keyCode, event);
            }
        }
    }

    private String code2name(int code) {
        switch (code) {
            case KeyEvent.KEYCODE_1:
                return "KEYCODE_1";
            case KeyEvent.KEYCODE_2:
                return "KEYCODE_2";
            case KeyEvent.KEYCODE_3:
                return "KEYCODE_3";
            case KeyEvent.KEYCODE_4:
                return "KEYCODE_4";
            case KeyEvent.KEYCODE_5:
                return "KEYCODE_5";
            case KeyEvent.KEYCODE_6:
                return "KEYCODE_6";
            case KeyEvent.KEYCODE_7:
                return "KEYCODE_7";
            case KeyEvent.KEYCODE_8:
                return "KEYCODE_8";
            case KeyEvent.KEYCODE_9:
                return "KEYCODE_9";
            case KeyEvent.KEYCODE_0:
                return "KEYCODE_0";
            default:
                return "";
        }
    }

    private void showWeather() {
        mAppMgr.showWeather();
    }

    public void clickLayout(View view) {
        Log.d(TAG, "Layouts was clicked");
        showAllApps();
    }

    public void clickWeather(View view) {
        Log.d(TAG, "Weather was clicked");
        showWeather();
    }

    public void clickClock(View view) {
        Log.d(TAG, "Clock was clicked");
        //mAppMgr.showPlayList(mAudioMgr.getCurrentSource());
    }

    public void clickPlayer(View view) {
        Log.d(TAG, "Player was clicked");
        showPlayer();
    }

    private void updateTime() {
        try {
            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            final String formattedDate = dateFormat.format(date);
            TextView timeView = (TextView) findViewById(R.id.textTime);
            timeView.setText(formattedDate);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void updateWeather(JSONObject json) {
        try {
            final String temp = json.getJSONObject("main").getInt("temp") + "°C";
            TextView view = (TextView) findViewById(R.id.textTemp);
            view.setText(temp);

            final String city = json.getString("name");
            view = (TextView) findViewById(R.id.textCity);
            view.setText(city);

            final int code = json.getJSONArray("weather").getJSONObject(0).getInt("id");
            final int id = WeatherMgr.getPictureId(code);
            ImageView pic = (ImageView) findViewById(R.id.imageWeather);
            pic.setImageResource(id);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void lineIn() {
        TextView view = (TextView) findViewById(R.id.textTrackTime);
        view.setText("");
        view = (TextView) findViewById(R.id.textTrack);
        view.setText("Line in");
    }

    private void updatePlayerState(PlayerInfo data) {
        try {
            switch (data.getState()) {
                case PlayerInfo.AUDIO_PLAY: {
                    final int position = data.getPosition();
                    final int duration = data.getDuration();
                    final String txt =
                            String.format("%01d:%02d/%01d:%02d", position / 60, position % 60, duration / 60, duration % 60);
                    TextView view = (TextView) findViewById(R.id.textTrackTime);
                    view.setText(txt);
                    break;
                }
                case PlayerInfo.AUDIO_STOP:
                case PlayerInfo.AUDIO_PAUSE:
                case PlayerInfo.AUDIO_INVALID:
                default:
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void updatePlayerMeta(Metadata data) {
        try {
            Log.d(TAG, "meta data: " + data.toString());
            final String artist = data.getArtist();
            // remove file format
            final String title = data.getTitle()
                    .replace(".mp3", "")
                    .replace(".wma","")
                    .replace(".ogg","");

            // check if artist name is included in title
            final boolean artist_included = title.toLowerCase().contains(artist.toLowerCase());
            String txt = "N/A";
            if (artist.isEmpty() || artist_included) {
                // if yes, we will just show title
                txt = title;
            } else {
                // if not, we will show artist and title
                txt = artist + " - " + title;
            }

            TextView view = (TextView) findViewById(R.id.textTrack);
            view.setText(txt);

            if (mAppMgr.showToast()) {
                mToastMgr.setText(txt,false);
                mToastMgr.show();
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private final Handler myHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            try {
                int cmd = msg.what;
                switch (cmd) {
                    case WEATHER: {
                        final JSONObject weatherData = (JSONObject) msg.obj;
                        updateWeather(weatherData);
                        break;
                    }
                    case PLAYER_STATE: {
                        final PlayerInfo data = (PlayerInfo) msg.obj;
                        updatePlayerState(data);
                        break;
                    }
                    case PLAYER_META: {
                        final Metadata data = (Metadata) msg.obj;
                        updatePlayerMeta(data);
                        break;
                    }
                    case LINE_IN: {
                        lineIn();
                        break;
                    }
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    };
}
