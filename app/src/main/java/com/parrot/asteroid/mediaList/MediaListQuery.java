package com.parrot.asteroid.mediaList;

import android.content.Context;
import android.net.Uri;
import com.parrot.asteroid.audio.service.Metadata;
import java.util.ArrayList;

public abstract class MediaListQuery implements Cloneable {
    private String mCategory;
    protected Context mContext;
    private String mFilter;
    protected MediaListQueryListener mMediaListQueryListener;
    protected String[] mProjection = null;
    protected boolean mRecursive = false;
    protected String mRequestedColumn;
    private ArrayList<QuerySelection> mSelectionList = new ArrayList();
    private String mSourceId;
    protected String mTitle = null;

    public abstract void finish();

    public abstract boolean launchRequest();

    MediaListQuery(Context context, String sourceId, String category, String requestedColumn) {
        this.mCategory = category;
        this.mSourceId = sourceId;
        this.mRequestedColumn = requestedColumn;
        this.mContext = context;
    }

    /* Access modifiers changed, original: 0000 */
    public void setMediaListQueryListener(MediaListQueryListener mediaListqueryListener) {
        this.mMediaListQueryListener = mediaListqueryListener;
    }

    /* Access modifiers changed, original: protected */
    public Uri buildRequest() {
        StringBuilder path = new StringBuilder();
        path.append("content://parrotmedia/");
        path.append(this.mSourceId);
        path.append("/audio/");
        path.append(this.mCategory);
        return Uri.parse(path.toString());
    }

    /* Access modifiers changed, original: protected */
    public String buildWhere() {
        String str = " AND ";
        if (this.mSelectionList == null && this.mFilter == null) {
            return null;
        }
        String str2;
        StringBuilder where = new StringBuilder();
        int i = 0;
        if (this.mSelectionList != null) {
            String oldName = Metadata.NO_COVER;
            while (i < this.mSelectionList.size()) {
                String newName = ((QuerySelection) this.mSelectionList.get(i)).getName();
                if (!oldName.equals(newName)) {
                    oldName = newName;
                    if (i != 0) {
                        str2 = " AND ";
                        where.append(str);
                    }
                    where.append(newName);
                    where.append("=");
                }
                where.append(((QuerySelection) this.mSelectionList.get(i)).getValue());
                i++;
            }
        }
        if (this.mFilter != null) {
            if (i != 0) {
                str2 = " AND ";
                where.append(str);
            }
            where.append("WHERE ");
            where.append(this.mRequestedColumn);
            where.append(" LIKE ");
            where.append(this.mFilter);
            where.append("%");
        }
        return where.toString();
    }

    public void setFilter(String filter) {
        this.mFilter = filter;
    }

    public void addSelectionList(ArrayList<QuerySelection> selectionList) {
        if (selectionList != null) {
            this.mSelectionList.addAll(selectionList);
        }
    }

    public void addSelection(QuerySelection selection) {
        this.mSelectionList.add(selection);
    }

    /* Access modifiers changed, original: protected */
    public String getOrder() {
        return this.mRequestedColumn + " ASC";
    }

    /* Access modifiers changed, original: protected */
    public String[] getProjection() {
        int size = 2;
        if (this.mProjection != null) {
            size = 2 + this.mProjection.length;
        }
        if (this.mTitle != null) {
            size++;
        }
        String[] projections = new String[size];
        int i = 0 + 1;
        projections[0] = "_id";
        int i2 = i + 1;
        projections[i] = this.mRequestedColumn;
        if (this.mProjection != null) {
            String[] arr$ = this.mProjection;
            int len$ = arr$.length;
            int i$ = 0;
            i = i2;
            while (i$ < len$) {
                i2 = i + 1;
                projections[i] = arr$[i$];
                i$++;
                i = i2;
            }
            i2 = i;
        }
        if (this.mTitle != null) {
            i = i2 + 1;
            projections[i2] = this.mTitle;
            i2 = i;
        }
        return projections;
    }

    public void setSpecificProjectionfield(String[] projection) {
        this.mProjection = projection;
    }

    public Object clone() {
        MediaListQuery query = null;
        try {
            query = (MediaListQuery) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        query.mSelectionList = new ArrayList();
        query.mSelectionList.addAll(this.mSelectionList);
        query.mMediaListQueryListener = null;
        return query;
    }

    /* Access modifiers changed, original: 0000 */
    public String getCategory() {
        return this.mCategory;
    }

    public ArrayList<QuerySelection> getSelections() {
        return this.mSelectionList;
    }

    public void clearSelection() {
        this.mSelectionList.clear();
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public boolean isRecursive() {
        return this.mRecursive;
    }

    public void setRecursive(boolean isRecursive) {
        this.mRecursive = isRecursive;
    }
}
