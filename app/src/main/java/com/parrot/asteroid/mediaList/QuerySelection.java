package com.parrot.asteroid.mediaList;

class QuerySelection {
    private String mName;
    private String mValue;

    public QuerySelection(String name, long value) {
        setName(name);
        setValue(value);
    }

    public QuerySelection(String name, String value) {
        setName(name);
        setValue(value);
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getName() {
        return this.mName;
    }

    public void setValue(String value) {
        this.mValue = value;
    }

    public String getValue() {
        return this.mValue;
    }

    public void setValue(long value) {
        this.mValue = new Long(value).toString();
    }
}
