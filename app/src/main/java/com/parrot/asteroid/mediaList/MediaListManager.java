package com.parrot.asteroid.mediaList;

import com.parrot.asteroid.Manager;
import com.parrot.asteroid.tools.AsteroidObservable;
import com.parrot.hsti.SysListener;

abstract class MediaListManager extends Manager implements MediaListManagerInterface, SysListener {
    AsteroidObservable<MediaListObserverInterface> mMediaListObserverList = new AsteroidObservable();

    MediaListManager() {
    }

    public boolean addObserver(MediaListObserverInterface mediaListObserver) {
        boolean addObserver;
        synchronized (this.mMediaListObserverList) {
            addObserver = this.mMediaListObserverList.addObserver(mediaListObserver);
        }
        return addObserver;
    }

    public boolean deleteObserver(MediaListObserverInterface mediaListObserver) {
        boolean deleteObserver;
        synchronized (this.mMediaListObserverList) {
            deleteObserver = this.mMediaListObserverList.deleteObserver(mediaListObserver);
        }
        return deleteObserver;
    }

    /* Access modifiers changed, original: protected */
    public void notifyOnTooLongAdapter() {
        synchronized (this.mMediaListObserverList) {
            for (MediaListObserverInterface observer : this.mMediaListObserverList.getObserverList()) {
                observer.onTooLongRequest();
            }
        }
    }

    /* Access modifiers changed, original: protected */
    public void notifyOnUpdateAdapter() {
        synchronized (this.mMediaListObserverList) {
            for (MediaListObserverInterface observer : this.mMediaListObserverList.getObserverList()) {
                observer.onUpdateAdapter();
            }
        }
    }

    /* Access modifiers changed, original: protected */
    public void notifyOnUpdatePosition(int position) {
        synchronized (this.mMediaListObserverList) {
            for (MediaListObserverInterface observer : this.mMediaListObserverList.getObserverList()) {
                observer.onUpdatePosition(position);
            }
        }
    }

    /* Access modifiers changed, original: protected */
    public void notifyOnSourceRemoved() {
        synchronized (this.mMediaListObserverList) {
            for (MediaListObserverInterface observer : this.mMediaListObserverList.getObserverList()) {
                observer.onSourceRemoved();
            }
        }
    }
}
