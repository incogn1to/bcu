package com.parrot.asteroid.media;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.parrot.asteroid.ServiceStatusInterface;
import com.parrot.asteroid.ServiceStatusInterface.Status;
import com.parrot.asteroid.audio.service.AudioAccess;
import com.parrot.asteroid.audio.service.MediaCallbackInterface;
import com.parrot.asteroid.audio.service.Metadata;
import com.parrot.asteroid.audio.service.PlayerInfo;
import com.parrot.asteroid.audio.service.Source;
import java.util.ArrayList;
import java.util.Iterator;

final class MediaManagerService extends MediaManager implements MediaCallbackInterface, ServiceStatusInterface {
    private static final boolean DEBUG = true;
    private static final boolean DEBUG_ALL = false;
    private static final String TAG = "MediaManagerService";
    private AudioAccess mAudioAccess;
    private CallBackHandler mCallBacksHandler;
    private Context mContext;
    private boolean mListening = false;
    private PlayerInfo mPlayerInfo;
    private boolean mReady = false;

    private class CallBackHandler extends Handler {
        protected static final int CHANGE_METADATA_MSG = 3;
        protected static final int CHANGE_SRC_MSG = 1;
        protected static final int CREATING_SRC_MSG = 6;
        protected static final int ERROR_SRC_MSG = 9;
        protected static final int MANAGER_READY_MSG = 5;
        protected static final int PLAYER_INFO_MSG = 4;
        protected static final int PLAYER_STATUS_MSG = 2;
        protected static final int READY_SRC_MSG = 7;
        protected static final int REMOVED_SRC_MSG = 10;
        protected static final int UPDATED_SRC_MSG = 8;
        private int mMetadataFieldMask;

        private CallBackHandler() {
            this.mMetadataFieldMask = 0;
        }

        public void handleMessage(Message msg) {
            if (MediaManagerService.this.mListening || msg.what == 5) {
                Iterator i$;
                switch (msg.what) {
                    case 1:
                        Source src = (Source) msg.obj;
                        Log.i(MediaManagerService.TAG, "CHANGE_SRC_MSG : " + src.getId());
                        i$ = MediaManagerService.this.mObservers.iterator();
                        while (i$.hasNext()) {
                            ((MediaObserverInterface) i$.next()).onSourceChanged(src);
                        }
                        return;
                    case 2:
                        Log.i(MediaManagerService.TAG, "PLAYER_STATUS_MSG");
                        Bundle b = msg.getData();
                        Source source = (Source) b.get("src");
                        PlayerInfo playerInf = (PlayerInfo) b.get("ply");
                        Metadata meta = (Metadata) b.get("meta");
                        i$ = MediaManagerService.this.mObservers.iterator();
                        while (i$.hasNext()) {
                            ((MediaObserverInterface) i$.next()).onPlayerStatus(source, playerInf, meta);
                        }
                        MediaManagerService.this.mPlayerInfo = playerInf;
                        return;
                    case 3:
                        Metadata metadata = (Metadata) msg.obj;
                        this.mMetadataFieldMask |= msg.arg1;
                        if (hasMessages(3)) {
                            Log.v(MediaManagerService.TAG, "Multiple CHANGE_METADATA_MSG");
                            return;
                        }
                        i$ = MediaManagerService.this.mObservers.iterator();
                        while (i$.hasNext()) {
                            ((MediaObserverInterface) i$.next()).onMetadataChanged(metadata, this.mMetadataFieldMask);
                        }
                        this.mMetadataFieldMask = 0;
                        return;
                    case 4:
                        PlayerInfo playerInfo = (PlayerInfo) msg.obj;
                        if (playerInfo == null) {
                            Log.e(MediaManagerService.TAG, "playerInfo is null");
                            return;
                        }
                        int changes = playerInfo.getChanges(MediaManagerService.this.mPlayerInfo);
                        if (changes != 0) {
                            if ((changes & 1) > 0) {
                                i$ = MediaManagerService.this.mObservers.iterator();
                                while (i$.hasNext()) {
                                    ((MediaObserverInterface) i$.next()).onStateChanged(playerInfo);
                                }
                            }
                            if ((changes & 12) > 0) {
                                i$ = MediaManagerService.this.mObservers.iterator();
                                while (i$.hasNext()) {
                                    ((MediaObserverInterface) i$.next()).onModeChanged(playerInfo.getRepeatmode(), playerInfo.getRandommode());
                                }
                            }
                            if ((changes & 192) > 0) {
                                i$ = MediaManagerService.this.mObservers.iterator();
                                while (i$.hasNext()) {
                                    ((MediaObserverInterface) i$.next()).onTrackProgress(playerInfo.getPosition(), playerInfo.getDuration());
                                }
                            }
                            if ((changes & -206) > 0) {
                                i$ = MediaManagerService.this.mObservers.iterator();
                                while (i$.hasNext()) {
                                    ((MediaObserverInterface) i$.next()).onPlayerInfoChanged(playerInfo, changes);
                                }
                            }
                            MediaManagerService.this.mPlayerInfo = playerInfo;
                            return;
                        }
                        return;
                    case 5:
                        Status state = (Status) msg.obj;
                        Log.i(MediaManagerService.TAG, "Service ready :" + state);
                        switch (state) {
                            case SERVICE_CONNECTED:
                            case SERVICE_DISCONNECTED:
                                if (MediaManagerService.this.mReady) {
                                    MediaManagerService.this.mReady = false;
                                    MediaManagerService.this.notifyManagerReady(MediaManagerService.this.mReady, MediaManagerService.this);
                                    return;
                                }
                                return;
                            case SERVICE_READY:
                                if (!MediaManagerService.this.mReady) {
                                    MediaManagerService.this.mReady = true;
                                    MediaManagerService.this.notifyManagerReady(MediaManagerService.this.mReady, MediaManagerService.this);
                                    MediaManagerService.this.initRequest();
                                    return;
                                }
                                return;
                            default:
                                return;
                        }
                    case 6:
                        Source creatingSrc = (Source) msg.obj;
                        Log.i(MediaManagerService.TAG, "CREATING_SRC_MSG : " + creatingSrc.getId());
                        i$ = MediaManagerService.this.mObservers.iterator();
                        while (i$.hasNext()) {
                            ((MediaObserverInterface) i$.next()).onCreatingSource(creatingSrc);
                        }
                        return;
                    case 7:
                        Source readySrc = (Source) msg.obj;
                        Log.i(MediaManagerService.TAG, "READY_SRC_MSG : " + readySrc.getId());
                        i$ = MediaManagerService.this.mObservers.iterator();
                        while (i$.hasNext()) {
                            ((MediaObserverInterface) i$.next()).onSourceReady(readySrc);
                        }
                        return;
                    case 8:
                        Source updatedSrc = (Source) msg.obj;
                        Log.i(MediaManagerService.TAG, "UPDATED_SRC_MSG : " + updatedSrc.getId());
                        i$ = MediaManagerService.this.mObservers.iterator();
                        while (i$.hasNext()) {
                            ((MediaObserverInterface) i$.next()).onSourceUpdated(updatedSrc);
                        }
                        return;
                    case 9:
                        Source errorSrc = (Source) msg.obj;
                        Log.i(MediaManagerService.TAG, "ERROR_SRC_MSG : " + errorSrc.getId());
                        i$ = MediaManagerService.this.mObservers.iterator();
                        while (i$.hasNext()) {
                            ((MediaObserverInterface) i$.next()).onSourceError(errorSrc);
                        }
                        return;
                    case 10:
                        Source removedSrc = (Source) msg.obj;
                        Log.i(MediaManagerService.TAG, "REMOVED_SRC_MSG : " + removedSrc.getId());
                        i$ = MediaManagerService.this.mObservers.iterator();
                        while (i$.hasNext()) {
                            ((MediaObserverInterface) i$.next()).onSourceRemoved(removedSrc);
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public void destroy() {
            removeCallbacksAndMessages(null);
        }
    }

    public MediaManagerService(Context ctx) {
        Log.i(TAG, "init");
        this.mContext = ctx;
        this.mCallBacksHandler = new CallBackHandler();
        this.mAudioAccess = new AudioAccess(this.mContext, this);
    }

    public synchronized void destroy() {
        Log.i(TAG, "destroy");
        this.mAudioAccess.removeListener((MediaCallbackInterface) this);
        this.mAudioAccess.destroy();
        if (this.mCallBacksHandler != null) {
            this.mCallBacksHandler.destroy();
        }
        this.mCallBacksHandler = null;
        super.destroy();
    }

    private void initRequest() {
        String str = TAG;
        String str2 = TAG;
        Log.i(str, "initRequest");
        if (this.mReady) {
            this.mAudioAccess.askAllInfos();
            return;
        }
        str2 = TAG;
        Log.w(str, "Manager and Service are NOT yet Ready");
    }

    public void onServiceReady(Status state) {
        Log.i(TAG, "onServiceReady :" + state);
        this.mCallBacksHandler.sendMessage(this.mCallBacksHandler.obtainMessage(5, state));
    }

    public void onMetadataChanged(Metadata metadata, int changeFlag) {
        this.mCallBacksHandler.sendMessage(this.mCallBacksHandler.obtainMessage(3, changeFlag, 0, metadata));
    }

    public void onSourceChanged(Source source) {
        this.mCallBacksHandler.sendMessage(this.mCallBacksHandler.obtainMessage(1, source));
    }

    public void onPlayerStateChanged(PlayerInfo playerInfo) {
        this.mCallBacksHandler.sendMessage(this.mCallBacksHandler.obtainMessage(4, playerInfo));
    }

    public void onPlayerStatusAvailable(Source source, PlayerInfo playerInfo, Metadata metadata) {
        Log.i(TAG, "onPlayerStatusAvailable");
        Bundle b = new Bundle();
        b.putParcelable("src", source);
        b.putParcelable("ply", playerInfo);
        b.putParcelable("meta", metadata);
        Message msg = this.mCallBacksHandler.obtainMessage(2);
        msg.setData(b);
        this.mCallBacksHandler.sendMessage(msg);
    }

    public void onSourceCreation(Source creatingSource) {
        this.mCallBacksHandler.sendMessage(this.mCallBacksHandler.obtainMessage(6, creatingSource));
    }

    public void onSourceError(Source source) {
        this.mCallBacksHandler.sendMessage(this.mCallBacksHandler.obtainMessage(9, source));
    }

    public void onSourceReady(Source readySource) {
        this.mCallBacksHandler.sendMessage(this.mCallBacksHandler.obtainMessage(7, readySource));
    }

    public void onSourceRemoved(Source source) {
        this.mCallBacksHandler.sendMessage(this.mCallBacksHandler.obtainMessage(10, source));
    }

    public void onSourceUpdate(Source source) {
        this.mCallBacksHandler.sendMessage(this.mCallBacksHandler.obtainMessage(8, source));
    }

    public synchronized boolean addListener(MediaObserverInterface observer) {
        boolean ret;
        String str = TAG;
        synchronized (this) {
            Log.i(TAG, "addListener");
            if (!this.mListening) {
                this.mAudioAccess.setListener((MediaCallbackInterface) this);
                this.mListening = true;
            }
            ret = super.addListener(observer);
            if (this.mReady) {
                Log.i(TAG, "already ready");
                initRequest();
            } else {
                Log.i(TAG, "Wait service to be ready");
            }
        }
        return ret;
    }

    public synchronized boolean removeListener(MediaObserverInterface observer) {
        boolean ret;
        Log.i(TAG, "removeListener");
        ret = super.removeListener(observer);
        if (ret && this.mObservers.isEmpty() && this.mListening) {
            this.mAudioAccess.removeListener((MediaCallbackInterface) this);
            this.mListening = false;
        }
        return ret;
    }

    public boolean nextMedia() {
        return this.mAudioAccess.nextMedia();
    }

    public boolean previousMedia() {
        return this.mAudioAccess.previousMedia();
    }

    public Source getCurrentSource() {
        if (this.mReady) {
            return this.mAudioAccess.getCurrentSource();
        }
        Log.e(TAG, "Service not ready");
        return null;
    }

    public void launchSource(Source source, boolean newContext) {
        this.mAudioAccess.playSource(source, newContext);
    }

    public boolean playpause() {
        return this.mAudioAccess.playPause();
    }

    public boolean playSpeedNormal() {
        return this.mAudioAccess.playSpeedNormal();
    }

    public boolean play() {
        return this.mAudioAccess.play();
    }

    public boolean pause() {
        return this.mAudioAccess.pause();
    }

    public boolean pause(boolean synchronous) {
        if (!synchronous) {
            return pause();
        }
        if (this.mReady) {
            return this.mAudioAccess.pauseSync();
        }
        Log.e(TAG, "Service not ready");
        return false;
    }

    public boolean setRandomRepeatMode(int random, int repeat) {
        return this.mAudioAccess.setRandomRepeat(random, repeat);
    }

    public boolean requestPlayerInfo() {
        return this.mAudioAccess.askAllInfos();
    }

    public boolean seekToPosition(int position) {
        return this.mAudioAccess.seekToPosition(position);
    }

    public boolean isManagerReady() {
        return this.mReady;
    }

    public ArrayList<Source> getSourceStack() {
        if (this.mReady) {
            return this.mAudioAccess.getSources(true);
        }
        Log.e(TAG, "Service not ready");
        return null;
    }

    public ArrayList<Source> getAllSource() {
        if (this.mReady) {
            return this.mAudioAccess.getSources(false);
        }
        Log.e(TAG, "Service not ready");
        return null;
    }

    public boolean start() {
        if (this.mReady) {
            return this.mAudioAccess.start();
        }
        Log.e(TAG, "Service not ready");
        return false;
    }

    public boolean isTypeSourceActivated(int type, int id) {
        if (this.mReady) {
            return this.mAudioAccess.isTypeSourceActivated(type, id);
        }
        Log.e(TAG, "Service not ready");
        return false;
    }

    public boolean setTypeSourceActivated(int type, int id, boolean activated) {
        if (this.mReady) {
            return this.mAudioAccess.setTypeSourceActivated(type, id, activated);
        }
        Log.e(TAG, "Service not ready");
        return false;
    }

    public boolean isCoverEnable() {
        if (this.mReady) {
            return this.mAudioAccess.getCoverActivated();
        }
        Log.e(TAG, "Service not ready");
        return false;
    }

    public boolean setCoverEnable(boolean enable) {
        if (this.mReady) {
            return this.mAudioAccess.setCoverActivated(enable);
        }
        Log.e(TAG, "Service not ready");
        return false;
    }

    public boolean resumeEngine() {
        if (this.mReady) {
            return this.mAudioAccess.resumeEngine();
        }
        Log.e(TAG, "Service not ready");
        return false;
    }

    public boolean suspendEngine() {
        if (this.mReady) {
            return this.mAudioAccess.suspendEngine();
        }
        Log.e(TAG, "Service not ready");
        return false;
    }

    public void fastBackward() {
        if (this.mReady) {
            this.mAudioAccess.fastBackward();
        } else {
            Log.e(TAG, "Service not ready");
        }
    }

    public void fastForward() {
        if (this.mReady) {
            this.mAudioAccess.fastForward();
        } else {
            Log.e(TAG, "Service not ready");
        }
    }

    public boolean isListPlayedFromMVR() {
        return this.mAudioAccess.isListPlayedFromMVR();
    }
}
