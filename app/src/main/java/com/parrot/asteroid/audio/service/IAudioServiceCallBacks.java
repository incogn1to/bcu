package com.parrot.asteroid.audio.service;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAudioServiceCallBacks extends IInterface {

    public static abstract class Stub extends Binder implements IAudioServiceCallBacks {
        private static final String DESCRIPTOR = "com.parrot.asteroid.audio.service.IAudioServiceCallBacks";
        static final int TRANSACTION_onAllMediaInfos = 6;
        static final int TRANSACTION_onMetadataChanged = 4;
        static final int TRANSACTION_onPlayerStateChanged = 7;
        static final int TRANSACTION_onServiceDestroyed = 3;
        static final int TRANSACTION_onServiceReady = 2;
        static final int TRANSACTION_onSourceChanged = 5;
        static final int TRANSACTION_onSourceCreation = 8;
        static final int TRANSACTION_onSourceError = 11;
        static final int TRANSACTION_onSourceReady = 9;
        static final int TRANSACTION_onSourceRemoved = 12;
        static final int TRANSACTION_onSourceUpdate = 10;
        static final int TRANSACTION_volumeChanged = 1;

        private static class Proxy implements IAudioServiceCallBacks {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public void volumeChanged(int source, int currentVolume, int maxVolume) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(source);
                    _data.writeInt(currentVolume);
                    _data.writeInt(maxVolume);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onServiceReady(boolean ready) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(ready ? 1 : 0);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onServiceDestroyed() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onMetadataChanged(Metadata metadata, int changeFlag) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (metadata != null) {
                        _data.writeInt(1);
                        metadata.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    _data.writeInt(changeFlag);
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onSourceChanged(Source currentSource) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (currentSource != null) {
                        _data.writeInt(1);
                        currentSource.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onAllMediaInfos(Source source, PlayerInfo playerInfo, Metadata metadata) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (source != null) {
                        _data.writeInt(1);
                        source.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    if (playerInfo != null) {
                        _data.writeInt(1);
                        playerInfo.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    if (metadata != null) {
                        _data.writeInt(1);
                        metadata.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onPlayerStateChanged(PlayerInfo playerInfo) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (playerInfo != null) {
                        _data.writeInt(1);
                        playerInfo.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onSourceCreation(Source creatingSource) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (creatingSource != null) {
                        _data.writeInt(1);
                        creatingSource.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(8, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onSourceReady(Source readySource) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (readySource != null) {
                        _data.writeInt(1);
                        readySource.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(9, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onSourceUpdate(Source source) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (source != null) {
                        _data.writeInt(1);
                        source.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(10, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onSourceError(Source source) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (source != null) {
                        _data.writeInt(1);
                        source.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(11, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onSourceRemoved(Source source) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (source != null) {
                        _data.writeInt(1);
                        source.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(12, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IAudioServiceCallBacks asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof IAudioServiceCallBacks)) {
                return new Proxy(obj);
            }
            return (IAudioServiceCallBacks) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            String str = DESCRIPTOR;
            String str2;
            Source _arg0;
            switch (code) {
                case 1:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    volumeChanged(data.readInt(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 2:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    onServiceReady(data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 3:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    onServiceDestroyed();
                    reply.writeNoException();
                    return true;
                case 4:
                    Metadata _arg02;
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    if (data.readInt() != 0) {
                        _arg02 = (Metadata) Metadata.CREATOR.createFromParcel(data);
                    } else {
                        _arg02 = null;
                    }
                    onMetadataChanged(_arg02, data.readInt());
                    reply.writeNoException();
                    return true;
                case 5:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    if (data.readInt() != 0) {
                        _arg0 = (Source) Source.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    onSourceChanged(_arg0);
                    reply.writeNoException();
                    return true;
                case 6:
                    PlayerInfo _arg1;
                    Metadata _arg2;
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    if (data.readInt() != 0) {
                        _arg0 = (Source) Source.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    if (data.readInt() != 0) {
                        _arg1 = (PlayerInfo) PlayerInfo.CREATOR.createFromParcel(data);
                    } else {
                        _arg1 = null;
                    }
                    if (data.readInt() != 0) {
                        _arg2 = (Metadata) Metadata.CREATOR.createFromParcel(data);
                    } else {
                        _arg2 = null;
                    }
                    onAllMediaInfos(_arg0, _arg1, _arg2);
                    reply.writeNoException();
                    return true;
                case 7:
                    PlayerInfo _arg03;
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    if (data.readInt() != 0) {
                        _arg03 = (PlayerInfo) PlayerInfo.CREATOR.createFromParcel(data);
                    } else {
                        _arg03 = null;
                    }
                    onPlayerStateChanged(_arg03);
                    reply.writeNoException();
                    return true;
                case 8:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    if (data.readInt() != 0) {
                        _arg0 = (Source) Source.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    onSourceCreation(_arg0);
                    reply.writeNoException();
                    return true;
                case 9:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    if (data.readInt() != 0) {
                        _arg0 = (Source) Source.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    onSourceReady(_arg0);
                    reply.writeNoException();
                    return true;
                case 10:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    if (data.readInt() != 0) {
                        _arg0 = (Source) Source.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    onSourceUpdate(_arg0);
                    reply.writeNoException();
                    return true;
                case 11:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    if (data.readInt() != 0) {
                        _arg0 = (Source) Source.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    onSourceError(_arg0);
                    reply.writeNoException();
                    return true;
                case 12:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    if (data.readInt() != 0) {
                        _arg0 = (Source) Source.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    onSourceRemoved(_arg0);
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    str2 = DESCRIPTOR;
                    reply.writeString(str);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onAllMediaInfos(Source source, PlayerInfo playerInfo, Metadata metadata) throws RemoteException;

    void onMetadataChanged(Metadata metadata, int i) throws RemoteException;

    void onPlayerStateChanged(PlayerInfo playerInfo) throws RemoteException;

    void onServiceDestroyed() throws RemoteException;

    void onServiceReady(boolean z) throws RemoteException;

    void onSourceChanged(Source source) throws RemoteException;

    void onSourceCreation(Source source) throws RemoteException;

    void onSourceError(Source source) throws RemoteException;

    void onSourceReady(Source source) throws RemoteException;

    void onSourceRemoved(Source source) throws RemoteException;

    void onSourceUpdate(Source source) throws RemoteException;

    void volumeChanged(int i, int i2, int i3) throws RemoteException;
}
