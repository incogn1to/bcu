package com.parrot.asteroid.media;

import android.content.Context;
import android.util.Log;
import java.lang.ref.SoftReference;

public class MediaManagerFactory {
    private static final String TAG = "MediaManagerFactory";
    private static boolean init = false;
    private static SoftReference<MediaManager> instance = new SoftReference(null);

    private MediaManagerFactory() {
    }

    public static MediaManager getMediaManager(Context ctx) {
        MediaManager m;
        Throwable th;
        if (instance != null) {
            m = (MediaManager) instance.get();
        } else {
            m = (MediaManager) new SoftReference(null).get();
        }
        if (!init || m == null) {
            synchronized (MediaManagerFactory.class) {
                try {
                    if (!init || m == null) {
                        MediaManager m2 = new MediaManagerService(ctx);
                        try {
                            instance = new SoftReference(m2);
                            init = true;
                            m = m2;
                        } catch (Throwable th2) {
                            th = th2;
                            m = m2;
                            throw th;
                        }
                    }
                    m = (MediaManager) instance.get();
                } catch (Throwable th3) {
                    th = th3;
                    //throw th;
                }
            }
        }
        return m;
    }

    public static MediaManager getMediaManager() {
        String str = TAG;
        String str2;
        if (init) {
            MediaManager m = (MediaManager) instance.get();
            if (m == null) {
                str2 = TAG;
                Log.e(str, "instance have been released too soon -> keep a strong reference to avoid this");
            }
            return m;
        }
        str2 = TAG;
        Log.e(str, "No MediaManager Previously Created -> call getMediaManager(Context) before");
        return null;
    }

    static boolean destroyInstance(MediaManager caller) {
        Log.i(TAG, "destroyInstance");
        if (caller == null) {
            return false;
        }
        if (instance.get() != caller) {
            return false;
        }
        init = false;
        instance.clear();
        instance = null;
        return true;
    }
}
