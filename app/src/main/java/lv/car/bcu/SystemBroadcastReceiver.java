package lv.car.bcu;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class SystemBroadcastReceiver extends android.content.BroadcastReceiver {
    private static final String TAG = SystemBroadcastReceiver.class.getSimpleName();

    public SystemBroadcastReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Log.v(TAG, "intent: " + intent.toString());
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
