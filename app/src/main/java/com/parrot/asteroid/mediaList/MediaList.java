package com.parrot.asteroid.mediaList;

import android.content.Context;
import com.parrot.asteroid.audio.service.Source;

abstract class MediaList {
    private Context mContext;
    private MediaListManagerInterface mMediaListManager;
    private int mPosition = -1;

    public abstract MediaList createSubList(int i, long j);

    public abstract void destroy();

    public abstract String getDynamicHeader(int i, long j);

    public abstract int getIconId();

    public abstract String getSelectedText();

    public abstract boolean hasAllEnabled();

    public abstract boolean hasDynamicHeader();

    public abstract boolean hasSubList(int i, long j);

    public abstract boolean isFilterable();

    public abstract boolean isRecursive();

    public abstract boolean requestPosition(String str);

    public abstract void setSelection(int i, long j);

    public abstract void show();

    MediaList(Context context, Source source) {
        setMediaListManager(MediaListManagerFactory.getMediaListManager(context));
        this.mContext = context;
    }

    /* Access modifiers changed, original: 0000 */
    public void setPosition(int position) {
        this.mPosition = position;
    }

    /* Access modifiers changed, original: 0000 */
    public int getPosition() {
        return this.mPosition;
    }

    /* Access modifiers changed, original: protected */
    public Context getContext() {
        return this.mContext;
    }

    public void setMediaListManager(MediaListManagerInterface mMediaListManager) {
        this.mMediaListManager = mMediaListManager;
    }

    public MediaListManagerInterface getMediaListManager() {
        return this.mMediaListManager;
    }
}
