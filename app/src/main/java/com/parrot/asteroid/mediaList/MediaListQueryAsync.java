package com.parrot.asteroid.mediaList;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

class MediaListQueryAsync extends MediaListQuery {
    private static final int ERROR_TIMEOUT_MESSAGE = 2;
    private static final int LONG_QUERY_TIMEOUT_MESSAGE = 1;
    private static final long LONG_TIMEOUT_MS = 2000;
    private static final String TAG = MediaListQueryAsync.class.getSimpleName();
    private static final long TIMEOUT_MS = 20000;
    private static int mTokenIndex = 0;
    private QueryHandler mQueryHandler;
    private Handler mTimeOutHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 2) {
                Log.e(MediaListQueryAsync.TAG, "request timeout");
                MediaListQueryListener mediaListQueryListener = MediaListQueryAsync.this.mMediaListQueryListener;
                MediaListQueryAsync.this.finish();
                if (mediaListQueryListener != null) {
                    mediaListQueryListener.onError(2);
                }
            } else if (msg.what == 1) {
                Log.e(MediaListQueryAsync.TAG, "long request timeout");
                if (MediaListQueryAsync.this.mMediaListQueryListener != null) {
                    MediaListQueryAsync.this.mMediaListQueryListener.onLongRequest();
                }
            } else {
                Log.e(MediaListQueryAsync.TAG, "Message Not Handle");
            }
        }
    };
    private int mToken = 0;

    class QueryHandler extends AsyncQueryHandler {
        QueryHandler(ContentResolver res) {
            super(res);
        }

        /* Access modifiers changed, original: protected */
        public void onQueryComplete(int token, Object cookie, Cursor cursor) {
            MediaListQueryAsync.this.mTimeOutHandler.removeMessages(1);
            MediaListQueryAsync.this.mTimeOutHandler.removeMessages(2);
            if (MediaListQueryAsync.this.mMediaListQueryListener != null) {
                MediaListQueryAsync.this.mMediaListQueryListener.onQueryCompleted(cursor);
            }
        }
    }

    MediaListQueryAsync(Context context, String source, String category, String requestedColumn) {
        super(context, source, category, requestedColumn);
    }

    private synchronized int getToken() {
        int i;
        i = mTokenIndex;
        mTokenIndex = i + 1;
        return i;
    }

    /* Access modifiers changed, original: 0000 */
    public boolean launchRequest() {
        Uri request = buildRequest();
        String where = buildWhere();
        Log.d(TAG, "request: " + request + ", Where: " + where);
        this.mQueryHandler = new QueryHandler(this.mContext.getContentResolver());
        this.mToken = getToken();
        this.mQueryHandler.startQuery(this.mToken, null, request, getProjection(), where, null, getOrder());
        this.mTimeOutHandler.sendEmptyMessageDelayed(2, TIMEOUT_MS);
        this.mTimeOutHandler.sendEmptyMessageDelayed(1, 2000);
        return true;
    }

    public void finish() {
        this.mMediaListQueryListener = null;
        if (this.mQueryHandler != null) {
            this.mQueryHandler.cancelOperation(this.mToken);
        }
    }
}
