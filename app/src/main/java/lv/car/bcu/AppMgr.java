package lv.car.bcu;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;

import com.parrot.asteroid.audio.service.Source;

import org.json.JSONObject;

import java.util.List;

public class AppMgr {
    private static final String BCU_APP = "lv.car.bcu";
    private static final String SETTINGS_APP = "com.android.settings";
    private static final String TAG = AppMgr.class.getSimpleName();
    private static Context mContext;

    AppMgr(Context context) {
        mContext = context;
    }

    private boolean isAppIsInBackground(String packageName) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        ComponentName componentInfo = taskInfo.get(0).topActivity;
        if (componentInfo.getPackageName().equals(packageName)) {
            isInBackground = false;
        }
        return isInBackground;
    }

    public Drawable getIcon(String name) {
        Drawable d = mContext.getResources().getDrawable(R.drawable.icon);
        try {
            PackageManager packageManager = mContext.getPackageManager();
            d = packageManager.getApplicationIcon(name);
            return d;
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return d;
    }

    public boolean showToast()
    {
        return isAppIsInBackground(BCU_APP);
    }

    public Drawable getIntentIcon(Intent app) {
        Drawable d = mContext.getResources().getDrawable(R.drawable.icon);
        try {
            PackageManager pm = mContext.getPackageManager();
            d = pm.getActivityIcon(app);
            return d;
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return d;
    }

    public String getLabel(String PackageName) {
        try {
            PackageManager packageManager = mContext.getPackageManager();
            ApplicationInfo info = packageManager.getApplicationInfo(PackageName, PackageManager.GET_META_DATA);
            String appName = (String) packageManager.getApplicationLabel(info);
            return appName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "N/A";
        }
    }

    public void startSettingsApp() {
        launchApp( SETTINGS_APP );
    }

    public void activateTask(ActivityManager.RecentTaskInfo task) {
        try {
            if (task.id > -1) {
                PackageManager packageManager = mContext.getPackageManager();
                Intent i = packageManager.getLaunchIntentForPackage(task.baseIntent.getComponent().getPackageName());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mContext.startActivity(i);
                return;
            }
        }catch (Throwable t) {
            t.printStackTrace();
        }
        launchApp(task.baseIntent.getComponent().getPackageName());
    }

    public void launchApp(String packageName) {
        launchApp(packageName, "android.intent.category.LAUNCHER");
    }

    public void launchApp(String packageName, String category) {
        try {
            Intent i = mContext.getPackageManager().getLaunchIntentForPackage(packageName);
            if (i != null) {
                i.addCategory(category);
                mContext.startActivity(i);
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void launchApk(ResolveInfo packageName) {
        Log.d(TAG, "will launch " + packageName.activityInfo.packageName);
        launchApp(packageName.activityInfo.packageName);
    }

    public void openUrl(String url) {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.setData(Uri.parse(url));
            mContext.startActivity(i);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void showPlayList() {
        try {
            Intent i = new Intent(mContext,PlayerActivity.class);
            i.setAction("lv.car.bcu");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(i);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void showPlayList( Source src ) {
        try {
            Intent i = new Intent(mContext,PlayerActivity.class);
            i.setAction("lv.car.bcu");
            i.putExtra("source", src);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(i);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void showParrotPlayList( Source src ) {
        try {
            Intent i = new Intent();
            i.setAction("com.parrot.showCurrentList");
            i.putExtra("source", src);
            i.addCategory("android.intent.category.DEFAULT");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(i);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void showCity( JSONObject json, int position ) {
        try {
            Intent i = new Intent(mContext,CityActivity.class);
            i.putExtra("json", json.toString());
            i.putExtra("position", position);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(i);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void showWeather() {
        try {
            Intent i = new Intent(mContext,WeatherActivity.class);
            i.setAction("lv.car.bcu");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(i);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void showMain() {
        try {
            Intent i = new Intent(mContext,MainActivity.class);
            i.setAction("lv.car.bcu");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(i);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public List<ResolveInfo> getAppsForLaunch() {
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        return mContext.getPackageManager().queryIntentActivities(mainIntent, 0);
    }

    public List<ActivityManager.RecentTaskInfo> getRecentTasks() {
        final ActivityManager activityManager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        return activityManager.getRecentTasks(Integer.MAX_VALUE,0);
    }
}
