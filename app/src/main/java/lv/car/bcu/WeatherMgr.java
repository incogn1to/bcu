package lv.car.bcu;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.util.Locale;

public class WeatherMgr {
    private static final String TAG = WeatherMgr.class.getSimpleName();
    private LocationManager mLocationManager;
    private Context mContext;
    private Handler mHandler;
    private FileMgr mConfigMgr;

    WeatherMgr(Context context, Handler hl) {
        mContext = context;
        mHandler = hl;
        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        mConfigMgr = new FileMgr(context);
    }

    LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {
            try {
                Log.d(TAG, "GPS location: " + location.toString());
                requestCurrentWeatherUpdate();
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {
            try {
                requestCurrentWeatherUpdate();
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public void startGpsUpdates() {
        final int time_interval = 60 * 10 * 1000; // 10 min
        final int distance_interval = 10 * 1000; // 10 km
        Log.v(TAG,"time inteval: " + time_interval + " distance interval: " + distance_interval);
        this.mLocationManager.requestLocationUpdates("gps",time_interval ,distance_interval , this.mLocationListener);
    }

    private void stopGpsUpdates() {
        this.mLocationManager.removeUpdates(this.mLocationListener);
    }

    private Location getLastLocation() {
        try {
            android.location.Location locationGPS = ((LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE)).getLastKnownLocation("gps");
            return locationGPS;
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return null;
    }

    private String getLanguage() {
        return Locale.getDefault().toString().substring(0, 2).toLowerCase();
    }

    private String getLangCode() {
        String lang = getLanguage();
        if (lang.equals("lv"))
        {
            lang = "la"; // oddly open weather uses "la" for latvian
        }
        return lang;
    }

    public void setStoredCurrentWeather() {
        setStoredWeather(mContext.getResources().getString(R.string.CURRENT_WEATHER));
    }

    public void setStoredWeatherForecast() {
        setStoredWeather(mContext.getResources().getString(R.string.FORECAST_WEATHER));
    }

    private void setStoredWeather(String file) {
        try {
            String data = mConfigMgr.readFromFile(file);
            if (!data.isEmpty())
            {
                Log.v(TAG,"stored weather data:" + data.toString());
                JSONObject weather = new JSONObject(data);
                setWeather(weather);
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static int getPictureId(int code) {
        // https://openweathermap.org/weather-conditions
        switch (code) {
            case 200: //	Thunderstorm	thunderstorm with light rain	 11d
            case 201: //	Thunderstorm	thunderstorm with rain	 11d
            case 202: //	Thunderstorm	thunderstorm with heavy rain	 11d
            case 230: //	Thunderstorm	thunderstorm with light drizzle	 11d
            case 231: //	Thunderstorm	thunderstorm with drizzle	 11d
            case 232: //	Thunderstorm	thunderstorm with heavy drizzle	 11d
            case 781: //	Tornado	tornado	 50d
                return R.drawable.thundershower_400x135px;
            case 210: //	Thunderstorm	light thunderstorm	 11d
            case 211: //	Thunderstorm	thunderstorm	 11d
            case 212: //	Thunderstorm	heavy thunderstorm	 11d
            case 221: //	Thunderstorm	ragged thunderstorm	 11d
                return R.drawable.thunder_400x135px;
            case 300: //	Drizzle	light intensity drizzle	 09d
            case 301: //	Drizzle	drizzle	 09d
            case 302: //	Drizzle	heavy intensity drizzle	 09d
            case 310: //	Drizzle	light intensity drizzle rain	 09d
            case 311: //	Drizzle	drizzle rain	 09d
            case 312: //	Drizzle	heavy intensity drizzle rain	 09d
            case 313: //	Drizzle	shower rain and drizzle	 09d
            case 314: //	Drizzle	heavy shower rain and drizzle	 09d
            case 321: //	Drizzle	shower drizzle	 09d
            case 500: //	Rain	light rain	 10d
            case 501: //	Rain	moderate rain	 10d
            case 502: //	Rain	heavy intensity rain	 10d
            case 503: //	Rain	very heavy rain	 10d
            case 504: //	Rain	extreme rain	 10d
            case 511: //	Rain	freezing rain	 13d
            case 520: //	Rain	light intensity shower rain	 09d
            case 521: //	Rain	shower rain	 09d
            case 522: //	Rain	heavy intensity shower rain	 09d
            case 531: //	Rain	ragged shower rain	 09d
                return R.drawable.rain_400x135px;
            case 611: //	Snow	Sleet	 13d
            case 612: //	Snow	Light shower sleet	 13d
            case 613: //	Snow	Shower sleet	 13d
                return R.drawable.sleet_400x135px;
            case 600: //	Snow	light snow	 13d
            case 601: //	Snow	Snow	 13d
            case 615: //	Snow	Light rain and snow	 13d
            case 616: //	Snow	Rain and snow	 13d
            case 620: //	Snow	Light shower snow	 13d
                return R.drawable.snow_400x135px;
            case 621: //	Snow	Shower snow	 13d
            case 622: //	Snow	Heavy shower snow	 13d
            case 602: //	Snow	Heavy snow	 13d
                return R.drawable.snowfall_400x135px;
            case 701: //	Mist	mist	 50d
            case 711: //	Smoke	Smoke	 50d
            case 721: //	Haze	Haze	 50d
            case 731: //	Dust	sand/ dust whirls	 50d
            case 741: //	Fog	fog	 50d
                return R.drawable.fog_400x135px;
            case 751: //	Sand	sand	 50d
            case 761: //	Dust	dust	 50d
            case 762: //	Ash	volcanic ash	 50d
            case 771: //	Squall	squalls	 50d
            case 800: //	Clear	clear sky	 01d  01n
            default:
                return R.drawable.sun_400x135px;
            case 801: //	Clouds	few clouds: 11-25%	 02d  02n
            case 802: //	Clouds	scattered clouds: 25-50%	 03d  03n
                return R.drawable.partly_sunny_400x135px;
            case 803: //	Clouds	broken clouds: 51-84%	 04d  04n
                return R.drawable.cloudy_400x135px;
            case 804: //	Clouds	overcast clouds: 85-100%	 04d  04n
                return R.drawable.overcast_400x135px;
        }
    }

    public static int getSquarePictureId(int code) {
        // https://openweathermap.org/weather-conditions
        switch (code) {
            case 200: //	Thunderstorm	thunderstorm with light rain	 11d
            case 201: //	Thunderstorm	thunderstorm with rain	 11d
            case 202: //	Thunderstorm	thunderstorm with heavy rain	 11d
            case 230: //	Thunderstorm	thunderstorm with light drizzle	 11d
            case 231: //	Thunderstorm	thunderstorm with drizzle	 11d
            case 232: //	Thunderstorm	thunderstorm with heavy drizzle	 11d
            case 781: //	Tornado	tornado	 50d
                return R.drawable.thundershower_175x250px;
            case 210: //	Thunderstorm	light thunderstorm	 11d
            case 211: //	Thunderstorm	thunderstorm	 11d
            case 212: //	Thunderstorm	heavy thunderstorm	 11d
            case 221: //	Thunderstorm	ragged thunderstorm	 11d
                return R.drawable.thunder_175x250px;
            case 300: //	Drizzle	light intensity drizzle	 09d
            case 301: //	Drizzle	drizzle	 09d
            case 302: //	Drizzle	heavy intensity drizzle	 09d
            case 310: //	Drizzle	light intensity drizzle rain	 09d
            case 311: //	Drizzle	drizzle rain	 09d
            case 312: //	Drizzle	heavy intensity drizzle rain	 09d
            case 313: //	Drizzle	shower rain and drizzle	 09d
            case 314: //	Drizzle	heavy shower rain and drizzle	 09d
            case 321: //	Drizzle	shower drizzle	 09d
            case 500: //	Rain	light rain	 10d
            case 501: //	Rain	moderate rain	 10d
            case 502: //	Rain	heavy intensity rain	 10d
            case 503: //	Rain	very heavy rain	 10d
            case 504: //	Rain	extreme rain	 10d
            case 511: //	Rain	freezing rain	 13d
            case 520: //	Rain	light intensity shower rain	 09d
            case 521: //	Rain	shower rain	 09d
            case 522: //	Rain	heavy intensity shower rain	 09d
            case 531: //	Rain	ragged shower rain	 09d
                return R.drawable.rain_175x250px;
            case 611: //	Snow	Sleet	 13d
            case 612: //	Snow	Light shower sleet	 13d
            case 613: //	Snow	Shower sleet	 13d
                return R.drawable.sleet_175x250px;
            case 600: //	Snow	light snow	 13d
            case 601: //	Snow	Snow	 13d
            case 615: //	Snow	Light rain and snow	 13d
            case 616: //	Snow	Rain and snow	 13d
            case 620: //	Snow	Light shower snow	 13d
                return R.drawable.snow_175x250px;
            case 621: //	Snow	Shower snow	 13d
            case 622: //	Snow	Heavy shower snow	 13d
            case 602: //	Snow	Heavy snow	 13d
                return R.drawable.snowfall_175x250px;
            case 701: //	Mist	mist	 50d
            case 711: //	Smoke	Smoke	 50d
            case 721: //	Haze	Haze	 50d
            case 731: //	Dust	sand/ dust whirls	 50d
            case 741: //	Fog	fog	 50d
                return R.drawable.fog_175x250px;
            case 751: //	Sand	sand	 50d
            case 761: //	Dust	dust	 50d
            case 762: //	Ash	volcanic ash	 50d
            case 771: //	Squall	squalls	 50d
            case 800: //	Clear	clear sky	 01d  01n
            default:
                return R.drawable.sun_175x250px;
            case 801: //	Clouds	few clouds: 11-25%	 02d  02n
            case 802: //	Clouds	scattered clouds: 25-50%	 03d  03n
                return R.drawable.partly_sunny_175x250px;
            case 803: //	Clouds	broken clouds: 51-84%	 04d  04n
                return R.drawable.cloudy_175x250px;
            case 804: //	Clouds	overcast clouds: 85-100%	 04d  04n
                return R.drawable.overcast_175x250px;
        }
    }

    public static int getWindPictureId(int deg) {
        // Cardinal Direction Degree Direction
        //N 348.75 - 11.25
        if (deg > 348 || deg < 11 )
        {
            return R.drawable.wind_n_highlighted_24x24px;
        }
        //NNE 11.25 - 33.75
        //NE 33.75 - 56.25
        //ENE 56.25 - 78.75
        else if ( deg > 11 && deg < 78 )
        {
            return R.drawable.wind_ne_highlighted_24x24px;
        }
        //E 78.75 - 101.25
        else if ( deg > 56 && deg < 101 )
        {
            return R.drawable.wind_e_highlighted_24x24px;
        }
        //ESE 101.25 - 123.75
        //SE 123.75 - 146.25
        //SSE 146.25 - 168.75
        else if ( deg > 101 && deg < 168 )
        {
            return R.drawable.wind_se_highlighted_24x24px;
        }
        //S 168.75 - 191.25
        else if ( deg > 146 && deg < 191 )
        {
            return R.drawable.wind_s_highlighted_24x24px;
        }
        //SSW 191.25 - 213.75
        //SW 213.75 - 236.25
        //WSW 236.25 - 258.75
        else if ( deg > 191 && deg < 258 )
        {
            return R.drawable.wind_sw_highlighted_24x24px;
        }
        //W 258.75 - 281.25
        else if ( deg > 236 && deg < 281 )
        {
            return R.drawable.wind_w_highlighted_24x24px;
        }
        //WNW 281.25 - 303.75
        //NW 303.75 - 326.25
        //NNW 326.25 - 348.75
        else
        {
            return R.drawable.wind_nw_highlighted_24x24px;
        }
    }

    private void setWeather(JSONObject data) {
        try {
            Message msg = mHandler.obtainMessage();
            msg.what = MainActivity.WEATHER;
            msg.obj = data;
            mHandler.sendMessage(msg);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void requestCurrentWeatherUpdate() {
        try {
            CurrentWeatherUpdateRunner runner = new CurrentWeatherUpdateRunner();
            runner.execute();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void requestWeatherForecastUpdate() {
        try {
            ForecastWeatherUpdateRunner runner = new ForecastWeatherUpdateRunner();
            runner.execute();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private class CurrentWeatherUpdateRunner extends AsyncTask<String, String, String> {
        private String resp;
        @Override
        protected String doInBackground(String... params) {
            try {
                 Location loc = getLastLocation();
                if (loc == null) {
                    Log.d(TAG, "GPS data not avalible");
                    return resp;
                }
                String lat = String.valueOf(loc.getLatitude());
                String lon = String.valueOf(loc.getLongitude());
                Log.d(TAG, "location: " + loc.toString() + " lat:" + lat + " lon: " + lon);
                WeatherHttpClient wc = new WeatherHttpClient();
                String lang = getLangCode();
                String report = wc.getCurrentWeatherData(lon, lat, lang);
                Log.v(TAG, report);
                JSONObject weather = new JSONObject(report);
                setWeather(weather);
                mConfigMgr.writeToFile(mContext.getResources().getString(R.string.CURRENT_WEATHER),weather.toString());
            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {

        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(String... text) {

        }
    }

    private class ForecastWeatherUpdateRunner extends AsyncTask<String, String, String> {

        private String resp;

        @Override
        protected String doInBackground(String... params) {
            try {
                Location loc = getLastLocation();
                if (loc == null) {
                    Log.d(TAG, "GPS data not avalible");
                    return resp;
                }
                String lat = String.valueOf(loc.getLatitude());
                String lon = String.valueOf(loc.getLongitude());
                Log.d(TAG, "location: " + loc.toString() + " lat:" + lat + " lon: " + lon);
                WeatherHttpClient wc = new WeatherHttpClient();
                String lang = getLangCode();
                final int n = 20;
                String report = wc.getForecastWeatherData(lon, lat, lang, n);
                Log.v(TAG, report);
                JSONObject weather = new JSONObject(report);
                setWeather(weather);
                mConfigMgr.writeToFile(mContext.getResources().getString(R.string.FORECAST_WEATHER),weather.toString());
            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {

        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(String... text) {

        }
    }
}
