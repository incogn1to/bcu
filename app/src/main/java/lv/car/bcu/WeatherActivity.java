package lv.car.bcu;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WeatherActivity extends AppCompatActivity {
    private static final String TAG = WeatherActivity.class.getSimpleName();
    private WeatherMgr mWeatherMgr;
    private AppMgr mAppMgr;
    private JSONObject mJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        mWeatherMgr = new WeatherMgr(getApplicationContext(),myHandler);
        mAppMgr = new AppMgr(getApplicationContext());
        mWeatherMgr.setStoredWeatherForecast();
        mWeatherMgr.requestWeatherForecastUpdate();
    }

    private void updateWeather(JSONObject json) {
        try {
            // { "message":0,
            //    "list":
            //    [
            //     {"clouds":{"all":4},"dt":1650477600,"wind":{"gust":10.42,"speed":4.78,"deg":65},"visibility":10000,
            //     "sys":{"pod":"n"},"weather":[{"id":800,"icon":"01n","description":"clear sky",
            //     "main":"Clear"}],"pop":0,"dt_txt":"2022-04-20 18:00:00",
            //     "main":{"temp_kf":3.77,"humidity":39,"pressure":1016,"temp_max":15.07,
            //     "sea_level":1016,"temp_min":11.3,"temp":15.07,"grnd_level":1016,"feels_like":13.65}}
            //    ],
            //    "cnt":3,"cod":"200",
            //    "city":{"coord":{"lon":24.1052,"lat":56.9496},"id":6615326,"timezone":10800,
            //    "sunset":1650476498,"name":"Vecrīga","sunrise":1650423781,"population":2000,"country":"LV"}
            // }
            Log.d(TAG, "JSON[" + json.toString() + "]");
            mJson = json;
            List<String> text = new ArrayList<String>();
            List<String> time = new ArrayList<String>();
            List<String> date = new ArrayList<String>();
            List<String> temp = new ArrayList<String>();
            List<Drawable> icons = new ArrayList<Drawable>();

            final JSONArray list = json.getJSONArray("list");
            for(int i=0; i<list.length(); i++){
                final JSONObject n = list.getJSONObject(i);
                Log.d(TAG, "obj[" + n.toString() + "]");

                String txt = n.getJSONArray("weather").getJSONObject(0).getString("description");
                text.add(txt);

                txt = n.getJSONObject("main").getInt("temp") + "°C";
                temp.add(txt);

                final String full_date = n.getString("dt_txt"); // "2022-04-20 18:00:00"
                txt = full_date.substring(8,10) + "." + full_date.substring(5,7) + "." + full_date.substring(2,4);
                date.add(txt);

                txt = full_date.substring(11,16);
                time.add(txt);

                final int code = n.getJSONArray("weather").getJSONObject(0).getInt("id");
                final int id = WeatherMgr.getSquarePictureId(code);
                icons.add(getApplicationContext().getResources().getDrawable(id));
            }

            final WeatherAdapter adapter = new WeatherAdapter(this, text, date, time, temp, icons);
            final ListView listview = (ListView) findViewById(R.id.weatherList);
            listview.setAdapter(adapter);

            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    Log.d(TAG, "weather clicked:" + position);
                    mAppMgr.showCity(mJson,position);
                }
            });

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void clickBack(View view) {
        Log.d(TAG, "Back was clicked");
        finish();
        mAppMgr.showMain();
    }

    private final Handler myHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            try {
                int cmd = msg.what;
                switch (cmd) {
                    case (int) MainActivity.WEATHER: {
                        final JSONObject weatherData = (JSONObject) msg.obj;
                        updateWeather(weatherData);
                        break;
                    }
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    };
}
