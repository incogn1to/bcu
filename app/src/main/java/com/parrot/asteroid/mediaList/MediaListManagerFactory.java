package com.parrot.asteroid.mediaList;

import android.content.Context;

public final class MediaListManagerFactory {
    private static volatile MediaListManagerInterface mInstance;

    private MediaListManagerFactory() {
    }

    public static synchronized MediaListManagerInterface getMediaListManager(Context context) {
        MediaListManagerInterface mediaListManagerInterface;
        synchronized (MediaListManagerFactory.class) {
            if (mInstance == null) {
                mInstance = new MediaListManagerHSTI(context);
            }
            mediaListManagerInterface = mInstance;
        }
        return mediaListManagerInterface;
    }
}
