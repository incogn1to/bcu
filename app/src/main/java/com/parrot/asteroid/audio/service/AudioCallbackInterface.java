package com.parrot.asteroid.audio.service;

public interface AudioCallbackInterface {
    void onVolumeChanged(int i, int i2, int i3);
}
