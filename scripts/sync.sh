#!/system/bin/sh

if [ -f /tmp/sync.pid ]; then
    echo "sync is already in progress!"
    toast.sh "sync is already in progress!"
    return 0
fi

echo $$ > /tmp/sync.pid

# low priority task
echo $$ > /dev/cpuctl/others/bg_non_interactive/tasks

# copy : cloud -> local
#rclone --quiet --config /mnt/sdcard/rclone/rclone.conf copy box:mp3 /mnt/sdcard/mp3

# copy : local -> cloud
#rclone --quiet --config /mnt/sdcard/rclone/rclone.conf copy /mnt/sdcard/mp3 box:mp3

# sync : cloud -> local
rclone --quiet --config /mnt/sdcard/rclone/rclone.conf sync box:mp3 /mnt/sdcard/mp3

# sync : local -> cloud
#rclone --quiet --config /mnt/sdcard/rclone/rclone.conf sync /mnt/sdcard/mp3 box:mp3

rm /tmp/sync.pid
toast.sh "sync-up done"

