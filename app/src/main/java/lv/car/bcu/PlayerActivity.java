package lv.car.bcu;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.database.Cursor;
import android.os.Handler;
import android.content.ContentResolver;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parrot.asteroid.media.MediaManager;
import com.parrot.asteroid.media.MediaManagerFactory;
import com.parrot.asteroid.media.common.MediaPlayerControler;
import com.parrot.asteroid.audio.service.Metadata;
import com.parrot.asteroid.audio.service.PlayerInfo;
import com.parrot.asteroid.audio.service.Source;
import com.parrot.asteroid.mediaList.MediaListBrowser;
import com.parrot.asteroid.mediaList.MediaListManagerHSTI;
import com.parrot.platform.provider.MediaStoreHelper;
import com.parrot.asteroid.audio.service.Source;
import com.parrot.asteroid.mediaList.MediaListController;


public class PlayerActivity extends AppCompatActivity {
    private static final String TAG = PlayerActivity.class.getSimpleName();
    private AppMgr mAppMgr;
    private AudioMgr mAudioMgr;
    private MediaListController mMediaListController;
    private MediaManager mManager;
    private MediaListBrowser mBrowser;
    private MediaListManagerHSTI mHSTI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.activity_player);
        mAppMgr = new AppMgr(getApplicationContext());
        mAudioMgr = new AudioMgr(getApplicationContext(),mHandler);
        mMediaListController = new MediaListController(getApplicationContext());
        mManager = MediaManagerFactory.getMediaManager(getApplicationContext());
        //mBrowser = new MediaListBrowser();
        mHSTI = new MediaListManagerHSTI(getApplicationContext());
    }

    private Source getSourceFromIntent(Intent intent) {
        Source src = null;
        if (intent != null) {
            src = (Source) intent.getParcelableExtra("source");
        }
        if (src == null) {
            Log.w(TAG, "Source must be given in intent extras");
        }
        return src;
    }

    public void clickBack(View view) {
        Log.d(TAG, "Back was clicked");
        mAppMgr.showMain();
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            try {
                int cmd = msg.what;
                switch (cmd) {
                    case MainActivity.PLAYER_STATE: {
                        PlayerInfo data = (PlayerInfo) msg.obj;
                        //updatePlayerState(data);
                        break;
                    }
                    case MainActivity.PLAYER_META: {
                        Metadata data = (Metadata) msg.obj;
                        //updatePlayerMeta(data);
                        break;
                    }
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    };
}