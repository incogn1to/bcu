package com.parrot.asteroid.mediaList;

import android.content.Context;
import android.database.MatrixCursor;
import com.parrot.asteroid.audio.service.Source;

public class MediaListCategory extends MediaList {
    private CategoryList mCategoryList;
    private int mSelection;
    private Source mSource;

    public MediaListCategory(Context context, CategoryList categoryList, Source source) {
        super(context, source);
        this.mSource = source;
        this.mCategoryList = categoryList;
    }

    /* Access modifiers changed, original: 0000 */
    public MediaList createSubList(int position, long item) {
        return new MediaListBrowser(getContext(), (Category) this.mCategoryList.get(position), null, this.mSource);
    }

    /* Access modifiers changed, original: 0000 */
    public boolean hasSubList(int position, long id) {
        return true;
    }

    /* Access modifiers changed, original: 0000 */
    public void show() {
        MatrixCursor cursor = new MatrixCursor(new String[]{"_id", "category", "icon", "iconItem"});
        for (int i = 0; i < this.mCategoryList.size(); i++) {
            Object[] columns = new Object[4];
            int id = ((Category) this.mCategoryList.get(i)).getLabelId();
            int idIcon = ((Category) this.mCategoryList.get(i)).getIconId();
            int idIconItem = ((Category) this.mCategoryList.get(i)).getIconItem();
            String str = getContext().getString(id);
            columns[0] = Integer.valueOf(i);
            columns[1] = str;
            columns[2] = Integer.valueOf(idIcon);
            columns[3] = Integer.valueOf(idIconItem);
            cursor.addRow(columns);
        }
        getMediaListManager().setCursor(cursor);
    }

    /* Access modifiers changed, original: 0000 */
    public boolean hasAllEnabled() {
        return false;
    }

    /* Access modifiers changed, original: 0000 */
    public boolean isFilterable() {
        return false;
    }

    /* Access modifiers changed, original: 0000 */
    public boolean requestPosition(String text) {
        return false;
    }

    /* Access modifiers changed, original: 0000 */
    public void destroy() {
    }

    /* Access modifiers changed, original: 0000 */
    public String getSelectedText() {
        return getContext().getString(((Category) this.mCategoryList.get(this.mSelection)).getLabelId());
    }

    /* Access modifiers changed, original: 0000 */
    public void setSelection(int position, long id) {
        this.mSelection = position;
    }

    /* Access modifiers changed, original: 0000 */
    public boolean hasDynamicHeader() {
        return false;
    }

    /* Access modifiers changed, original: 0000 */
    public String getDynamicHeader(int position, long id) {
        return null;
    }

    /* Access modifiers changed, original: 0000 */
    public int getIconId() {
        return -1;
    }

    /* Access modifiers changed, original: 0000 */
    public boolean isRecursive() {
        return false;
    }
}
