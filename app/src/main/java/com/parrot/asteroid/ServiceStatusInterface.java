package com.parrot.asteroid;

public interface ServiceStatusInterface {

    public enum Status {
        SERVICE_DISCONNECTED,
        SERVICE_CONNECTED,
        SERVICE_READY
    }

    void onServiceReady(Status status);
}
