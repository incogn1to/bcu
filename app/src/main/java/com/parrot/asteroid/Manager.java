package com.parrot.asteroid;

import com.parrot.asteroid.tools.AsteroidObservable;

public abstract class Manager implements ManagerInterface {
    protected AsteroidObservable<ManagerObserverInterface> mManagerObservers = new AsteroidObservable();

    public synchronized void addManagerObserver(ManagerObserverInterface managerObserver) {
        synchronized (this.mManagerObservers) {
            this.mManagerObservers.addObserver(managerObserver);
        }
    }

    public synchronized void deleteManagerObserver(ManagerObserverInterface managerObserver) {
        synchronized (this.mManagerObservers) {
            this.mManagerObservers.deleteObserver(managerObserver);
        }
    }

    /* Access modifiers changed, original: protected|declared_synchronized */
    public synchronized void notifyManagerReady(boolean ready, Manager manager) {
        synchronized (this.mManagerObservers) {
            for (ManagerObserverInterface observer : this.mManagerObservers.getObserverList()) {
                observer.onManagerReady(ready, manager);
            }
        }
    }
}
