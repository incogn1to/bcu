package com.parrot.asteroid.media.tools;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import com.parrot.asteroid.ParrotIntent;
import com.parrot.asteroid.audio.service.Source;

public class ClearCurrentList {
    private static final String INTENT_EXTRA_SOURCE = "source";
    private static final String TAG = ClearCurrentList.class.getSimpleName();
    private String mActivityName;
    private BroadcastReceiver mBroadcastReceiver;
    private boolean mEnabled = true;
    private ClearCurrentListObserverInterface mObserver;

    public interface ClearCurrentListObserverInterface {
        void clearCurrentList(Source source);
    }

    public void disable() {
        this.mEnabled = false;
    }

    public void enable() {
        this.mEnabled = true;
    }

    public void register(Activity activity, ClearCurrentListObserverInterface observer) {
        this.mObserver = observer;
        this.mActivityName = activity.getLocalClassName();
        this.mBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Source src = (Source) intent.getParcelableExtra("source");
                if (src == null) {
                    Log.w(ClearCurrentList.TAG, "The current source is null !");
                } else if (ClearCurrentList.this.mObserver != null && ClearCurrentList.this.mEnabled) {
                    Log.d(ClearCurrentList.TAG, "The current source has changed. clear the list : " + ClearCurrentList.this.mActivityName);
                    ClearCurrentList.this.mObserver.clearCurrentList(src);
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ParrotIntent.ACTION_SOURCE_CHANGED);
        intentFilter.addCategory("android.intent.category.DEFAULT");
        activity.registerReceiver(this.mBroadcastReceiver, intentFilter);
    }

    public void unregister(Activity activity) {
        activity.unregisterReceiver(this.mBroadcastReceiver);
        this.mObserver = null;
    }
}
