package com.parrot.asteroid.audio.service;

public abstract class AudioEffect {
    public static final int AVC_NERVOUS = 3;
    public static final int AVC_NONE = 0;
    public static final int AVC_NORMAL = 2;
    public static final int AVC_SMOOTH = 1;
    public static final int EQUALIZER_BAND_MAX = 120;
    public static final int EQUALIZER_BAND_MAXID = 6;
    public static final int EQUALIZER_BAND_MIN = -120;
    public static final int EQUALIZER_BAND_MINID = 0;
    public static final int SUBWOOFER_FREQUENCY_MAX = 200;
    public static final int SUBWOOFER_FREQUENCY_MIN = 50;
    public static final int SUBWOOFER_LEVEL_MAX = 6;
    public static final int SUBWOOFER_LEVEL_MIN = -6;

    public abstract void enableLoudness(boolean z);

    public abstract boolean enableSubwoofer(boolean z);

    public abstract boolean enableVirtualBass(boolean z);

    public abstract int getAVC();

    public abstract int getBalance();

    public abstract int[] getEqualizer();

    public abstract int getFader();

    public abstract int getLoudness();

    public abstract int getSubwooferFrequency();

    public abstract int getSubwooferLevel();

    public abstract int getVirtualBassFequency();

    public abstract int getVirtualBassGain();

    public abstract boolean isAVC();

    public abstract boolean isEnableSubwoofer();

    public abstract boolean isLoudnessEnable();

    public abstract boolean isVirtualBass();

    public abstract void loadAllAudioSettings();

    public abstract boolean saveBalance(int i);

    public abstract boolean saveFader(int i);

    public abstract boolean setAVC(int i);

    public abstract boolean setEqualizer(int i, int i2, int i3, int i4, int i5, int i6, int i7);

    public abstract boolean setLoudness(int i);

    public abstract boolean setSubwooferFrequency(int i);

    public abstract boolean setSubwooferLevel(int i);

    public abstract boolean setVirtualBass(int i, int i2);
}
