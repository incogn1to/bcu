package lv.car.bcu;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class WeatherAdapter extends ArrayAdapter<String> {

    private Context ctx;
    private List<String> descriptions;
    private List<String> temperature;
    private List<String> date;
    private List<String> time;
    private List<Drawable> imageArray;
    private Typeface mTypeface;

    public WeatherAdapter(Context context,
                          List<String> text,
                          List<String> d,
                          List<String> t,
                          List<String> temp,
                          List<Drawable> images) {
        super(context,  R.layout.spinner_row, R.id.text, text);
        this.ctx = context;
        this.descriptions = text;
        this.temperature = temp;
        this.time = t;
        this.date = d;
        this.imageArray = images;
        final String font = context.getResources().getString(R.string.FONT);
        this.mTypeface = Typeface.createFromAsset(context.getAssets(), font);
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.weather_row, parent, false);

        TextView textView = (TextView) row.findViewById(R.id.description);
        textView.setText(descriptions.get(position));
        textView.setTypeface(mTypeface);

        textView = (TextView) row.findViewById(R.id.temp);
        textView.setText(temperature.get(position));
        textView.setTypeface(mTypeface);

        textView = (TextView) row.findViewById(R.id.time);
        textView.setText(time.get(position));
        textView.setTypeface(mTypeface);

        textView = (TextView) row.findViewById(R.id.date);
        textView.setText(date.get(position));
        textView.setTypeface(mTypeface);

        ImageView imageView = (ImageView)row.findViewById(R.id.icon);
        imageView.setImageDrawable(imageArray.get(position));

        return row;
    }
}
