package com.parrot.asteroid.audio.service;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;

public class PlayerInfo implements Parcelable, Cloneable {
    public static final int AUDIO_INVALID = -1;
    public static final int AUDIO_PAUSE = 1;
    public static final int AUDIO_PLAY = 2;
    public static final int AUDIO_STOP = 0;
    public static final Creator<PlayerInfo> CREATOR = new Creator<PlayerInfo>() {
        public PlayerInfo createFromParcel(Parcel source) {
            return new PlayerInfo(source);
        }
        public PlayerInfo[] newArray(int size) {
            return new PlayerInfo[size];
        }
    };
    public static final int FLAG_ALL = 511;
    public static final int FLAG_AUDIO = 2;
    public static final int FLAG_DURATION = 128;
    public static final int FLAG_MUTE = 16;
    public static final int FLAG_NONE = 0;
    public static final int FLAG_POSITION = 64;
    public static final int FLAG_RANDOM = 8;
    public static final int FLAG_REPEAT = 4;
    public static final int FLAG_SOURCE = 32;
    public static final int FLAG_STATE = 1;
    public static final int FLAG_VOLUME = 256;
    public static final int RANDOM_ALBUM = 3;
    public static final int RANDOM_INVALID = -1;
    public static final int RANDOM_NONE = 1;
    public static final int RANDOM_SONG = 2;
    public static final int REPEAT_ALL = 3;
    public static final int REPEAT_AUTO_ALBUM = 5;
    public static final int REPEAT_AUTO_SONG = 4;
    public static final int REPEAT_CONTINUOUS = 6;
    public static final int REPEAT_INVALID = -1;
    public static final int REPEAT_NONE = 1;
    public static final int REPEAT_SONG = 2;
    public static final int STATE_CONNECTED = 8;
    public static final int STATE_CONNECTING = 7;
    public static final int STATE_DISCONNECTED = 6;
    public static final int STATE_FF = 4;
    public static final int STATE_MUTED = 3;
    public static final int STATE_PAUSE = 1;
    public static final int STATE_PLAY = 2;
    public static final int STATE_REWIND = 5;
    public static final int STATE_STOP = 0;
    public static final int STATE_STREAMING = 9;
    public static final int STATE_UNKNOWN = 10;
    protected int mAudioActivityState = -1;
    protected int mDuration = -255;
    protected boolean mMute = false;
    protected int mPlayerVolume = -1;
    protected int mPlayerVolumeMax = -1;
    protected int mPlayerVolumeMin = -1;
    protected int mPosition = -255;
    protected int mRandommode = -1;
    protected int mRepeatmode = -1;
    protected int mState = 10;

    public PlayerInfo(Parcel source) {
        readFromParcel(source);
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            Log.e("PlayerInfo", "CloneNotSupportedException");
            return null;
        }
    }

    public int getState() {
        return this.mState;
    }

    public int getAudioActivityState() {
        return this.mAudioActivityState;
    }

    public int getDuration() {
        return this.mDuration;
    }

    public int getPosition() {
        return this.mPosition;
    }

    public int getRepeatmode() {
        return this.mRepeatmode;
    }

    public int getRandommode() {
        return this.mRandommode;
    }

    public boolean isMute() {
        return this.mMute;
    }

    public int getPlayerVolume() {
        return this.mPlayerVolume;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mState);
        dest.writeInt(this.mAudioActivityState);
        dest.writeInt(this.mDuration);
        dest.writeInt(this.mPosition);
        dest.writeInt(this.mRepeatmode);
        dest.writeInt(this.mRandommode);
        dest.writeInt(this.mMute ? 1 : 0);
        dest.writeInt(this.mPlayerVolume);
    }

    private void readFromParcel(Parcel parcel) {
        this.mState = parcel.readInt();
        this.mAudioActivityState = parcel.readInt();
        this.mDuration = parcel.readInt();
        this.mPosition = parcel.readInt();
        this.mRepeatmode = parcel.readInt();
        this.mRandommode = parcel.readInt();
        this.mMute = parcel.readInt() > 0;
        this.mPlayerVolume = parcel.readInt();
    }

    public int getChanges(PlayerInfo playerInfo) {
        int flag = 0;
        if (playerInfo == null) {
            return FLAG_ALL;
        }
        if (playerInfo.mMute != this.mMute) {
            flag = 0 + 16;
        }
        if (playerInfo.mAudioActivityState != this.mAudioActivityState) {
            flag += 2;
        }
        if (playerInfo.mDuration != this.mDuration) {
            flag += 128;
        }
        if (playerInfo.mPlayerVolume != this.mPlayerVolume) {
            flag += 256;
        }
        if (playerInfo.mPosition != this.mPosition) {
            flag += 64;
        }
        if (playerInfo.mRandommode != this.mRandommode) {
            flag += 8;
        }
        if (playerInfo.mRepeatmode != this.mRepeatmode) {
            flag += 4;
        }
        if (playerInfo.mState != this.mState) {
            flag++;
        }
        return flag;
    }

    static int adaptScaleToHard(int volume) {
        return volume;
    }

    public String toString() {
        String str = "/";
        String str2 = "/";
        str2 = "/";
        return "mState:" + this.mState + " mAudioActivityState:" + this.mAudioActivityState + " position:" + this.mPosition + str + this.mDuration + " mPlayerVolume :" + this.mPlayerVolume + str + this.mPlayerVolumeMax + " mPlayerVolumeMin :" + this.mPlayerVolumeMin + " mRandommode :" + this.mRandommode + " mRepeatmode :" + this.mRepeatmode + " mState :" + this.mState + " mMute :" + this.mMute;
    }
}
