package com.parrot.asteroid.audio.service;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;

public class Metadata implements Parcelable, Cloneable {
    public static final Creator<Metadata> CREATOR = new Creator<Metadata>() {
        public Metadata createFromParcel(Parcel source) {
            return new Metadata(source);
        }

        public Metadata[] newArray(int size) {
            return new Metadata[size];
        }
    };
    public static final int FLAG_ALBUM = 8;
    public static final int FLAG_ALL = 16383;
    public static final int FLAG_ARTIST = 1;
    public static final int FLAG_BAND = 2;
    public static final int FLAG_COMPOSER = 4;
    public static final int FLAG_COVER = 16;
    public static final int FLAG_DYNAMICPSNAME = 512;
    public static final int FLAG_FREQUENCY = 32;
    public static final int FLAG_GENRE = 64;
    public static final int FLAG_NAMEISFOUND = 1024;
    public static final int FLAG_NONE = 0;
    public static final int FLAG_PATH = 128;
    public static final int FLAG_RADIOTEXT = 2048;
    public static final int FLAG_STATICPSNAME = 256;
    public static final int FLAG_TITLE = 4096;
    public static final int FLAG_TRACK = 8192;
    public static final String NO_COVER = "";
    public static final int RADIO_SEARCH_FREQUENCY = -1;
    private String mAlbum;
    private String mArtist;
    private int mBand;
    private String mComposer;
    private String mCoverPath;
    private String mDynamicPsName;
    private int mFrequency;
    private String mGenre;
    private LOAD_STATUS mLoaded = LOAD_STATUS.NONE;
    private int mNameIsFound;
    private String mPath;
    private String mRadioText;
    private String mStaticPsName;
    private String mTitle;
    private int mTrack = -1;

    public enum LOAD_STATUS implements Parcelable {
        NONE,
        RESET,
        LOADING,
        LOADED;
        
        public static Creator<LOAD_STATUS> CREATOR = null;

        static {
            CREATOR = new Creator<LOAD_STATUS>() {
                public LOAD_STATUS createFromParcel(Parcel source) {
                    return LOAD_STATUS.values()[source.readInt()];
                }

                public LOAD_STATUS[] newArray(int size) {
                    return null;
                }
            };
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(ordinal());
        }
    }

    public Metadata clone() {
        try {
            return (Metadata) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public Metadata resetMedia() {
        this.mPath = null;
        this.mTitle = null;
        this.mArtist = null;
        this.mAlbum = null;
        this.mTrack = -1;
        this.mGenre = null;
        this.mComposer = null;
        this.mCoverPath = null;
        this.mLoaded = LOAD_STATUS.RESET;
        return this;
    }

    public Metadata resetTuner() {
        this.mFrequency = -1;
        this.mStaticPsName = null;
        this.mDynamicPsName = null;
        this.mRadioText = null;
        this.mBand = -1;
        this.mLoaded = LOAD_STATUS.RESET;
        return this;
    }

    public boolean isEmpty() {
        if (this.mPath == null && this.mTitle == null && this.mArtist == null && this.mAlbum == null && this.mTrack == -1 && this.mGenre == null && this.mComposer == null && this.mFrequency == -1 && this.mStaticPsName == null && this.mDynamicPsName == null && this.mRadioText == null && this.mBand == -1 && this.mCoverPath == null) {
            return true;
        }
        return false;
    }

    public Metadata(Parcel in) {
        readFromParcel(in);
    }

    public Metadata(String path, String title, String artist, String album, int track, String genre, String composer) {
        this.mPath = path;
        this.mTitle = title;
        this.mArtist = artist;
        this.mAlbum = album;
        this.mTrack = track;
        this.mGenre = genre;
        this.mComposer = composer;
    }

    public String getPath() {
        return this.mPath;
    }

    public String getTitle() {
        return this.mTitle;
    }

    public String getArtist() {
        return this.mArtist;
    }

    public String getAlbum() {
        return this.mAlbum;
    }

    public int getTrack() {
        return this.mTrack;
    }

    public String getGenre() {
        return this.mGenre;
    }

    public String getComposer() {
        return this.mComposer;
    }

    public int getFrequency() {
        return this.mFrequency;
    }

    public String getStaticPsName() {
        return this.mStaticPsName;
    }

    public String getDynamicPsName() {
        return this.mDynamicPsName;
    }

    public int getNameIsFound() {
        return this.mNameIsFound;
    }

    public String getRadioText() {
        return this.mRadioText;
    }

    public int getBand() {
        return this.mBand;
    }

    public String getCoverPath() {
        return this.mCoverPath;
    }

    public void setPath(String path) {
        this.mPath = path;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public void setArtist(String artist) {
        this.mArtist = artist;
    }

    public void setAlbum(String album) {
        this.mAlbum = album;
    }

    public void setTrack(int track) {
        this.mTrack = track;
    }

    public void setGenre(String genre) {
        this.mGenre = genre;
    }

    public void setComposer(String composer) {
        this.mComposer = composer;
    }

    public void setFrequency(int frequency) {
        this.mFrequency = frequency;
    }

    public void setStaticPsName(String staticPsName) {
        this.mStaticPsName = staticPsName;
    }

    public void setDynamicPsName(String dynamicPsName) {
        this.mDynamicPsName = dynamicPsName;
    }

    public void setNameIsFound(int nameIsFound) {
        this.mNameIsFound = nameIsFound;
    }

    public void setRadioText(String radioText) {
        this.mRadioText = radioText;
    }

    public void setBand(int band) {
        this.mBand = band;
    }

    public void setCoverPath(String coverPath) {
        this.mCoverPath = coverPath;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mPath);
        dest.writeString(this.mTitle);
        dest.writeString(this.mArtist);
        dest.writeString(this.mAlbum);
        dest.writeInt(this.mTrack);
        dest.writeString(this.mGenre);
        dest.writeString(this.mComposer);
        dest.writeString(this.mCoverPath);
        dest.writeInt(this.mFrequency);
        dest.writeString(this.mStaticPsName);
        dest.writeString(this.mDynamicPsName);
        dest.writeInt(this.mNameIsFound);
        dest.writeString(this.mRadioText);
        dest.writeInt(this.mBand);
        this.mLoaded.writeToParcel(dest, 0);
    }

    private void readFromParcel(Parcel in) {
        this.mPath = in.readString();
        this.mTitle = in.readString();
        this.mArtist = in.readString();
        this.mAlbum = in.readString();
        this.mTrack = in.readInt();
        this.mGenre = in.readString();
        this.mComposer = in.readString();
        this.mCoverPath = in.readString();
        this.mFrequency = in.readInt();
        this.mStaticPsName = in.readString();
        this.mDynamicPsName = in.readString();
        this.mNameIsFound = in.readInt();
        this.mRadioText = in.readString();
        this.mBand = in.readInt();
        this.mLoaded = (LOAD_STATUS) LOAD_STATUS.CREATOR.createFromParcel(in);
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o instanceof Metadata) {
            return getChanges((Metadata) o) == 0;
        } else {
            throw new RuntimeException("Metadata: Unsupported compare object:" + o.getClass().getSimpleName());
        }
    }

    private static boolean isStringChanged(String a, String b) {
        if (a != null && b != null) {
            return !a.equals(b);
        } else {
            int i;
            if (a == null) {
                i = 1;
            } else {
                i = 0;
            }
            if ((i ^ (b == null ? 1 : 0)) != 0) {
                return true;
            }
            if (a == null) {
                i = 1;
            } else {
                i = 0;
            }
            return (i & (b == null ? 1 : 0)) == 0;
        }
    }

    public int getChanges(Metadata metadata) {
        int flag = 0;
        if (metadata == null) {
            Log.e(Metadata.class.getSimpleName(), "metadate is null in getChanges");
            return 0;
        }
        if (isStringChanged(metadata.getAlbum(), this.mAlbum)) {
            flag = 0 | 8;
        }
        if (isStringChanged(metadata.getArtist(), this.mArtist)) {
            flag |= 1;
        }
        if (metadata.getBand() != this.mBand) {
            flag |= 2;
        }
        if (isStringChanged(metadata.getComposer(), this.mComposer)) {
            flag |= 4;
        }
        if (isStringChanged(metadata.getCoverPath(), this.mCoverPath)) {
            flag |= 16;
        }
        if (metadata.getFrequency() != this.mFrequency) {
            flag |= 32;
        }
        if (isStringChanged(metadata.getGenre(), this.mGenre)) {
            flag |= 64;
        }
        if (isStringChanged(metadata.getPath(), this.mPath)) {
            flag |= 128;
        }
        if (isStringChanged(metadata.getStaticPsName(), this.mStaticPsName)) {
            flag |= 256;
        }
        if (isStringChanged(metadata.getDynamicPsName(), this.mDynamicPsName)) {
            flag |= 512;
        }
        if (metadata.getNameIsFound() != this.mNameIsFound) {
            flag |= 1024;
        }
        if (isStringChanged(metadata.getRadioText(), this.mRadioText)) {
            flag |= FLAG_RADIOTEXT;
        }
        if (isStringChanged(metadata.getTitle(), this.mTitle)) {
            flag |= FLAG_TITLE;
        }
        if (metadata.getTrack() != this.mTrack) {
            flag |= FLAG_TRACK;
        }
        if (isStringChanged(metadata.getCoverPath(), this.mCoverPath)) {
            flag |= FLAG_TRACK;
        }
        return flag;
    }

    public static boolean isChanged(int changeFlag, int flag) {
        return (changeFlag & flag) > 0;
    }

    public void print(String tag) {
        Log.i(tag, "path : " + this.mPath);
        Log.i(tag, "title : " + this.mTitle);
        Log.i(tag, "artist : " + this.mArtist);
        Log.i(tag, "album : " + this.mAlbum);
        Log.i(tag, "nb track : " + this.mTrack);
        Log.i(tag, "genre : " + this.mGenre);
        Log.i(tag, "composer : " + this.mComposer);
        Log.i(tag, "cover : " + this.mCoverPath);
        Log.i(tag, "freq : " + this.mFrequency);
        Log.i(tag, "staticPsName : " + this.mStaticPsName);
        Log.i(tag, "dynamicPsName : " + this.mDynamicPsName);
        Log.i(tag, "nameIsFound : " + this.mNameIsFound);
        Log.i(tag, "radio : " + this.mRadioText);
        Log.i(tag, "band : " + this.mBand);
    }

    public void setLoaded(LOAD_STATUS loaded) {
        this.mLoaded = loaded;
    }

    public LOAD_STATUS getLoaded() {
        return this.mLoaded;
    }
}
