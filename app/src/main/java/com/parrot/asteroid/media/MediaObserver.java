package com.parrot.asteroid.media;

import com.parrot.asteroid.audio.service.Metadata;
import com.parrot.asteroid.audio.service.PlayerInfo;
import com.parrot.asteroid.audio.service.Source;

public abstract class MediaObserver extends MediaObserverDummy {
    public abstract void onMetadataChanged(Metadata metadata, int i);

    public abstract void onModeChanged(int i, int i2);

    public abstract void onPlayerInfoChanged(PlayerInfo playerInfo, int i);

    public abstract void onPlayerStatus(Source source, PlayerInfo playerInfo, Metadata metadata);

    public abstract void onStateChanged(int i);

    public abstract void onTrackProgress(int i, int i2);
}
