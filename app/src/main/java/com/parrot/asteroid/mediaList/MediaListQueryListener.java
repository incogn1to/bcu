package com.parrot.asteroid.mediaList;

import android.database.Cursor;

interface MediaListQueryListener {
    void onError(int i);

    void onLongRequest();

    void onQueryCompleted(Cursor cursor);
}
