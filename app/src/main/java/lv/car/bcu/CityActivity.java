package lv.car.bcu;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CityActivity extends AppCompatActivity {
    private static final String TAG = CityActivity.class.getSimpleName();
    private AppMgr mAppMgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_city);
            mAppMgr = new AppMgr(getApplicationContext());

            // { "message":0,
            //    "list":
            //    [
            //       {"clouds":{"all":4},"dt":1650477600,
            //         "wind":{"gust":10.42,"speed":4.78,"deg":65},
            //         "visibility":10000,"sys":{"pod":"n"},
            //         "weather":[{"id":800,"icon":"01n","description":"clear sky","main":"Clear"}],
            //         "pop":0,"dt_txt":"2022-04-20 18:00:00","main":{"temp_kf":3.77,"humidity":39,
            //         "pressure":1016,"temp_max":15.07,"sea_level":1016,"temp_min":11.3,"temp":15.07,
            //         "grnd_level":1016,"feels_like":13.65}}
            //    ],
            //    "cnt":3,"cod":"200",
            //    "city":
            //    {"coord":{"lon":24.1052,"lat":56.9496},"id":6615326,
            //    "timezone":10800,"sunset":1650476498,"name":"Vecrīga",
            //    "sunrise":1650423781,"population":2000,"country":"LV"}
            // }

            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                return;
            }

            final String str = extras.getString("json");
            final int pos = extras.getInt("position");

            final JSONObject js = new JSONObject(str);
            final JSONObject list = js.getJSONArray("list").getJSONObject(pos);
            final JSONArray city = js.getJSONArray("city");

            List<String> f = new ArrayList<String>();
            List<String> d = new ArrayList<String>();
            List<Drawable> icons = new ArrayList<Drawable>();


                final CityAdapter adapter = new CityAdapter(this, f, d);
            final ListView listview = (ListView) findViewById(R.id.weatherList);
            listview.setAdapter(adapter);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void clickBack(View view) {
        Log.d(TAG, "Back was clicked");
        finish();
        mAppMgr.showWeather();
    }
}
