package com.parrot.asteroid.mediaList;

import com.parrot.asteroid.tools.AsteroidObserverInterface;

public interface MediaListObserverInterface extends AsteroidObserverInterface {
    void onSourceRemoved();

    void onTooLongRequest();

    void onUpdateAdapter();

    void onUpdatePosition(int i);
}
