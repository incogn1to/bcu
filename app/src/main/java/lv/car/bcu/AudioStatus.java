package lv.car.bcu;
import android.util.Log;
import com.parrot.asteroid.ServiceStatusInterface;

public class AudioStatus implements ServiceStatusInterface {
    private static final String TAG = AudioStatus.class.getSimpleName();
    @Override
    public void onServiceReady(Status status)
    {
        Log.d(TAG,"audio server status " + status  );
    }
}
