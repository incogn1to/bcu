package com.parrot.asteroid.media;


import com.parrot.asteroid.audio.service.Metadata;
import com.parrot.asteroid.audio.service.Source;
import com.parrot.asteroid.audio.service.PlayerInfo;

public interface MediaObserverInterface {
    void onCreatingSource(Source source);

    void onManagerDestroyed(Object obj);

    void onMetadataChanged(Metadata metadata, int i);

    void onModeChanged(int i, int i2);

    void onPlayerInfoChanged(PlayerInfo playerInfo, int i);

    void onPlayerStatus(Source source, PlayerInfo playerInfo, Metadata metadata);

    void onPreparingSource(Source source);

    void onSourceChanged(Source source);

    void onSourceError(Source source);

    void onSourceReady(Source source);

    void onSourceRemoved(Source source);

    void onSourceUpdated(Source source);

    void onStateChanged(PlayerInfo playerInfo);

    void onTrackProgress(int i, int i2);
}
