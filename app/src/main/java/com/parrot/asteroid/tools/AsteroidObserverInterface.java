package com.parrot.asteroid.tools;

public interface AsteroidObserverInterface {
    void deleteObserver();

    void registerObserver();
}
