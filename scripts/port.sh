#!/system/bin/sh

PORT="$1"
INODE=""

if ! [ "$PORT" ]; then
    echo "usage $0 [PORT]"
    exit
fi

HEX_PORT=$(echo $PORT | awk '{ printf "%04X", $1 }')
echo "will search for port [$PORT] hex [$HEX_PORT]"

while read num HOST_PORT _ _ _ _ _ _ _ inode _; do
    CUR_PORT=$(echo $HOST_PORT | cut -d ':' -f2)
    if [ $CUR_PORT = $HEX_PORT ]; then
        INODE=$inode
        echo "[$HOST_PORT] match [$CUR_PORT] need to search for inode [$INODE]"
        PATTERN=socket:[$INODE]
        for fn in /proc/[1-9]*/fd/*; do
            TMP=${fn%/fd*}
            PID=$(echo $TMP | cut -d '/' -f3)
            SOCKET=$(readlink $fn)
            if [ "$SOCKET" = "$PATTERN" ]; then
                echo pid [$PID]
            fi
        done
    fi
done < /proc/net/tcp

