package com.parrot.asteroid.media.tools;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import com.parrot.asteroid.ParrotIntent;
import com.parrot.asteroid.audio.service.Source;

public class MediaPlayerLauncher {
    public static final String INTENT_PARAM_BACK_HOME_IF_SOURCE_CHANGED = "homeIfSrcChange";
    public static final String INTENT_PARAM_SOURCE = "source";
    private static final String TAG = MediaPlayerLauncher.class.getSimpleName();

    public static boolean launch(Context ctx, Source source, boolean gotoHomeIfSourceChange) {
        Intent intent = new Intent(ParrotIntent.ACTION_MEDIAPLAYER);
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        intent.putExtra("source", source);
        intent.putExtra(INTENT_PARAM_BACK_HOME_IF_SOURCE_CHANGED, gotoHomeIfSourceChange);
        intent.putExtra(ParrotIntent.EXTRA_MEDIAPLAYER_SWITCH_SOURCE, source);
        return startActivity(ctx, intent);
    }

    @Deprecated
    public static boolean launchFromHome(Context ctx, Source source, boolean gotoHomeIfSourceChange) {
        Intent intent = new Intent();
        intent.addCategory("android.intent.category.DEFAULT");
        intent.setAction(ParrotIntent.ACTION_MEDIAPLAYER);
        intent.putExtra("source", source);
        intent.putExtra(INTENT_PARAM_BACK_HOME_IF_SOURCE_CHANGED, gotoHomeIfSourceChange);
        return startActivity(ctx, intent);
    }

    public static void launchAudioEffect(Context context) {
        Intent intent = new Intent(ParrotIntent.ACTION_AUDIO_EFFECTS);
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(268435456);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Can't launch audioeffect activity : " + e.getMessage());
        }
    }

    private static boolean startActivity(Context ctx, Intent intent) {
        try {
            ctx.startActivity(intent);
            return true;
        } catch (ActivityNotFoundException e) {
            Toast.makeText(ctx, "No mediaplayer installed on the device", 1);
            Log.e(TAG, "No Mediaplayer Found");
            return false;
        }
    }
}
