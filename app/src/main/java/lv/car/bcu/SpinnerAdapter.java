package lv.car.bcu;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class SpinnerAdapter extends ArrayAdapter<String> {

    private Context ctx;
    private List<String> contentArray;
    private List<Drawable> imageArray;
    private Typeface mTypeface;

    public SpinnerAdapter(Context context,
                          List<String> objects,
                          List<Drawable> imageArray) {
        super(context,  R.layout.spinner_row, R.id.text, objects);
        this.ctx = context;
        this.contentArray = objects;
        this.imageArray = imageArray;
        final String font = context.getResources().getString(R.string.FONT);
        this.mTypeface = Typeface.createFromAsset(context.getAssets(), font);
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.spinner_row, parent, false);

        TextView textView = (TextView) row.findViewById(R.id.text);
        textView.setText(contentArray.get(position));
        textView.setTypeface(mTypeface);

        ImageView imageView = (ImageView)row.findViewById(R.id.icon);
        imageView.setImageDrawable(imageArray.get(position));

        return row;
    }
}