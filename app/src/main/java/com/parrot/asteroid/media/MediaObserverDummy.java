package com.parrot.asteroid.media;

import com.parrot.asteroid.audio.service.Metadata;
import com.parrot.asteroid.audio.service.PlayerInfo;
import com.parrot.asteroid.audio.service.Source;

public class MediaObserverDummy implements MediaObserverInterface {
    public void onManagerDestroyed(Object manager) {
    }

    public void onMetadataChanged(Metadata metadata, int changeFlag) {
    }

    public void onModeChanged(int repeatMode, int randomMode) {
    }

    public void onPlayerInfoChanged(PlayerInfo playerInfo, int flag) {
    }

    public void onPlayerStatus(Source source, PlayerInfo playerInfo, Metadata metadata) {
    }

    public void onPreparingSource(Source source) {
    }

    public void onSourceChanged(Source source) {
    }

    public void onStateChanged(PlayerInfo playerInfo) {
    }

    public void onTrackProgress(int position, int duration) {
    }

    public void onCreatingSource(Source creatingSrc) {
    }

    public void onSourceError(Source errorSrc) {
    }

    public void onSourceReady(Source readySrc) {
    }

    public void onSourceRemoved(Source removedSrc) {
    }

    public void onSourceUpdated(Source updatedSrc) {
    }
}
