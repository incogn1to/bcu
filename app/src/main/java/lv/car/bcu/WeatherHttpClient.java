package lv.car.bcu;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WeatherHttpClient {
    private static final String TAG = WeatherHttpClient.class.getSimpleName();
    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    private static final String KEY = "ab2757b5910fedfc3f437323124b2968";
    private static final String SUFFIX_URL = "&units=metric&appid=" + KEY;
    private static final String CITY_URL = "https://openweathermap.org/city/";
    // URL Examples:
    // http://api.openweathermap.org/data/2.5/weather?q=riga&lang=la&appid=ab2757b5910fedfc3f437323124b2968&units=metric
    // http://api.openweathermap.org/data/2.5/weather?lat=57&lon=24.08&lang=la&appid=ab2757b5910fedfc3f437323124b2968&units=metric
    // http://api.openweathermap.org/data/2.5/forecast?lat=57&lon=24.08&cnt=5&lang=la&appid=ab2757b5910fedfc3f437323124b2968&units=metric
    // http://api.openweathermap.org/data/2.5/onecall?lat=57&lon=24.08&exclude=minutely,hourly,allerts&lang=la&appid=ab2757b5910fedfc3f437323124b2968&units=metric
    // http://api.openweathermap.org/geo/1.0/direct?q=London&limit=5&appid=ab2757b5910fedfc3f437323124b2968

    public String getCurrentWeatherData(String location, String lang) {
        final String url = BASE_URL + "weather?q=" + location + "&lang=" + lang + SUFFIX_URL;
        return getWeatherData(url);
    }

    public String getWeatherData(String lon, String lat, String lang) {
        final String url = BASE_URL + "onecall?lat=" + lat + "&lon=" + lon + "&lang=" + lang + SUFFIX_URL;
        return getWeatherData(url);
    }

    public String getForecastWeatherData(String lon, String lat, String lang, int days) {
        final String url = BASE_URL + "forecast?lat=" + lat + "&lon=" + lon + "&cnt=" + days + "&lang=" + lang + SUFFIX_URL;
        return getWeatherData(url);
    }

    public String getCurrentWeatherData(String lon, String lat, String lang) {
        final String url = BASE_URL + "weather?lat=" + lat + "&lon=" + lon + "&lang=" + lang + SUFFIX_URL;
        return getWeatherData(url);
    }

    private String getWeatherData(String url) {
        HttpURLConnection con = null;
        InputStream is = null;

        try {
            Log.d("Tag", "URL: " + url);
            con = (HttpURLConnection) (new URL(url)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.connect();

            // Let's read the response
            StringBuffer buffer = null;
            try {
                buffer = new StringBuffer();
                is = con.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String line = null;
                while ((line = br.readLine()) != null)
                    buffer.append(line + "\r\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
            is.close();
            con.disconnect();

            return buffer.toString();
        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (Throwable t) {
            }
            try {
                con.disconnect();
            } catch (Throwable t) {
            }
        }
        return null;
    }
}
