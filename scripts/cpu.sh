#!/system/bin/sh
echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
# 20% CPU to audio
echo "4096" > /dev/cpuctl/audio/cpu.shares
# 80% CPU to other tasks
echo "8192" > /dev/cpuctl/others/cpu.shares
 

