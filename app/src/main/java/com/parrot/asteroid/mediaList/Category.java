package com.parrot.asteroid.mediaList;

class Category {
    private boolean mAllEnabled = false;
    private String mColumnId = null;
    private String mColumnValue = null;
    private String mDynamicHeader = null;
    private boolean mFilterable = false;
    private int mIconId = -1;
    private int mIconItem = -1;
    private int mLabelId = 0;
    private String mName = null;
    private boolean mRecursiv = false;
    private Category mSubCategory = null;

    Category() {
    }

    /* Access modifiers changed, original: 0000 */
    public void addSubCategory(Category subCategory) {
        this.mSubCategory = subCategory;
    }

    /* Access modifiers changed, original: 0000 */
    public Category getSubCategory() {
        if (isRecursive()) {
            return this;
        }
        return this.mSubCategory;
    }

    public boolean hasSubCategory() {
        return this.mSubCategory != null;
    }

    public int getLabelId() {
        return this.mLabelId;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void setLabelId(int labelId) {
        this.mLabelId = labelId;
    }

    public String getColumnId() {
        return this.mColumnId;
    }

    public String getColumnValue() {
        return this.mColumnValue;
    }

    public void setColumnId(String columnId) {
        this.mColumnId = columnId;
    }

    public void setColumnValue(String columnValue) {
        this.mColumnValue = columnValue;
    }

    public void setAllEnabled(boolean value) {
        this.mAllEnabled = value;
    }

    public boolean hasAllEnabled() {
        return this.mAllEnabled;
    }

    public void setRecursive(boolean recursive) {
        this.mRecursiv = recursive;
    }

    public boolean isRecursive() {
        return this.mRecursiv;
    }

    public String getDynamicHeader() {
        return this.mDynamicHeader;
    }

    public void setDynamicHeader(String dynamicHeader) {
        this.mDynamicHeader = dynamicHeader;
    }

    public void setIcon(int iconId) {
        this.mIconId = iconId;
    }

    public int getIconId() {
        return this.mIconId;
    }

    public void setFilterable(boolean filterable) {
        this.mFilterable = filterable;
    }

    public boolean isFilterable() {
        return this.mFilterable;
    }

    public void setIconItem(int iconItemId) {
        this.mIconItem = iconItemId;
    }

    public int getIconItem() {
        return this.mIconItem;
    }
}
