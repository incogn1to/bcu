package com.parrot.asteroid;

public interface ManagerObserverInterface {
    void onManagerReady(boolean z, Manager manager);
}
