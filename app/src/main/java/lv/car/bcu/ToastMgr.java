package lv.car.bcu;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ToastMgr extends Toast {
    private static final String TAG = ToastMgr.class.getSimpleName();
    LayoutInflater mInflater;
    private static String mCurrentTxt;
    private static boolean mTxtChanged = false;
    private static Typeface mTypeface;

    ToastMgr(Context context) {
        super(context);
        mInflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View customToastRoot = mInflater.inflate(R.layout.toast_row, null);
        setView(customToastRoot);
        setDuration(Toast.LENGTH_LONG);
        final String font = context.getResources().getString(R.string.FONT);
        mTypeface = Typeface.createFromAsset(context.getAssets(), font);
    }

    public void show() {
        if (mTxtChanged) {
            Log.d(TAG, "requesting to show toast:" + mCurrentTxt );
            super.show();
        }
    }

    public void setText(String txt, boolean force) {
        try {
            Log.d(TAG, "toast:" + txt);
            if (force || !mCurrentTxt.equals(txt)) {
                View customToastRoot = mInflater.inflate(R.layout.toast_row, null);
                TextView msg = (TextView) customToastRoot.findViewById(R.id.toastText);
                Log.d(TAG, "new txt:" + txt);
                msg.setText(txt);
                msg.setTypeface(mTypeface);
                setView(customToastRoot);
                mCurrentTxt = txt;
                mTxtChanged = true;
           } else {
                Log.d(TAG, "same txt:" + txt);
                mTxtChanged = false;
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
