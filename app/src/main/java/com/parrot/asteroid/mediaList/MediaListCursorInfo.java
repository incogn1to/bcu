package com.parrot.asteroid.mediaList;

import android.database.Cursor;
import java.util.HashMap;
import java.util.Map;

public class MediaListCursorInfo {
    private Cursor mCursor;
    private int mErrCode = 0;
    private Map<String, Object> mInfoList;
    private String[] mMainColumn;

    public MediaListCursorInfo(Cursor cursor, String mainColumn) {
        this.mCursor = cursor;
        this.mMainColumn = new String[1];
        this.mInfoList = new HashMap();
        this.mMainColumn[0] = mainColumn;
    }

    public Cursor getCursor() {
        return this.mCursor;
    }

    public String[] getMainColumn() {
        return this.mMainColumn;
    }

    /* Access modifiers changed, original: 0000 */
    public void setInfo(String name, int value) {
        this.mInfoList.put(name, new Integer(value));
    }

    /* Access modifiers changed, original: 0000 */
    public void setInfo(String name, String value) {
        this.mInfoList.put(name, value);
    }

    /* Access modifiers changed, original: 0000 */
    public void setInfo(String name, Object value) {
        this.mInfoList.put(name, value);
    }

    /* Access modifiers changed, original: 0000 */
    public int getInfoInt(String name) {
        return ((Integer) this.mInfoList.get(name)).intValue();
    }

    /* Access modifiers changed, original: 0000 */
    public String getInfoString(String name) {
        return (String) this.mInfoList.get(name);
    }

    /* Access modifiers changed, original: 0000 */
    public Object getInfo(String name) {
        return this.mInfoList.get(name);
    }

    /* Access modifiers changed, original: 0000 */
    public void setErrCode(int errCode) {
        this.mErrCode = errCode;
    }

    public int getErrCode() {
        return this.mErrCode;
    }

    public void closeCusor() {
        if (this.mCursor != null && !this.mCursor.isClosed()) {
            this.mCursor.close();
        }
    }
}
