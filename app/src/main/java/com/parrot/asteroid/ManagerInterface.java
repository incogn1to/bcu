package com.parrot.asteroid;

public interface ManagerInterface {
    void addManagerObserver(ManagerObserverInterface managerObserverInterface);

    void deleteManagerObserver(ManagerObserverInterface managerObserverInterface);

    boolean isManagerReady();
}
