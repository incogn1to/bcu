package com.parrot.asteroid.audio.service;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;
import java.util.ArrayList;

public class Source implements Parcelable, Cloneable {
    public static final String CAR_RADIO_NAME = "Car stereo";
    public static final int CONNEX_PERMANENT = 0;
    public static final int CONNEX_REMOVABLE = 1;
    public static final int CONNEX_STREAMING = 2;
    public static final Creator<Source> CREATOR = new Creator<Source>() {
        public Source createFromParcel(Parcel source) {
            return new Source(source);
        }
        public Source[] newArray(int size) {
            return new Source[size];
        }
    };
    public static final int DB_BUILDING = 1;
    public static final int DB_CLOSED = 0;
    public static final int DB_NONE = -1;
    public static final int DB_UPDATED = 2;
    public static final int DB_UPDATING = 3;
    public static final int DB_UPDATINGNOCHG = 4;
    public static final String DEEZER_NAME = "Deezer";
    public static final int ERROR_DISC_OK = 5;
    public static final int ERROR_DISC_UNABLE_ACT = 8;
    public static final int ERROR_DISC_UNEXPEC = 6;
    public static final int ERROR_EMPTY = 2;
    public static final int ERROR_NONE = 0;
    public static final int ERROR_NOT_SUPPORT = 3;
    public static final int ERROR_OUT_OF_MEM = 4;
    public static final int ERROR_UNKNOW = 1;
    public static final String LIVE_RADIO_NAME = "Liveradio";
    public static final String MOG_NAME = "Mog";
    public static final String PANDORA_NAME = "Pandora";
    public static final String RDIO_NAME = "Rdio";
    public static final int RESTORE_ACTIVE = 4;
    public static final int RESTORE_FROM_BOOT = 1;
    public static final int RESTORE_IMPLICIT = 2;
    public static final int RESTORE_LAST_CTX = 3;
    public static final int RESTORE_NONE = 0;
    public static final String RHAPSODY_NAME = "Rhapsody";
    public static final String SIMFY_NAME = "Simfy";
    public static final String SLACKER_NAME = "Slacker";
    public static final String SPOTIFY_NAME = "Spotify";
    public static final int STATE_ACTIVE = 4;
    public static final int STATE_ERROR = 5;
    public static final int STATE_INCOMING = 3;
    public static final int STATE_MISSING = 0;
    public static final int STATE_MOUNTING = 1;
    public static final int STATE_READY = 2;
    public static final int STATE_REMOVED = 6;
    public static final String TUNEIN_NAME = "TuneIn";
    public static final int TYPE_BLUETOOTH_SOURCE = 2;
    public static final int TYPE_FILES_SOURCE = 3;
    public static final int TYPE_INVALID_SOURCE = 0;
    public static final int TYPE_IPOD_IPHONE_SOURCE = 8;
    public static final int TYPE_LINEIN_SOURCE = 9;
    public static final int TYPE_SD_SOURCE = 5;
    public static final int TYPE_SMART_RADIO = 12;
    public static final int TYPE_TUNER_SOURCE = 7;
    public static final int TYPE_UNKNOWN_SOURCE = 1;
    public static final int TYPE_UPNP_SOURCE = 11;
    public static final int TYPE_USB_SOURCE = 4;
    public static final int TYPE_WEB_RADIO = 13;
    public static final int TYPE_WIFI_SOURCE = 10;
    private boolean mActivated = true;
    protected boolean mBrowsable = false;
    protected boolean mConnexState = false;
    protected int mConnexType = -1;
    protected int mDBState = -2;
    protected int mErrCode = 0;
    protected boolean mExtendedRepeatAvailable = false;
    protected boolean mMetadataAvailable = false;
    protected boolean mPlayingControl = false;
    protected boolean mRandomAlbumAvailable = false;
    protected boolean mRandomSongAvailable = false;
    protected boolean mRepeatAvailable = false;
    protected String mSourceId = null;
    protected int mSourceIdInt = -1;
    protected int mSourceLoadContextState = 0;
    protected String mSourceName = null;
    protected int mSourceStackId = -1;
    protected int mState = -1;
    protected int mSubType = -1;
    protected boolean mTrackPositionAvailable = false;
    protected int mType = 0;
    protected int mVolumeSource = -1;

    protected Source() {
    }

    public static Source createInvalidSource() {
        return new Source("S-1", Metadata.NO_COVER, 0, -1, false, false, false, false, false, false, false, false, 5, 1, false);
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            Log.e("Source", "CloneNotSupportedException");
            return null;
        }
    }

    public boolean inStack() {
        return (getConnexState() && getState() == 2) || getState() == 4;
    }

    public Source copy(Source source) {
        if (source != null) {
            this.mSourceId = source.mSourceId;
            this.mSourceIdInt = source.mSourceIdInt;
            this.mSourceName = source.mSourceName;
            this.mType = source.mType;
            this.mSubType = source.mSubType;
            this.mVolumeSource = source.mVolumeSource;
            this.mMetadataAvailable = source.mMetadataAvailable;
            this.mTrackPositionAvailable = source.mTrackPositionAvailable;
            this.mPlayingControl = source.mPlayingControl;
            this.mBrowsable = source.mBrowsable;
            this.mRandomSongAvailable = source.mRandomSongAvailable;
            this.mRandomAlbumAvailable = source.mRandomAlbumAvailable;
            this.mRepeatAvailable = source.mRepeatAvailable;
            this.mExtendedRepeatAvailable = source.mExtendedRepeatAvailable;
            this.mState = source.mState;
            this.mConnexType = source.mConnexType;
            this.mConnexState = source.mConnexState;
            this.mSourceStackId = source.mSourceStackId;
            this.mActivated = source.mActivated;
            this.mDBState = source.mDBState;
            this.mErrCode = source.mErrCode;
            this.mSourceLoadContextState = source.mSourceLoadContextState;
        } else {
            copy(createInvalidSource());
        }
        return this;
    }

    public boolean equals(Object o) {
        if (o instanceof Source) {
            return ((Source) o).getIntId() == getIntId();
        } else {
            if (o instanceof Integer) {
                return ((Integer) o).intValue() == getIntId();
            } else {
                if (o instanceof String) {
                    return ((String) o).equals(getId());
                }
                return super.equals(o);
            }
        }
    }

    public Source(Parcel in) {
        readFromParcel(in);
    }

    public Source(String src, String name, int type, int volume, boolean playingControl, boolean metadataAvailable, boolean trackPositionAvailable, boolean browsable, boolean randomSongAvailable, boolean randomAlbumAvailable, boolean repeatAvailable, boolean extendedRepeatAvailable, int state, int connexType, boolean connexState) {
        this.mSourceId = src;
        if (this.mSourceId != null) {
            this.mSourceIdInt = Integer.parseInt(this.mSourceId.subSequence(1, this.mSourceId.length()).toString());
        }
        this.mSourceName = name;
        this.mType = type;
        this.mVolumeSource = volume;
        this.mMetadataAvailable = metadataAvailable;
        this.mTrackPositionAvailable = trackPositionAvailable;
        this.mPlayingControl = playingControl;
        this.mBrowsable = browsable;
        this.mRandomSongAvailable = randomSongAvailable;
        this.mRandomAlbumAvailable = randomAlbumAvailable;
        this.mRepeatAvailable = repeatAvailable;
        this.mExtendedRepeatAvailable = extendedRepeatAvailable;
        this.mState = state;
        this.mConnexType = connexType;
        this.mConnexState = connexState;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mSourceId);
        dest.writeString(this.mSourceName);
        dest.writeInt(this.mType);
        dest.writeInt(this.mSubType);
        dest.writeInt(this.mVolumeSource);
        dest.writeInt(this.mSourceIdInt);
        dest.writeInt(this.mSourceStackId);
        dest.writeBooleanArray(new boolean[]{this.mMetadataAvailable, this.mBrowsable, this.mPlayingControl, this.mRandomSongAvailable, this.mRandomAlbumAvailable, this.mRepeatAvailable, this.mExtendedRepeatAvailable, this.mTrackPositionAvailable, this.mConnexState, this.mActivated});
        dest.writeInt(this.mState);
        dest.writeInt(this.mConnexType);
        dest.writeInt(this.mDBState);
        dest.writeInt(this.mErrCode);
        dest.writeInt(this.mSourceLoadContextState);
    }

    /* Access modifiers changed, original: 0000 */
    public void readFromParcel(Parcel in) {
        this.mSourceId = in.readString();
        this.mSourceName = in.readString();
        this.mType = in.readInt();
        this.mSubType = in.readInt();
        this.mVolumeSource = in.readInt();
        this.mSourceIdInt = in.readInt();
        this.mSourceStackId = in.readInt();
        boolean[] values = new boolean[10];
        in.readBooleanArray(values);
        this.mMetadataAvailable = values[0];
        this.mBrowsable = values[1];
        this.mPlayingControl = values[2];
        this.mRandomSongAvailable = values[3];
        this.mRandomAlbumAvailable = values[4];
        this.mRepeatAvailable = values[5];
        this.mExtendedRepeatAvailable = values[6];
        this.mTrackPositionAvailable = values[7];
        this.mConnexState = values[8];
        this.mActivated = values[9];
        this.mState = in.readInt();
        this.mConnexType = in.readInt();
        this.mDBState = in.readInt();
        this.mErrCode = in.readInt();
        this.mSourceLoadContextState = in.readInt();
    }

    public boolean isPlayingControl() {
        return this.mPlayingControl;
    }

    public boolean isMetadataAvailable() {
        return this.mMetadataAvailable;
    }

    public boolean isTrackPositionAvailable() {
        return this.mTrackPositionAvailable;
    }

    public boolean isBrowsable() {
        return this.mBrowsable;
    }

    public String getSourceName() {
        return this.mSourceName;
    }

    public int getType() {
        return this.mType;
    }

    public int getSubType() {
        return this.mSubType;
    }

    public int getVolumeSource() {
        return this.mVolumeSource;
    }

    public String getId() {
        return this.mSourceId;
    }

    public int getIntId() {
        return this.mSourceIdInt;
    }

    @Deprecated
    public int getSourceStackId() {
        return this.mSourceStackId;
    }

    public boolean isRandomSongAvailable() {
        return this.mRandomSongAvailable;
    }

    public boolean isRandomAlbumAvailable() {
        return this.mRandomAlbumAvailable;
    }

    public boolean isRepeatAvailable() {
        return this.mRepeatAvailable;
    }

    public boolean isExtendedRepeatAvailable() {
        return this.mExtendedRepeatAvailable;
    }

    public int getState() {
        return this.mState;
    }

    public int getConnexType() {
        return this.mConnexType;
    }

    public boolean getConnexState() {
        return this.mConnexState;
    }

    public boolean getActivated() {
        return this.mActivated;
    }

    public int getDbSate() {
        return this.mDBState;
    }

    public int getErrCode() {
        return this.mErrCode;
    }

    public int getContextStatusLoaded() {
        return this.mSourceLoadContextState;
    }

    public String toString() {
        return this.mSourceName;
    }

    public static void filterSource(ArrayList<Source> arrayList) {
    }
}
