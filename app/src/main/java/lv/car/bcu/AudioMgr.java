package lv.car.bcu;


import android.content.Context;
import com.parrot.asteroid.ServiceStatusInterface;
import com.parrot.asteroid.audio.service.AudioAccess;
import com.parrot.asteroid.audio.service.Source;
import java.util.ArrayList;
import java.util.Collections;

import android.graphics.drawable.Drawable;
import android.os.Handler;

public class AudioMgr {
    private static final String TAG = AudioMgr.class.getSimpleName();
    private Context mContext;
    public AudioAccess mAudio;

    AudioMgr(Context context, Handler hl) {
        mContext = context;
        ServiceStatusInterface audioStatus = new AudioStatus();
        mAudio = new AudioAccess(mContext, audioStatus);
        AudioCB audioCB = new AudioCB(hl);
        mAudio.setListener(audioCB);
    }

    void playPause() {
        mAudio.playPause();
    }

    void playNext() {
        mAudio.nextMedia();
    }

    void playPrev()
    {
        mAudio.previousMedia();
    }

    ArrayList<Source> getSources() {
        ArrayList<Source> list = mAudio.getSources(true);
        Collections.reverse(list);
        final Source current = mAudio.getCurrentSource();
        final int idx = list.indexOf(current);
        if (list.size() > 1 && current != null && idx >= 0) {
            list.set(idx,list.get(0));
            list.set(0, current);
        }
        return list;
    }

    public Drawable getIcon(int type) {
        switch (type) {
            case 2: // bt
                    return mContext.getResources().getDrawable(R.drawable.ic_sourcesaudio_bt);
            case 4:  // usb
                    return mContext.getResources().getDrawable(R.drawable.ic_sourcesaudio_usb);
            case 5:  // sd
                    return mContext.getResources().getDrawable(R.drawable.ic_sourcesaudio_sd);
            case 9:  // line-in
                    return mContext.getResources().getDrawable(R.drawable.ic_sourcesaudio_linein);
            case 12: // deezer
            case 3:  // files
            case 13: // web radio
            case 0:  // n/A
            case 1:  // tuner
            case 10: // wifi
            case 11: // usb
            case 6:
            case 7:
            case 8:
            default:
                return mContext.getResources().getDrawable(R.drawable.ic_melody);
        }
    }

    void playSource(Source source) {
        try {
            mAudio.playSource(source,false);
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private boolean hasDatabase(Source source) {
        if (source.getType() == 2 || source.getType() == 11) {
            return false;
        }
        return true;
    }

    Source getCurrentSource()
    {
        return mAudio.getCurrentSource();
    }
}
