# BCU
                     
### Description

BCU is application launcher designed for Volvo Sensus Connected Touch (SCT) media unit.
Original launcher for this system is called ACU and was developed by Volvo.
It was delivering sub-optimal user experience, so there was a necessity to develop a replacement.
Second argument - company decided to drop support for SCT and ACU and have disabled all remote services. 
All attempts to communicate Volvo or Parrot to solve this ended badly.
They where are unwiling to support unit and are where whiling to kill SCT.
Nobody where whilling to share any source code or infromation on system internals. 
As a result SCT unit become unoperational.
To be fair - SCT hardware is still quite powerfull and is capable of taking everyday needs.
BCU launcher is trying to aid this.
New launcher allows to execute applications and switch between them using steering wheel alone.
Main idea - not to take your hands from steering wheel and concentrate on the road. 
If steering wheel is not enough - use keypad instaed of touch screen.
BCU allows you to do most of activities while not touching extra controls.
It uses OpenWeather API to obtain weather data and shows currently played music track.

### UX/UI

#### Screenshots
![Launcher screen](screenshots/3.png)
![Weather screen](screenshots/4.png)

#### Video
- https://youtu.be/8tY9sDDus3Q
- https://youtu.be/xZyFQN9HuFw

### Supported platforms

BCU is designed for Volvo Sensus Connected Touch (SCT) media unit.
It can work on other devices as well.

https://www.volvocars.com/my/support/topics/car-systems/sensus-connected-touch/

### Requirements

- Android API level - 10.
- Android version 2.3 (Gingerbread) or higher.

### Compilation

- Android Studio - 3.4.2
- Gradle - 4.10.10
- Gradle plugin - 3.3.1

### Configuration

BCU can be configured using json configuration file.

Location: /mnt/sdcard/lv.car.bcu/config.json

It can be used to launch applications or execute shell scripts using hardware keypad. 
Example is provided ir config filder.

```javascript
{
    "keys" : {
        "KEYCODE_1" : {
            "action"   : "app",
            "package"  : "com.android.settings",
            "category" : "android.intent.category.LAUNCHER"
        },
        "KEYCODE_2" : {
            "action"   : "shell",
            "cmd"      : "adb-on.sh"
        } 
    }
}
```

### Next steps 

- Implement playlist control

### FAQ

- Q: BCU text doensn't fit the screen.
- A: It seems that your unit uses default DPI configuration. Recomendation is to change DPI from default 240 to 200. This can be done by editing /system/build.prop. ro.sf.lcd_density=240 field should be changed to ro.sf.lcd_density=200. Plese don't try untested DPI values, since it can brake SCT video output. 
- Q: Can I hide virtual buttons?
- A: Android frame buffer is reduced by 64px on kernel level. This is done using kernel boot parameter omaplfb.sgx_reduce_pixels_left=64. During system boot oemVKB deamon is started. It cathes virtual keyboard button touches and forwards them to Android environment. CAUTION: I bricked my unit while trying to get rid of virtual buttons. More details can be found here: https://habr.com/ru/articles/706840/ 
- Q: Does SCT support any hidden commands?
- A: Type using HW keyboard - #reboot# #menu# #diag00#

### License

GNU GPLv3

Copyrights (c) 2022 Pāvels Reško

### Used software

- OpenWeather: https://openweathermap.org/
- Graphical resources from Volvo ACU application

### Remarks

This software is created for R&D purposes and may not work as expected by the original authors. 
You use this software at your own risk.

### Useful links

- https://forum.xda-developers.com/t/volvo-sct-volvo-sensus-connected-touch-car-navi-audio.2449005/
- https://4pda.ru/forum/index.php?showtopic=619847&st=800

### Feedback 

You can write to me at pavels.resko@gmail.com

