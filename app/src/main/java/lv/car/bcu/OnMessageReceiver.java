package lv.car.bcu;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class OnMessageReceiver extends android.content.BroadcastReceiver {
    private static final String TAG = WeatherActivity.class.getSimpleName();

    public OnMessageReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            final String msg = intent.getStringExtra("MESSAGE");
            Log.i(TAG, "Message received: " + msg);
            final ToastMgr toastMgr = new ToastMgr( context );
            toastMgr.setText(msg,true);
            toastMgr.show();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
