package lv.car.bcu;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CityAdapter extends ArrayAdapter<String> {

    private Context ctx;
    private List<String> descriptions;
    private List<String> fields;
    private Typeface mTypeface;

    public CityAdapter(Context context,
                       List<String> f,
                       List<String> d ) {
        super(context,  R.layout.spinner_row, R.id.text);
        this.ctx = context;
        this.descriptions = d;
        this.fields = f;
        final String font = context.getResources().getString(R.string.FONT);
        this.mTypeface = Typeface.createFromAsset(context.getAssets(), font);
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.weather_row, parent, false);

        TextView textView = (TextView) row.findViewById(R.id.description);
        textView.setText(descriptions.get(position));
        textView.setTypeface(mTypeface);

        textView = (TextView) row.findViewById(R.id.text);
        textView.setText(fields.get(position));
        textView.setTypeface(mTypeface);

        return row;
    }
}
