package com.parrot.asteroid.mediaList;

import android.content.Context;
import com.parrot.asteroid.audio.service.Metadata;
import com.parrot.asteroid.audio.service.Source;
import java.util.ArrayList;

public class MediaListBrowser extends MediaList {
    private Category mCategory;
    private MediaListQuery mQuery;
    private String mSelectedText;
    private ArrayList<QuerySelection> mSelectionList;
    private Source mSource;

    public MediaListBrowser(Context context, Category category, ArrayList<QuerySelection> selectionList, Source source) {
        super(context, source);
        this.mCategory = category;
        this.mSelectionList = selectionList;
        this.mSource = source;
    }

    private void launchRequest() {
        this.mQuery = new MediaListQueryAsync(getContext(), this.mSource.getId(), this.mCategory.getName(), this.mCategory.getColumnValue());
        this.mQuery.addSelectionList(this.mSelectionList);
        this.mQuery.setTitle(this.mCategory.getDynamicHeader());
        this.mQuery.setRecursive(this.mCategory.isRecursive());
        getMediaListManager().launchQuery(this.mQuery);
    }

    /* Access modifiers changed, original: 0000 */
    public MediaList createSubList(int position, long id) {
        String str = "/";
        ArrayList<QuerySelection> requestList = new ArrayList();
        if (this.mSelectionList != null) {
            requestList.addAll(this.mSelectionList);
        }
        if (id != -1) {
            String idString = Metadata.NO_COVER + id;
            if (this.mCategory.getName().contains("directory")) {
                String str2;
                if (this.mSelectionList == null) {
                    str2 = "/";
                    str2 = "/";
                    idString = str + idString + str;
                } else {
                    str2 = "/";
                    idString = idString + str;
                }
            }
            requestList.add(new QuerySelection(this.mCategory.getColumnId(), idString));
        }
        if (hasSubList(position, id)) {
            return new MediaListBrowser(getContext(), this.mCategory.getSubCategory(), requestList, this.mSource);
        }
        return null;
    }

    /* Access modifiers changed, original: 0000 */
    public boolean hasSubList(int position, long id) {
        if (this.mCategory.isRecursive()) {
            return !getMediaListManager().isTrack(position, id);
        } else {
            return this.mCategory.hasSubCategory();
        }
    }

    /* Access modifiers changed, original: 0000 */
    public boolean isRecursive() {
        return this.mCategory.isRecursive();
    }

    /* Access modifiers changed, original: 0000 */
    public void show() {
        launchRequest();
    }

    /* Access modifiers changed, original: 0000 */
    public boolean hasAllEnabled() {
        return this.mCategory.hasAllEnabled();
    }

    /* Access modifiers changed, original: 0000 */
    public boolean isFilterable() {
        return this.mCategory.isFilterable();
    }

    /* Access modifiers changed, original: 0000 */
    public boolean requestPosition(String text) {
        return getMediaListManager().requestPosition(text);
    }

    /* Access modifiers changed, original: 0000 */
    public void destroy() {
        this.mQuery.finish();
    }

    /* Access modifiers changed, original: 0000 */
    public String getSelectedText() {
        return this.mSelectedText;
    }

    /* Access modifiers changed, original: 0000 */
    public void setSelection(int position, long id) {
        this.mSelectedText = getMediaListManager().getValue(position, id);
    }

    /* Access modifiers changed, original: 0000 */
    public boolean hasDynamicHeader() {
        return this.mCategory.getDynamicHeader() != null;
    }

    /* Access modifiers changed, original: 0000 */
    public String getDynamicHeader(int position, long id) {
        return getMediaListManager().getColumn(this.mCategory.getDynamicHeader(), position, id);
    }

    /* Access modifiers changed, original: 0000 */
    public int getIconId() {
        return this.mCategory.getIconId();
    }
}
