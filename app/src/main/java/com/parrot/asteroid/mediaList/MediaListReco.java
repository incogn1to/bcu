package com.parrot.asteroid.mediaList;

import android.content.Context;
import com.parrot.platform.provider.MediaStore.Audio.PlayerQueueColumns;

public class MediaListReco extends MediaList {
    private MediaListQuery mQuery;

    public /* bridge */ /* synthetic */ MediaListManagerInterface getMediaListManager() {
        return super.getMediaListManager();
    }

    public /* bridge */ /* synthetic */ void setMediaListManager(MediaListManagerInterface x0) {
        super.setMediaListManager(x0);
    }

    public MediaListReco(Context context) {
        super(context, null);
    }

    /* Access modifiers changed, original: 0000 */
    public MediaList createSubList(int position, long id) {
        return null;
    }

    /* Access modifiers changed, original: 0000 */
    public boolean hasSubList(int position, long id) {
        return false;
    }

    /* Access modifiers changed, original: 0000 */
    public boolean isRecursive() {
        return false;
    }

    /* Access modifiers changed, original: 0000 */
    public void show() {
        this.mQuery = new MediaListQueryAsync(getContext(), "S0", "playqueue", "title");
        this.mQuery.setSpecificProjectionfield(new String[]{PlayerQueueColumns.SOURCE});
        getMediaListManager().launchQuery(this.mQuery);
    }

    /* Access modifiers changed, original: 0000 */
    public boolean hasAllEnabled() {
        return false;
    }

    /* Access modifiers changed, original: 0000 */
    public boolean isFilterable() {
        return true;
    }

    /* Access modifiers changed, original: 0000 */
    public String getSelectedText() {
        return null;
    }

    /* Access modifiers changed, original: 0000 */
    public boolean requestPosition(String text) {
        return false;
    }

    /* Access modifiers changed, original: 0000 */
    public void destroy() {
        if (this.mQuery != null) {
            this.mQuery.finish();
        }
    }

    /* Access modifiers changed, original: 0000 */
    public void setSelection(int position, long id) {
    }

    /* Access modifiers changed, original: 0000 */
    public int getPosition() {
        return getMediaListManager().getPlaylistPosition();
    }

    /* Access modifiers changed, original: 0000 */
    public boolean hasDynamicHeader() {
        return false;
    }

    /* Access modifiers changed, original: 0000 */
    public String getDynamicHeader(int position, long id) {
        return null;
    }

    /* Access modifiers changed, original: 0000 */
    public int getIconId() {
        return 0;
    }
}
