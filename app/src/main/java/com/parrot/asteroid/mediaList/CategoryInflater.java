package com.parrot.asteroid.mediaList;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.Log;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

class CategoryInflater {
    private static final String TAG = CategoryInflater.class.getSimpleName();
    private static final String XML_ALL = "all";
    private static final String XML_CATEGORY = "category";
    private static final String XML_CATEGORYLIST = "categoryList";
    private static final String XML_COLUMN_ID = "columnId";
    private static final String XML_COLUMN_VALUE = "columnValue";
    private static final String XML_DYNAMIC_HEADER = "columnDynamicHeader";
    private static final String XML_FILTERABLE = "filterable";
    private static final String XML_ICON = "icone";
    private static final String XML_ICON_ITEM = "iconeItem";
    private static final String XML_LABEL = "label";
    private static final String XML_NAME = "name";
    private static final String XML_RECURSIVE = "recursive";
    private Context mContext;
    private XmlResourceParser mParser = null;

    public CategoryInflater(Context ctx, int categoryXmlId) {
        this.mParser = ctx.getResources().getXml(categoryXmlId);
        this.mContext = ctx;
    }

    public CategoryList inflate() throws XmlPullParserException {
        CategoryList categoryList = new CategoryList();
        try {
            parse(categoryList);
            return categoryList;
        } catch (IOException e) {
            throw new XmlPullParserException("io exception", this.mParser, e);
        }
    }

    private void parse(CategoryList categoryList) throws XmlPullParserException, IOException {
        Category category = null;
        while (this.mParser.getEventType() != 1) {
            if (this.mParser.getEventType() == 2) {
                Log.d(TAG, "tag found :" + this.mParser.getName());
                if (this.mParser.getName().equals(XML_CATEGORYLIST)) {
                    this.mParser.next();
                    break;
                }
            }
            this.mParser.next();
        }
        int currentLevel = this.mParser.getDepth();
        while (this.mParser.getEventType() != 1) {
            if (this.mParser.getEventType() == 2) {
                if (this.mParser.getDepth() > currentLevel) {
                    parseSubCategory(category);
                } else if (this.mParser.getDepth() >= currentLevel) {
                    if (this.mParser.getName().equals(XML_CATEGORY)) {
                        category = parseCategory();
                        categoryList.add(category);
                    }
                } else {
                    return;
                }
            }
            this.mParser.next();
        }
    }

    private void parseSubCategory(Category category) throws XmlPullParserException, IOException {
        Category newCategory = parseCategory();
        category.addSubCategory(newCategory);
        int currentLevel = this.mParser.getDepth();
        this.mParser.next();
        if (currentLevel < this.mParser.getDepth() && this.mParser.getEventType() == 2 && this.mParser.getName().equals(XML_CATEGORY)) {
            parseSubCategory(newCategory);
        }
    }

    private Category parseCategory() throws XmlPullParserException {
        Category category = new Category();
        category.setName(this.mParser.getAttributeValue(null, "name"));
        category.setColumnId(this.mParser.getAttributeValue(null, XML_COLUMN_ID));
        category.setColumnValue(this.mParser.getAttributeValue(null, XML_COLUMN_VALUE));
        String allEnabled = this.mParser.getAttributeValue(null, "all");
        if (allEnabled != null) {
            category.setAllEnabled(Boolean.parseBoolean(allEnabled));
        }
        String labelStr = this.mParser.getAttributeValue(null, XML_LABEL);
        if (labelStr != null) {
            int labelId = this.mContext.getResources().getIdentifier(labelStr, null, null);
            if (labelId == 0) {
                throw new XmlPullParserException(labelStr + " is not found in the application strings", this.mParser, null);
            }
            category.setLabelId(labelId);
        }
        String recursive = this.mParser.getAttributeValue(null, XML_RECURSIVE);
        if (recursive != null) {
            category.setRecursive(Boolean.parseBoolean(recursive));
        }
        category.setDynamicHeader(this.mParser.getAttributeValue(null, XML_DYNAMIC_HEADER));
        String icon = this.mParser.getAttributeValue(null, XML_ICON);
        if (icon != null) {
            category.setIcon(this.mContext.getResources().getIdentifier(icon, null, null));
        }
        String iconItem = this.mParser.getAttributeValue(null, XML_ICON_ITEM);
        if (iconItem != null) {
            category.setIconItem(this.mContext.getResources().getIdentifier(iconItem, null, null));
        }
        String filterable = this.mParser.getAttributeValue(null, XML_FILTERABLE);
        if (filterable != null) {
            category.setFilterable(Boolean.parseBoolean(filterable));
        }
        return category;
    }
}
