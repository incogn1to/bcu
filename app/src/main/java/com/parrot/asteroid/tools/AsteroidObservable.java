package com.parrot.asteroid.tools;

import java.util.ArrayList;
import java.util.List;

public class AsteroidObservable<TObserver> {
    private List<TObserver> mObserverList = new ArrayList();

    public synchronized boolean addObserver(TObserver observer) {
        boolean z;
        if (observer == null) {
            throw new NullPointerException();
        } else if (this.mObserverList.contains(observer)) {
            z = true;
        } else {
            z = this.mObserverList.add(observer);
        }
        return z;
    }

    public synchronized int countObservers() {
        return this.mObserverList.size();
    }

    public synchronized boolean deleteObserver(TObserver observer) {
        return this.mObserverList.remove(observer);
    }

    public synchronized void deleteObservers() {
        this.mObserverList.clear();
    }

    public synchronized TObserver getObserver(int index) {
        return this.mObserverList.get(index);
    }

    public synchronized List<TObserver> getObserverList() {
        return this.mObserverList;
    }
}
