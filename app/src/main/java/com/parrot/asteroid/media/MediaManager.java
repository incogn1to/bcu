package com.parrot.asteroid.media;

import android.util.Log;
import com.parrot.asteroid.Manager;
import com.parrot.asteroid.audio.service.Source;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class MediaManager extends Manager {
    protected static final String TAG = "MediaManager";
    protected ArrayList<MediaObserverInterface> mObservers = new ArrayList();

    public abstract void fastBackward();

    public abstract void fastForward();

    public abstract ArrayList<Source> getAllSource();

    public abstract Source getCurrentSource();

    public abstract ArrayList<Source> getSourceStack();

    public abstract boolean isCoverEnable();

    public abstract boolean isManagerReady();

    public abstract boolean isTypeSourceActivated(int i, int i2);

    public abstract void launchSource(Source source, boolean z);

    public abstract boolean nextMedia();

    public abstract boolean pause();

    public abstract boolean pause(boolean z);

    public abstract boolean play();

    public abstract boolean playSpeedNormal();

    public abstract boolean playpause();

    public abstract boolean previousMedia();

    public abstract boolean requestPlayerInfo();

    public abstract boolean resumeEngine();

    public abstract boolean seekToPosition(int i);

    public abstract boolean setCoverEnable(boolean z);

    public abstract boolean setRandomRepeatMode(int i, int i2);

    public abstract boolean setTypeSourceActivated(int i, int i2, boolean z);

    public abstract boolean start();

    public abstract boolean suspendEngine();

    MediaManager() {
    }

    public synchronized boolean addListener(MediaObserverInterface observer) {
        boolean ret;
        ret = false;
        if (this.mObservers == null) {
            this.mObservers = new ArrayList();
        }
        synchronized (this.mObservers) {
            if (observer != null) {
                if (!this.mObservers.contains(observer)) {
                    ret = this.mObservers.add(observer);
                }
            }
        }
        return ret;
    }

    public synchronized boolean removeListener(MediaObserverInterface observer) {
        int ret;
        boolean ret2 = false;
        if (this.mObservers == null) {
            ret = 0;
        } else {
            synchronized (this.mObservers) {
                if (observer != null) {
                    ret2 = this.mObservers.remove(observer);
                }
            }
            boolean ret3 = ret2;
        }
        //return ret3;
        return false;
    }

    /* Access modifiers changed, original: protected */
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    public synchronized void destroy() {
        String str = TAG;
        synchronized (this) {
            Log.w(TAG, "Singleton destroyed");
            if (this.mObservers != null) {
                Iterator i$ = this.mObservers.iterator();
                while (i$.hasNext()) {
                    MediaObserverInterface observer = (MediaObserverInterface) i$.next();
                    Log.w(TAG, "remove listener : " + observer.toString());
                    observer.onManagerDestroyed(this);
                }
                this.mObservers.clear();
                this.mObservers = null;
            }
            MediaManagerFactory.destroyInstance(this);
        }
    }

    /* Access modifiers changed, original: protected */
    public void finalize() throws Throwable {
        Log.w(TAG, "Singleton finalized");
        destroy();
        super.finalize();
    }

    public boolean isListPlayedFromMVR() {
        return false;
    }
}
