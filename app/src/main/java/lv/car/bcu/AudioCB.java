package lv.car.bcu;

import com.parrot.asteroid.audio.service.MediaCallbackInterface;
import com.parrot.asteroid.audio.service.Metadata;
import com.parrot.asteroid.audio.service.PlayerInfo;
import com.parrot.asteroid.audio.service.Source;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class AudioCB implements MediaCallbackInterface {
    private static final String TAG = MainActivity.class.getSimpleName();
    private Handler handler;

    AudioCB(Handler hl) {
        handler = hl;
    }

    public void onMetadataChanged(Metadata metadata, int i) {
        Log.d(TAG,"meta " + metadata.toString() + "i: " + i  );
        setPlayerMeta(metadata);
    }

    public void onPlayerStateChanged(PlayerInfo playerInfo) {
        //Log.d(TAG,"player " + playerInfo.toString() );
        setPlayerState(playerInfo);
    }

    public void onPlayerStatusAvailable(Source source, PlayerInfo playerInfo, Metadata metadata) {
        Log.d(TAG,"meta " + metadata.toString() + "player " + playerInfo.toString() );
        setPlayerMeta(metadata);
        setPlayerState(playerInfo);
    }

    public void onSourceChanged(Source source) {
        Log.d(TAG,"source changed " + source.toString() );
        if (source.getType()==9)
        {
            setLineIn();
        }
    }

    public void onSourceCreation(Source source) {
        Log.d(TAG,"source created " + source.toString() );
    }

    public void onSourceError(Source source)
    {
        Log.d(TAG,"source error " + source.toString() );
    }

    public void onSourceReady(Source source)
    {
        Log.d(TAG,"source ready " + source.toString() );
    }

    public void onSourceRemoved(Source source) {
        Log.d(TAG,"source removed " + source.toString() );
    }

    public void onSourceUpdate(Source source)
    {
        Log.d(TAG,"source updated " + source.toString() );
    }

    private void setLineIn() {
        try {
            Message msg = handler.obtainMessage();
            msg.what = MainActivity.LINE_IN;
            handler.sendMessage(msg);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void setPlayerState(PlayerInfo data) {
        try {
            Message msg = handler.obtainMessage();
            msg.what = MainActivity.PLAYER_STATE;
            msg.obj = data;
            handler.sendMessage(msg);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void setPlayerMeta(Metadata data) {
        try {
            Message msg = handler.obtainMessage();
            msg.what = MainActivity.PLAYER_META;
            msg.obj = data;
            handler.sendMessage(msg);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
