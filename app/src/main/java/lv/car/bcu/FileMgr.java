package lv.car.bcu;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class FileMgr {
    private static final String TAG = PlayerActivity.class.getSimpleName();
    private Context mContext;

    FileMgr(Context context )
    {
        mContext = context;
    }

    public void writeToFile(String filename, String data) {
        try {
            Log.v("writing to file: ", data);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(mContext.openFileOutput(filename, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e(TAG, "File write failed: " + e.toString());
        }
    }

    public String readFromFile(String filename) {
        String ret = "";
        try {
            InputStream inputStream = mContext.openFileInput(filename);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        } catch(Exception e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        Log.v( TAG,"json: " + filename + " content: " + ret);
        return ret;
    }

    String readFromExtFile(String fileName) {
        String ret = "";
        try {
            File file = new File(fileName);
            FileInputStream inputStream = new FileInputStream(file);
            Log.v( TAG,"will read file: " + file.toString());

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        } catch(Exception e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        Log.v( TAG,"json: " + fileName + " content: " + ret);
        return ret;
    }
}
