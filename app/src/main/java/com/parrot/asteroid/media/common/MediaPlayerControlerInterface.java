package com.parrot.asteroid.media.common;

import android.content.Context;
import com.parrot.asteroid.ManagerObserverInterface;
import com.parrot.asteroid.audio.service.Source;
import com.parrot.asteroid.media.MediaObserverInterface;
import java.util.ArrayList;

public interface MediaPlayerControlerInterface {
    void addListener(MediaObserverInterface mediaObserverInterface);

    void addManagerObserver(ManagerObserverInterface managerObserverInterface);

    void changeMedia(String str, int i);

    void deleteManagerObserver(ManagerObserverInterface managerObserverInterface);

    void fastBackward();

    void fastForward();

    ArrayList<Source> getAllSource();

    Source getCurrentSource();

    ArrayList<Source> getSourceStack();

    boolean isCoverEnable();

    boolean isListPlayedFromMVR();

    boolean isManagerReady();

    boolean isTunerSupported();

    boolean launchMediaList(Context context, Source source);

    void launchSource(Source source);

    void launchSource(Source source, boolean z);

    boolean nextMedia();

    void nextMediaWithUiNotif(int i);

    boolean pause();

    boolean play();

    boolean playPause();

    void playPauseWithUiNotif(int i);

    boolean playSpeedNormal();

    boolean previousMedia();

    void previousMediaWithUiNotif(int i);

    void removeListener(MediaObserverInterface mediaObserverInterface);

    boolean requestPlayerStatus();

    boolean resumeEngine();

    boolean seekToPosition(int i);

    boolean setCoverEnable(boolean z);

    boolean setRandomRepeatMode(int i, int i2);

    void start();

    boolean suspendEngine();
}
