package com.parrot.asteroid.mediaList;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.parrot.asteroid.audio.service.Source;
import com.parrot.asteroid.media.tools.MediaPlayerLauncher;
import com.parrot.asteroid.tools.AsteroidObservable;
import java.util.Stack;
import org.xmlpull.v1.XmlPullParserException;

public class MediaListController implements MediaListControllerInterface, MediaListObserverInterface {
    private static final int FILTER_REQUEST_DELAY_MS = 400;
    public static final int MODE_BROWSING = 1;
    public static final int MODE_CURRENTLIST = 2;
    private static final String TAG = MediaListController.class.getSimpleName();
    private int mBrowsingMode = 1;
    private CategoryList mCategoryRoot = null;
    private Context mContext = null;
    private SearchHandler mHandler = new SearchHandler();
    private int mLastPosition = 0;
    private MediaListManagerInterface mMediaListManager;
    private AsteroidObservable<MediaListObserverInterface> mMediaListObserverList;
    private Stack<MediaList> mMediaListStack;
    private MediaList mNewList;
    private Translation mOldTranslation = null;
    private int mPositionInCurrentList = 0;
    private Source mSource;

    class SearchHandler extends Handler {
        SearchHandler() {
        }

        public void handleMessage(Message msg) {
            String text = (String) msg.obj;
            if (!MediaListController.this.mMediaListStack.empty()) {
                if (text.length() == 0) {
                    ((MediaList) MediaListController.this.mMediaListStack.lastElement()).requestPosition(null);
                }
                ((MediaList) MediaListController.this.mMediaListStack.lastElement()).requestPosition(text);
            }
        }

        public void sendRequestPosition(String filter) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0, filter), 400);
        }
    }

    public MediaListController(Context ctx) {
        this.mContext = ctx;
        this.mMediaListManager = MediaListManagerFactory.getMediaListManager(ctx);
        this.mMediaListStack = new Stack();
        this.mMediaListObserverList = new AsteroidObservable();
        this.mMediaListManager.addObserver(this);
    }

    public boolean addObserver(MediaListObserverInterface mediaListObserver) {
        boolean addObserver;
        synchronized (this.mMediaListObserverList) {
            addObserver = this.mMediaListObserverList.addObserver(mediaListObserver);
        }
        return addObserver;
    }

    public boolean deleteObserver(MediaListObserverInterface mediaListObserver) {
        boolean deleteObserver;
        synchronized (this.mMediaListObserverList) {
            deleteObserver = this.mMediaListObserverList.deleteObserver(mediaListObserver);
        }
        return deleteObserver;
    }

    public void start(Source source, int categoryXmlId) throws XmlPullParserException {
        this.mBrowsingMode = 1;
        this.mSource = source;
        createCategoryTree(categoryXmlId);
        this.mMediaListManager.setSource(source);
        this.mMediaListStack.clear();
        this.mNewList = null;
        MediaListCategory mediaListCategory = new MediaListCategory(this.mContext, this.mCategoryRoot, this.mSource);
        this.mNewList = mediaListCategory;
        this.mPositionInCurrentList = -1;
        mediaListCategory.show();
    }

    public void startCurrentList(Source source, int categoryXmlId) throws XmlPullParserException {
        this.mBrowsingMode = 2;
        this.mSource = source;
        this.mMediaListStack.clear();
        this.mNewList = null;
        MediaListReco mediaListReco = new MediaListReco(this.mContext);
        this.mNewList = mediaListReco;
        this.mPositionInCurrentList = mediaListReco.getPosition();
        Log.d(TAG, "position : " + this.mPositionInCurrentList);
        this.mMediaListManager.setPositionInCurrentList(this.mPositionInCurrentList);
        mediaListReco.show();
    }

    private void buildCurrentBrowsing(Source source, int categoryXmlId) throws XmlPullParserException {
        createCategoryTree(categoryXmlId);
        this.mMediaListManager.setSource(source);
        this.mMediaListStack.clear();
        MediaListCategory mediaListCategory = new MediaListCategory(this.mContext, this.mCategoryRoot, this.mSource);
        this.mMediaListStack.push(mediaListCategory);
        mediaListCategory.show();
    }

    public void select(int position, long id, int headerCount) {
        if (this.mMediaListManager.requestIsPending()) {
            Log.i(TAG, "try to select an item whereas a request is pending");
            return;
        }
        MediaList currentList = (MediaList) this.mMediaListStack.lastElement();
        int cursorPosition = position - headerCount;
        this.mLastPosition = cursorPosition;
        currentList.setPosition(position);
        if (currentList.hasSubList(cursorPosition, id)) {
            currentList.setSelection(cursorPosition, id);
            this.mNewList = currentList.createSubList(cursorPosition, id);
            this.mNewList.show();
            return;
        }
        this.mMediaListManager.launchTrack(position, id, this.mBrowsingMode == 2);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                MediaListController.this.launchMediaPlayer();
            }
        }, 200);
    }

    public void showLastStack() {
        ((MediaList) this.mMediaListStack.lastElement()).setPosition(this.mLastPosition);
        ((MediaList) this.mMediaListStack.lastElement()).show();
    }

    public void launchMediaPlayer() {
        MediaPlayerLauncher.launch(this.mContext, this.mSource, true);
    }

    public boolean back() {
        this.mNewList = null;
        if (this.mBrowsingMode == 2) {
            return false;
        }
        if (this.mMediaListStack.empty()) {
            return false;
        }
        ((MediaList) this.mMediaListStack.pop()).destroy();
        if (this.mMediaListStack.empty()) {
            return false;
        }
        ((MediaList) this.mMediaListStack.lastElement()).show();
        return true;
    }

    private void createCategoryTree(int categoryXmlId) throws XmlPullParserException {
        this.mCategoryRoot = new CategoryInflater(this.mContext, categoryXmlId).inflate();
    }

    public boolean hasAllEnabled() {
        if (this.mMediaListStack.empty()) {
            return false;
        }
        return ((MediaList) this.mMediaListStack.lastElement()).hasAllEnabled();
    }

    public boolean isFilterable() {
        if (this.mMediaListStack.empty()) {
            return false;
        }
        return ((MediaList) this.mMediaListStack.lastElement()).isFilterable();
    }

    public boolean hasSubList(int position, int id) {
        if (this.mMediaListStack.empty()) {
            return false;
        }
        return ((MediaList) this.mMediaListStack.lastElement()).hasSubList(position, (long) id);
    }

    public boolean isRecusive() {
        if (this.mMediaListStack.empty()) {
            return false;
        }
        return ((MediaList) this.mMediaListStack.lastElement()).isRecursive();
    }

    public boolean requestPosition(String beginText) {
        this.mHandler.sendRequestPosition(beginText);
        return true;
    }

    public void requestDestroy(boolean force) {
        this.mMediaListManager.requestDestroy(force);
    }

    public void setLanguage(Translation translation) {
        if (this.mOldTranslation == null || !translation.equals(this.mOldTranslation)) {
            this.mMediaListManager.setTranslation(translation);
            this.mOldTranslation = translation;
        }
    }

    public boolean isEmpty() {
        return this.mMediaListStack.empty();
    }

    public int getPosition() {
        if (this.mMediaListStack.empty()) {
            return -1;
        }
        return ((MediaList) this.mMediaListStack.lastElement()).getPosition();
    }

    public int getPositionInCurrentList() {
        return this.mPositionInCurrentList;
    }

    public String getParentTxt() {
        if (this.mMediaListStack.size() <= 1) {
            return null;
        }
        return ((MediaList) this.mMediaListStack.get(this.mMediaListStack.size() - 2)).getSelectedText();
    }

    public int getIconId() {
        if (!this.mMediaListStack.empty()) {
            return ((MediaList) this.mMediaListStack.lastElement()).getIconId();
        }
        Log.e(TAG, "getIconId : no more element in the task");
        return 0;
    }

    public int getBrowsingMode() {
        return this.mBrowsingMode;
    }

    public String getDynamicHeader(int position, long id, int headerViewsCount) {
        if (!hasDynamicHeader()) {
            return null;
        }
        int cursorPosition = position - headerViewsCount;
        if (cursorPosition < 0) {
            return null;
        }
        return ((MediaList) this.mMediaListStack.lastElement()).getDynamicHeader(cursorPosition, id);
    }

    public boolean hasDynamicHeader() {
        if (!this.mMediaListStack.empty()) {
            return ((MediaList) this.mMediaListStack.lastElement()).hasDynamicHeader();
        }
        Log.e(TAG, "hasTitle : no more element in the task");
        return false;
    }

    public boolean isRootPage() {
        return this.mMediaListStack.size() <= 1;
    }

    public int getMediaListStackDepth() {
        return this.mMediaListStack.size();
    }

    public void registerObserver() {
    }

    public void deleteObserver() {
    }

    public void onTooLongRequest() {
        synchronized (this.mMediaListObserverList) {
            for (MediaListObserverInterface observer : this.mMediaListObserverList.getObserverList()) {
                observer.onTooLongRequest();
            }
        }
    }

    public void onUpdateAdapter() {
        if (this.mMediaListManager.getDataCursor().getErrCode() == 0) {
            if (this.mNewList != null) {
                this.mMediaListStack.push(this.mNewList);
            }
            synchronized (this.mMediaListObserverList) {
                for (MediaListObserverInterface observer : this.mMediaListObserverList.getObserverList()) {
                    observer.onUpdateAdapter();
                }
            }
            return;
        }
        Log.e(TAG, "Error during query request: " + this.mMediaListManager.getDataCursor().getErrCode());
    }

    public void onUpdatePosition(int position) {
        synchronized (this.mMediaListObserverList) {
            for (MediaListObserverInterface observer : this.mMediaListObserverList.getObserverList()) {
                observer.onUpdatePosition(position);
            }
        }
    }

    public void onSourceRemoved() {
        synchronized (this.mMediaListObserverList) {
            for (MediaListObserverInterface observer : this.mMediaListObserverList.getObserverList()) {
                observer.onSourceRemoved();
            }
        }
    }
}
