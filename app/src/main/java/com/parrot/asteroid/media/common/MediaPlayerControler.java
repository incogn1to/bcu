package com.parrot.asteroid.media.common;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.parrot.asteroid.ManagerObserverInterface;
import com.parrot.asteroid.ParrotIntent;
import com.parrot.asteroid.audio.service.Source;
import com.parrot.asteroid.media.MediaManager;
import com.parrot.asteroid.media.MediaManagerFactory;
import com.parrot.asteroid.media.MediaObserverInterface;
import java.util.ArrayList;
import java.util.Iterator;

public class MediaPlayerControler implements MediaPlayerControlerInterface {
    private static final boolean DEBUG = true;
    private static final String TAG = MediaPlayerControler.class.getSimpleName();
    private Context mContext;
    private MediaManager mManager;

    public MediaPlayerControler(Context applicationContext) {
        this.mManager = MediaManagerFactory.getMediaManager(applicationContext);
        this.mContext = applicationContext;
    }

    public boolean setProgress(int progress) {
        this.mManager.seekToPosition(progress);
        return true;
    }

    public boolean nextMedia() {
        return this.mManager.nextMedia();
    }

    public boolean previousMedia() {
        return this.mManager.previousMedia();
    }

    public boolean playPause() {
        return this.mManager.playpause();
    }

    public boolean pause() {
        return this.mManager.pause();
    }

    public boolean pause(boolean synchronously) {
        return this.mManager.pause(synchronously);
    }

    public boolean play() {
        return this.mManager.play();
    }

    public boolean playSpeedNormal() {
        return this.mManager.playSpeedNormal();
    }

    public boolean setRandomRepeatMode(int random, int repeat) {
        return this.mManager.setRandomRepeatMode(random, repeat);
    }

    public boolean launchMediaList(Context context, Source source) {
        Intent mediaListIntent = new Intent();
        mediaListIntent.addCategory("android.intent.category.DEFAULT");
        mediaListIntent.addFlags(268435456);
        mediaListIntent.addFlags(131072);
        mediaListIntent.addFlags(536870912);
        mediaListIntent.setAction(ParrotIntent.ACTION_MEDIALIST);
        if (source != null) {
            mediaListIntent.putExtra("source", source);
        }
        mediaListIntent.putExtra("retrieveListContext", true);
        try {
            context.startActivity(mediaListIntent);
            return true;
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void changeMedia(String browsingContext, int trackId) {
    }

    public boolean isManagerReady() {
        return this.mManager.isManagerReady();
    }

    public Source getCurrentSource() {
        return this.mManager.getCurrentSource();
    }

    public boolean requestPlayerStatus() {
        return this.mManager.requestPlayerInfo();
    }

    public void launchSource(Source source, boolean newContext) {
        this.mManager.launchSource(source, newContext);
    }

    public void launchSource(Source source) {
        this.mManager.launchSource(source, false);
    }

    public boolean seekToPosition(int position) {
        return this.mManager.seekToPosition(position);
    }

    public ArrayList<Source> getAllSource() {
        return this.mManager.getAllSource();
    }

    public ArrayList<Source> getSourceStack() {
        return this.mManager.getSourceStack();
    }

    public boolean isCoverEnable() {
        return this.mManager.isCoverEnable();
    }

    public boolean setCoverEnable(boolean enable) {
        return this.mManager.setCoverEnable(enable);
    }

    public boolean resumeEngine() {
        return this.mManager.resumeEngine();
    }

    public boolean suspendEngine() {
        return this.mManager.suspendEngine();
    }

    public void fastBackward() {
        this.mManager.fastBackward();
    }

    public void fastForward() {
        this.mManager.fastForward();
    }

    public boolean isTunerSupported() {
        ArrayList<Source> list = this.mManager.getAllSource();
        if (list != null) {
            Iterator i$ = list.iterator();
            while (i$.hasNext()) {
                if (((Source) i$.next()).getType() == 7) {
                    return true;
                }
            }
        }
        return false;
    }

    public void addListener(MediaObserverInterface listener) {
        this.mManager.addListener(listener);
    }

    public void removeListener(MediaObserverInterface listener) {
        this.mManager.removeListener(listener);
    }

    public void addManagerObserver(ManagerObserverInterface observer) {
        this.mManager.addManagerObserver(observer);
    }

    public void deleteManagerObserver(ManagerObserverInterface observer) {
        this.mManager.deleteManagerObserver(observer);
    }

    public void start() {
        this.mManager.start();
    }

    public void nextMediaWithUiNotif(int uiNotifFlags) {
        Log.d(TAG, "nextMediaWithUiNotif");
        Intent intent = new Intent(ParrotIntent.ACTION_MEDIA_NEXT);
        intent.putExtra("uiNotifFlags", uiNotifFlags);
        this.mContext.sendBroadcast(intent);
    }

    public void previousMediaWithUiNotif(int uiNotifFlags) {
        Log.d(TAG, "previousMediaWithUiNotif");
        Intent intent = new Intent(ParrotIntent.ACTION_MEDIA_PREVIOUS);
        intent.putExtra("uiNotifFlags", uiNotifFlags);
        this.mContext.sendBroadcast(intent);
    }

    public void playPauseWithUiNotif(int uiNotifFlags) {
        Log.d(TAG, "playPauseWithUiNotif");
        Intent intent = new Intent(ParrotIntent.ACTION_MEDIA_PLAYPAUSE);
        intent.putExtra("uiNotifFlags", uiNotifFlags);
        this.mContext.sendBroadcast(intent);
    }

    public boolean isListPlayedFromMVR() {
        return this.mManager.isListPlayedFromMVR();
    }
}
