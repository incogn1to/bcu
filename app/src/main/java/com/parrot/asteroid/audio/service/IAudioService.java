package com.parrot.asteroid.audio.service;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IAudioService extends IInterface {
    public static abstract class Stub extends Binder implements IAudioService {
        private static final String DESCRIPTOR = "com.parrot.asteroid.audio.service.IAudioService";
        static final int TRANSACTION_askAllMediaInfos = 18;
        static final int TRANSACTION_askMetadata = 19;
        static final int TRANSACTION_blockUiMediaNotification = 30;
        static final int TRANSACTION_changeListenMask = 7;
        static final int TRANSACTION_fastBackward = 13;
        static final int TRANSACTION_fastForward = 12;
        static final int TRANSACTION_getCoverActivated = 23;
        static final int TRANSACTION_getCurrentSource = 17;
        static final int TRANSACTION_getMaxVolume = 3;
        static final int TRANSACTION_getSourceStack = 26;
        static final int TRANSACTION_getVolume = 2;
        static final int TRANSACTION_isListPlayedFromMVR = 31;
        static final int TRANSACTION_isTypeActivated = 28;
        static final int TRANSACTION_nextMedia = 8;
        static final int TRANSACTION_pause = 15;
        static final int TRANSACTION_pauseSync = 16;
        static final int TRANSACTION_play = 11;
        static final int TRANSACTION_playPause = 10;
        static final int TRANSACTION_playSource = 27;
        static final int TRANSACTION_playSpeedNormal = 14;
        static final int TRANSACTION_previousMedia = 9;
        static final int TRANSACTION_register = 5;
        static final int TRANSACTION_resumeEngine = 25;
        static final int TRANSACTION_seekToPosition = 21;
        static final int TRANSACTION_setAudioVolume = 1;
        static final int TRANSACTION_setCoverActivated = 22;
        static final int TRANSACTION_setRandomRepeat = 20;
        static final int TRANSACTION_setTypeActivated = 29;
        static final int TRANSACTION_startParrotService = 4;
        static final int TRANSACTION_suspendEngine = 24;
        static final int TRANSACTION_unregister = 6;

        private static class Proxy implements IAudioService {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public boolean setAudioVolume(int src, int volume) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    boolean _result;
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(src);
                    _data.writeInt(volume);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    } else {
                        _result = false;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                    return false;
                }
            }

            public int getVolume(int source) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(source);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                    int _result = _reply.readInt();
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int getMaxVolume(int source) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(source);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                    int _result = _reply.readInt();
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean startParrotService() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    boolean _result;
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    } else {
                        _result = false;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }

                return false;
            }

            public boolean register(int maskListener, IAudioServiceCallBacks callBack) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    boolean _result;
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(maskListener);
                    _data.writeStrongBinder(callBack != null ? callBack.asBinder() : null);
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    } else {
                        _result = false;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }

                return false;
            }

            public boolean unregister(IAudioServiceCallBacks callBack) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    boolean _result;
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(callBack != null ? callBack.asBinder() : null);
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    } else {
                        _result = false;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }

                return false;
            }

            public boolean changeListenMask(int maskListener, IAudioServiceCallBacks callBack) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    boolean _result;
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(maskListener);
                    _data.writeStrongBinder(callBack != null ? callBack.asBinder() : null);
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    } else {
                        _result = false;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }

                return false;
            }

            public void nextMedia() throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(8, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void previousMedia() throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(9, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void playPause() throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(10, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void play() throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(11, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void fastForward() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(12, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void fastBackward() throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(13, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void playSpeedNormal() throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(14, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void pause() throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(15, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public boolean pauseSync() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    boolean _result;
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(16, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    } else {
                        _result = false;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }

                return false;
            }

            public Source getCurrentSource() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    Source _result;
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(17, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = (Source) Source.CREATOR.createFromParcel(_reply);
                    } else {
                        _result = null;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }

                return null;
            }

            public void askAllMediaInfos() throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(18, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void askMetadata() throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_askMetadata, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void setRandomRepeat(int random, int repeat) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(random);
                    _data.writeInt(repeat);
                    this.mRemote.transact(Stub.TRANSACTION_setRandomRepeat, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void seekToPosition(int position) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(position);
                    this.mRemote.transact(Stub.TRANSACTION_seekToPosition, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void setCoverActivated(boolean activated) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(activated ? 1 : 0);
                    this.mRemote.transact(Stub.TRANSACTION_setCoverActivated, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public boolean getCoverActivated() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    boolean _result;
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getCoverActivated, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    } else {
                        _result = false;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }

                return false;
            }

            public void suspendEngine(IAudioServiceCallBacks callBack) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(callBack != null ? callBack.asBinder() : null);
                    this.mRemote.transact(Stub.TRANSACTION_suspendEngine, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void resumeEngine(IAudioServiceCallBacks callBack) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeStrongBinder(callBack != null ? callBack.asBinder() : null);
                    this.mRemote.transact(Stub.TRANSACTION_resumeEngine, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public List<Source> getSourceStack(boolean onlyStacked) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    int i;
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (onlyStacked) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(Stub.TRANSACTION_getSourceStack, _data, _reply, 0);
                    _reply.readException();
                    List<Source> _result = _reply.createTypedArrayList(Source.CREATOR);
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void playSource(Source source, boolean newContext) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    int i;
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (source != null) {
                        _data.writeInt(1);
                        source.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    if (newContext) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(Stub.TRANSACTION_playSource, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public boolean isTypeActivated(int type, int id) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    boolean _result;
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(type);
                    _data.writeInt(id);
                    this.mRemote.transact(Stub.TRANSACTION_isTypeActivated, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    } else {
                        _result = false;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }

                return false;
            }

            public boolean setTypeActivated(int type, int id, boolean activated) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    int i;
                    boolean _result;
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(type);
                    _data.writeInt(id);
                    if (activated) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(Stub.TRANSACTION_setTypeActivated, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    } else {
                        _result = false;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }

                return false;
            }

            public boolean blockUiMediaNotification(boolean block, int flags) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    int i;
                    boolean _result;
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (block) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    _data.writeInt(i);
                    _data.writeInt(flags);
                    this.mRemote.transact(Stub.TRANSACTION_blockUiMediaNotification, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    } else {
                        _result = false;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }

                return false;
            }

            public boolean isListPlayedFromMVR() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    boolean _result;
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_isListPlayedFromMVR, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    } else {
                        _result = false;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }

                return false;
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IAudioService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof IAudioService)) {
                return new Proxy(obj);
            }
            return (IAudioService) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            String str = DESCRIPTOR;
            String str2;
            boolean _result;
            int i;
            int _result2;
            boolean _arg0;
            switch (code) {
                case 1:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    _result = setAudioVolume(data.readInt(), data.readInt());
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    reply.writeInt(i);
                    return true;
                case 2:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    _result2 = getVolume(data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result2);
                    return true;
                case 3:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    _result2 = getMaxVolume(data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result2);
                    return true;
                case 4:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    _result = startParrotService();
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    reply.writeInt(i);
                    return true;
                case 5:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    _result = register(data.readInt(), com.parrot.asteroid.audio.service.IAudioServiceCallBacks.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    reply.writeInt(i);
                    return true;
                case 6:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    _result = unregister(com.parrot.asteroid.audio.service.IAudioServiceCallBacks.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    reply.writeInt(i);
                    return true;
                case 7:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    _result = changeListenMask(data.readInt(), com.parrot.asteroid.audio.service.IAudioServiceCallBacks.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    reply.writeInt(i);
                    return true;
                case 8:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    nextMedia();
                    return true;
                case 9:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    previousMedia();
                    return true;
                case 10:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    playPause();
                    return true;
                case 11:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    play();
                    return true;
                case 12:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    fastForward();
                    reply.writeNoException();
                    return true;
                case 13:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    fastBackward();
                    return true;
                case 14:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    playSpeedNormal();
                    return true;
                case 15:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    pause();
                    return true;
                case 16:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    _result = pauseSync();
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    reply.writeInt(i);
                    return true;
                case 17:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    Source _result3 = getCurrentSource();
                    reply.writeNoException();
                    if (_result3 != null) {
                        reply.writeInt(1);
                        _result3.writeToParcel(reply, 1);
                    } else {
                        reply.writeInt(0);
                    }
                    return true;
                case 18:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    askAllMediaInfos();
                    return true;
                case TRANSACTION_askMetadata /*19*/:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    askMetadata();
                    return true;
                case TRANSACTION_setRandomRepeat /*20*/:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    setRandomRepeat(data.readInt(), data.readInt());
                    return true;
                case TRANSACTION_seekToPosition /*21*/:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    seekToPosition(data.readInt());
                    return true;
                case TRANSACTION_setCoverActivated /*22*/:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    if (data.readInt() != 0) {
                        _arg0 = true;
                    } else {
                        _arg0 = false;
                    }
                    setCoverActivated(_arg0);
                    return true;
                case TRANSACTION_getCoverActivated /*23*/:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    _result = getCoverActivated();
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    reply.writeInt(i);
                    return true;
                case TRANSACTION_suspendEngine /*24*/:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    suspendEngine(com.parrot.asteroid.audio.service.IAudioServiceCallBacks.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case TRANSACTION_resumeEngine /*25*/:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    resumeEngine(com.parrot.asteroid.audio.service.IAudioServiceCallBacks.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case TRANSACTION_getSourceStack /*26*/:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    if (data.readInt() != 0) {
                        _arg0 = true;
                    } else {
                        _arg0 = false;
                    }
                    List<Source> _result4 = getSourceStack(_arg0);
                    reply.writeNoException();
                    reply.writeTypedList(_result4);
                    return true;
                case TRANSACTION_playSource /*27*/:
                    Source _arg02;
                    boolean _arg1;
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    if (data.readInt() != 0) {
                        _arg02 = (Source) Source.CREATOR.createFromParcel(data);
                    } else {
                        _arg02 = null;
                    }
                    if (data.readInt() != 0) {
                        _arg1 = true;
                    } else {
                        _arg1 = false;
                    }
                    playSource(_arg02, _arg1);
                    return true;
                case TRANSACTION_isTypeActivated /*28*/:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    _result = isTypeActivated(data.readInt(), data.readInt());
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    reply.writeInt(i);
                    return true;
                case TRANSACTION_setTypeActivated /*29*/:
                    boolean _arg2;
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    int _arg03 = data.readInt();
                    int _arg12 = data.readInt();
                    if (data.readInt() != 0) {
                        _arg2 = true;
                    } else {
                        _arg2 = false;
                    }
                    _result = setTypeActivated(_arg03, _arg12, _arg2);
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    reply.writeInt(i);
                    return true;
                case TRANSACTION_blockUiMediaNotification /*30*/:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    if (data.readInt() != 0) {
                        _arg0 = true;
                    } else {
                        _arg0 = false;
                    }
                    _result = blockUiMediaNotification(_arg0, data.readInt());
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    reply.writeInt(i);
                    return true;
                case TRANSACTION_isListPlayedFromMVR /*31*/:
                    str2 = DESCRIPTOR;
                    data.enforceInterface(str);
                    _result = isListPlayedFromMVR();
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    reply.writeInt(i);
                    return true;
                case 1598968902:
                    str2 = DESCRIPTOR;
                    reply.writeString(str);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void askAllMediaInfos() throws RemoteException;

    void askMetadata() throws RemoteException;

    boolean blockUiMediaNotification(boolean z, int i) throws RemoteException;

    boolean changeListenMask(int i, IAudioServiceCallBacks iAudioServiceCallBacks) throws RemoteException;

    void fastBackward() throws RemoteException;

    void fastForward() throws RemoteException;

    boolean getCoverActivated() throws RemoteException;

    Source getCurrentSource() throws RemoteException;

    int getMaxVolume(int i) throws RemoteException;

    List<Source> getSourceStack(boolean z) throws RemoteException;

    int getVolume(int i) throws RemoteException;

    boolean isListPlayedFromMVR() throws RemoteException;

    boolean isTypeActivated(int i, int i2) throws RemoteException;

    void nextMedia() throws RemoteException;

    void pause() throws RemoteException;

    boolean pauseSync() throws RemoteException;

    void play() throws RemoteException;

    void playPause() throws RemoteException;

    void playSource(Source source, boolean z) throws RemoteException;

    void playSpeedNormal() throws RemoteException;

    void previousMedia() throws RemoteException;

    boolean register(int i, IAudioServiceCallBacks iAudioServiceCallBacks) throws RemoteException;

    void resumeEngine(IAudioServiceCallBacks iAudioServiceCallBacks) throws RemoteException;

    void seekToPosition(int i) throws RemoteException;

    boolean setAudioVolume(int i, int i2) throws RemoteException;

    void setCoverActivated(boolean z) throws RemoteException;

    void setRandomRepeat(int i, int i2) throws RemoteException;

    boolean setTypeActivated(int i, int i2, boolean z) throws RemoteException;

    boolean startParrotService() throws RemoteException;

    void suspendEngine(IAudioServiceCallBacks iAudioServiceCallBacks) throws RemoteException;

    boolean unregister(IAudioServiceCallBacks iAudioServiceCallBacks) throws RemoteException;
}
