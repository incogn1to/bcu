package com.parrot.asteroid.mediaList;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

class MediaListQuerySync extends MediaListQuery {
    private static final String TAG = MediaListQuerySync.class.getSimpleName();

    MediaListQuerySync(Context context, String sourceName, String category, String requestedColumn) {
        super(context, sourceName, category, requestedColumn);
    }

    /* Access modifiers changed, original: 0000 */
    public boolean launchRequest() {
        Uri request = buildRequest();
        String where = buildWhere();
        Log.d(TAG, "request: " + request + ", Where: " + where);
        Cursor cursor = this.mContext.getContentResolver().query(request, getProjection(), where, null, getOrder());
        if (this.mMediaListQueryListener != null) {
            this.mMediaListQueryListener.onQueryCompleted(cursor);
        } else if (cursor != null) {
            cursor.close();
        }
        return true;
    }

    public void finish() {
    }
}
