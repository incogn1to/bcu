#!/bin/sh

cd ~/Music/

name=$(yt-dlp --get-filename --audio-format mp3 --audio-quality 0 -x -o "%(title)s." $1)
yt-dlp --audio-format mp3 --audio-quality 0 -x -o "%(title)s." $1

if [ -z "$name" ]
then
      echo "download failed"
      exit 1
fi

orig_file_name=$name
echo "original filename: " $orig_file_name
new_file_name=$name

new_file_name=$(echo "$new_file_name" | cut -d'[' -f 1)
new_file_name=$(echo "$new_file_name" | cut -d'(' -f 1)
new_file_name=$(echo "$new_file_name" | cut -d'.' -f 1)
new_file_name=$(echo "$new_file_name" | sed 's/ *$//g')
new_file_name=$new_file_name".mp3"

echo "filename: " $new_file_name
mv "$orig_file_name" "$new_file_name"

rclone copy "$new_file_name" box:mp3/ 
echo "upload successful"
