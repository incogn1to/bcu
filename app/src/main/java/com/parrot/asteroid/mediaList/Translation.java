package com.parrot.asteroid.mediaList;

import android.util.Log;

public class Translation {
    private static final String TAG = Translation.class.getSimpleName();
    private String mTextAll;
    private String mTextEmpty;
    private String mTextRoot;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Translation)) {
            return false;
        }
        Translation tr = (Translation) obj;
        return this.mTextAll.equals(tr.mTextAll) && this.mTextRoot.equals(tr.mTextRoot) && this.mTextEmpty.equals(tr.mTextEmpty);
    }

    public void setTextAll(String textAll) {
        this.mTextAll = textAll;
    }

    /* Access modifiers changed, original: 0000 */
    public String getTextAll(String defaultValue) {
        if (this.mTextAll != null) {
            return getTextAll();
        }
        Log.d(TAG, "no 'all' translation use the default value :" + defaultValue);
        return defaultValue;
    }

    public String getTextAll() {
        return this.mTextAll;
    }

    public void setTextRoot(String textRoot) {
        this.mTextRoot = textRoot;
    }

    /* Access modifiers changed, original: 0000 */
    public String getTextRoot(String defaultValue) {
        if (this.mTextRoot != null) {
            return getTextRoot();
        }
        Log.d(TAG, "no 'root' translation use the default value :" + defaultValue);
        return defaultValue;
    }

    public String getTextRoot() {
        return this.mTextRoot;
    }

    public void setTextEmpty(String textEmpty) {
        this.mTextEmpty = textEmpty;
    }

    /* Access modifiers changed, original: 0000 */
    public String getTextEmpty(String defaultValue) {
        if (this.mTextEmpty != null) {
            return getTextEmpty();
        }
        Log.d(TAG, "no 'empty' translation use the default value :" + defaultValue);
        return defaultValue;
    }

    public String getTextEmpty() {
        return this.mTextEmpty;
    }
}
