package com.parrot.asteroid.mediaList;

import android.database.MatrixCursor;
import com.parrot.asteroid.ManagerInterface;
import com.parrot.asteroid.audio.service.Source;

public interface MediaListManagerInterface extends ManagerInterface {
    public static final int ERROR_GENERIC = 1;
    public static final int ERROR_SUCCESS = 0;
    public static final int ERROR_TIMEOUT = 2;

    boolean addObserver(MediaListObserverInterface mediaListObserverInterface);

    boolean deleteObserver(MediaListObserverInterface mediaListObserverInterface);

    String getColumn(String str, int i, long j);

    void getCurrentBrowsing();

    MediaListCursorInfo getDataCursor();

    int getPlaylistPosition();

    int getPositionInCurrentList();

    String getValue(int i, long j);

    boolean isCurrentListReco();

    boolean isFilterable(String str, Source source);

    boolean isTrack(int i, long j);

    void launchCurentListQuery(String str);

    void launchQuery(MediaListQuery mediaListQuery);

    void launchTrack(int i, long j, boolean z);

    void requestDestroy(boolean z);

    boolean requestIsPending();

    boolean requestPosition(String str);

    void setCursor(MatrixCursor matrixCursor);

    void setPositionInCurrentList(int i);

    void setSource(Source source);

    void setTranslation(Translation translation);
}
